﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WorldsEndCasino {

    public class TitleScreen : MonoBehaviour, AlphaFader.CallbackInterface {

        public string nextSceneName;
        public RectTransform[] skullParts;
        public GameObject skullPartsParent;
        public Animator crushSkullAnimator;
        public Animator hostWobbleAnimator;
        public GameObject tapToStart;

        [FMODUnity.EventRef]
        public string musicEvent;

        [FMODUnity.EventRef]
        public string worldsEndCasinoEvent;

        [FMODUnity.EventRef]
        public string crushSkullEvent;

        [FMODUnity.EventRef]
        public string zoomEvent;

        [Space]
        public AlphaFader black;
        public Zoomer zoomer;

        List<Vector3> skullDirections;

        void Awake() {
            black.RegisterCallbackInterface(this);
            skullPartsParent.SetActive(false);
        }

        void Start() {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            skullDirections = new List<Vector3>(skullParts.Length);

            // Velocity vectors with direction(unit) and speed(magnitude) components
            skullDirections.Add(new Vector3(0f, 0f));
            skullDirections.Add(new Vector3(-707f, 707f));
            skullDirections.Add(new Vector3(-707f, 707f));
            skullDirections.Add(new Vector3(707f, 707f));
            skullDirections.Add(new Vector3(707f, 707f));
            skullDirections.Add(new Vector3(707f, 707f));
        }
        
        void Update() {
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }

        public void OnTapToStart() {
            tapToStart.SetActive(false);
            hostWobbleAnimator.SetTrigger("StopWobble");
            crushSkullAnimator.SetTrigger("CrushSkull");// Animation launches TitleScreen.CrushSkull()
        }

        public void CrushSkull() {
            StartCoroutine(CrushSkullAudio());
            StartCoroutine(SkullPiecesAnimation());
        }

        IEnumerator CrushSkullAudio() {
            Options.PlayOneShot(crushSkullEvent, Options.AudioType.SFX);
            yield return new WaitForSeconds(1f);
            
            Options.PlayOneShot(worldsEndCasinoEvent, Options.AudioType.VOICE_ACTING);
            yield return new WaitForSeconds(2.5f);

            black.FadeIn();
            zoomer.Zoom(1.8f, 1f);
            Options.PlayOneShot(zoomEvent, Options.AudioType.SFX);
        }

        IEnumerator SkullPiecesAnimation() {
            const float GRAVITY = 150f;
            const float SPEED = 0.008f;

            skullPartsParent.SetActive(true);

            for (int time = 0; time < 150; ++time) {
                for(int skullIndex = 0; skullIndex < skullParts.Length; ++skullIndex) {
                    Vector3 skullDirection = skullDirections[skullIndex];
                    skullDirection.y -= GRAVITY;
                    skullDirections[skullIndex] = skullDirection;

                    Vector3 position = skullParts[skullIndex].position;
                    position.x += skullDirection.x * SPEED;
                    position.y += skullDirection.y * SPEED; // Multiplies gravity, whatever it works
                    skullParts[skullIndex].position = position;
                }
                yield return new WaitForSeconds(0.01f);
            }
        }

        public void OnFadeIn() {
            SceneManager.LoadScene(nextSceneName);
        }

        public void OnFadeOut() {
        }
    }
}