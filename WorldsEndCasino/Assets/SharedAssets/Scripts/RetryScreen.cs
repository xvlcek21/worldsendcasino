﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetryScreen : MonoBehaviour {

    public Text winner;
    public Text[] loosers;

    const float HORIZONTAL_OFFSET = 70f;
    const float VERTICAL_OFFSET = 40f;

    public void Show(bool status) {
        gameObject.SetActive(status);
    }

    public void SetWinner(string winnerName) {
        winner.text = winnerName + "!";
    }

    public void SetLoosers(List<string> loosersNames) {
        int lastIndex = loosersNames.Count - 1;
        Vector3 position = new Vector3(0f, VERTICAL_OFFSET, 0f);

        for (int i = 0; i <= lastIndex; ++i) {
            if (i % 2 == 0) {
                position.y -= VERTICAL_OFFSET;

                if (i != lastIndex)  position.x = -HORIZONTAL_OFFSET;
                else position.x = 0f;
            }
            else {
                position.x = HORIZONTAL_OFFSET;
            }

            loosers[i].rectTransform.anchoredPosition = position;
            loosers[i].text = loosersNames[i];
        }
    }
}
