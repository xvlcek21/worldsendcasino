﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardsUI : MonoBehaviour
{
    #region private variables

    private int firstHostCardIndex = 0;
    private int secondHostCardIndex = 0;

    #endregion

    #region public variables

    public BJ_Game blackjack;

    static public Vector2 playingDeckPosition;
    static public Vector2 discardDeckPosition;
    static public Vector2 playerDeckPosition;
    static public Vector2 hostDeckPosition;
    static public Vector2 playerDeckSwitchingPosition;
    static public Vector2 hostDeckSwitchingPosition;

    static public Vector2 bustPlayerDeckPosition;
    static public Vector2 bustHostDeckPosition;

    public List<Pair<string, GameObject>> playingDeck;
    public List<Pair<string, GameObject>> discardDeck;
    public List<Pair<string, GameObject>> playerDeck;
    public List<Pair<string, GameObject>> hostDeck;

    public Text playingDeckCardCounter;
    public Text playerHandTotal;
    public Text hostHandTotal;
    public GameObject hostHandBlackjackIndicator;
    public GameObject hostHandBustIndicator;

    public readonly int cardOffsetInHand = 12;
    public readonly float handSwitchingPhaseDuration = 1.0f;

    #endregion

    #region monobehaviour callbacks

    private void Awake()
    {
        blackjack = GameObject.Find("GameLogic").GetComponent<BJ_Game>();

        playingDeckPosition = new Vector2(300, 210);
        discardDeckPosition = new Vector2(300, 356);
        playerDeckPosition = new Vector2(-10, 65);
        hostDeckPosition = new Vector2(-10, 215);

        playerDeckSwitchingPosition = new Vector2(100, 138);
        hostDeckSwitchingPosition = new Vector2(-100, 138);

        bustPlayerDeckPosition = new Vector2(400, 210);
        bustHostDeckPosition = new Vector2(400, 356);

        playingDeck = new List<Pair<string, GameObject>>();
        discardDeck = new List<Pair<string, GameObject>>();
        playerDeck = new List<Pair<string, GameObject>>();
        hostDeck = new List<Pair<string, GameObject>>();
    }

    private void Start()
    {
    }

    private void FixedUpdate()
    {

    }

    #endregion

    #region private methods

    #endregion

    #region public methods

    public void CreateDeck()
    {
        List<Pair<BJ_Game.Suits, BJ_Game.Values>> deck = blackjack.GetPlayingDeck();

        playingDeck.Clear();

        // Assembling the playing deck in the correct order.
        for (int i = 0; i < deck.Count; ++i)
        {
            GameObject card = Instantiate(blackjack.cardPrefab, playingDeckPosition, Quaternion.identity, transform);
            card.name = deck[i].First.ToString() + "_" + deck[i].Second.ToString();
            card.transform.localPosition = new Vector2(playingDeckPosition.x + i * 0.20f, playingDeckPosition.y + i * 0.12f);

            card.GetComponent<CardMovement>().InitializeCard(deck[i].First, deck[i].Second);

            AdjustDeckPositions(BJ_Game.Decks.PLAYING);
            playingDeck.Add(new Pair<string, GameObject>(card.name, card));
        }
        playerHandTotal.gameObject.transform.parent.SetAsLastSibling();
        hostHandTotal.gameObject.transform.parent.SetAsLastSibling();
        playingDeckCardCounter.gameObject.transform.SetAsLastSibling();
    }

    public void MoveCardFromToDeck(ref List<Pair<string, GameObject>> sourceDeck, BJ_Game.Decks destinationDeck, int index = 0)
    {
        // Make room for the new card in the UI.
        AdjustDeckPositions(destinationDeck);

        CardMovement card = sourceDeck[index].Second.GetComponent<CardMovement>();

        switch (destinationDeck)
        {
            case BJ_Game.Decks.PLAYING:
                playingDeck.Add(sourceDeck[index]);
                card.cardTurning = true;
                StartCoroutine(card.CardTurning(false));
                card.HideCard();
                break;
            case BJ_Game.Decks.DISCARD:
                discardDeck.Add(sourceDeck[index]);
                card.HideCard();
                break;
            case BJ_Game.Decks.PLAYER:
                playerDeck.Add(sourceDeck[index]);
                card.cardTurning = true;
                StartCoroutine(card.CardTurning(true));
                card.ShowCard(false);
                break;
            case BJ_Game.Decks.HOST:
                hostDeck.Add(sourceDeck[index]);
                card.cardTurning = true;
                StartCoroutine(card.CardTurning(true));  
                if (hostDeck.Count != 2)
                    card.ShowCard(false);
                else
                    SwapCardsPosition(hostDeck[0].Second, hostDeck[1].Second);
                break;

        }

        card.SetDestination(destinationDeck);
        

        sourceDeck.RemoveAt(index);
        UpdatePlayingDeckCounter();
    }

    public void RearrangeDeckAfterShuffle(ref List<Pair<BJ_Game.Suits, BJ_Game.Values>> deck)
    {
        playingDeck.Clear();

        for (int i = 0; i < deck.Count; ++i)
        {
            string name = deck[i].First + "_" + deck[i].Second;
            Transform card = transform.FindChild(name);

            card.SetAsLastSibling();

            AdjustDeckPositions(BJ_Game.Decks.PLAYING);
            playingDeck.Add(new Pair<string, GameObject>(card.name, card.gameObject));          
            card.gameObject.GetComponent<CardMovement>().SetDestination(BJ_Game.Decks.PLAYING);
        }
        UpdatePlayingDeckCounter();
        playerHandTotal.gameObject.transform.parent.SetAsLastSibling();
        hostHandTotal.gameObject.transform.parent.SetAsLastSibling();
    }

    public void SwapCardsPosition(GameObject c1, GameObject c2)
    {
        int temp = c1.transform.GetSiblingIndex();
        c1.transform.SetSiblingIndex(c2.transform.GetSiblingIndex());
        c2.transform.SetSiblingIndex(temp);
    }

    // We need to compensate for the card going out of the player hand because of the redo last hit rule bender, before then next one comes in.
    public void readjustPlayerHand()
    {
        for (int i = 0; i < playerDeck.Count; ++i)
        {
            CardMovement cm = (playerDeck[i].Second).GetComponent<CardMovement>();
            cm.SetDestination(new Vector2(cm.GetDestination().x + cardOffsetInHand, cm.GetDestination().y));
        }
    }

    public void AssignBustHand(ref List<Pair<string, GameObject>> bustPlayerDeck, ref List<Pair<string, GameObject>> bustHostDeck)
    {
        CardMovement card;

        while (playerDeck.Count != 0)
        {
            AdjustDeckPositions(BJ_Game.Decks.PLAYER);
            bustPlayerDeck.Add(playerDeck[0]);
            card = bustPlayerDeck[bustPlayerDeck.Count - 1].Second.GetComponent<CardMovement>();
            playerDeck.RemoveAt(0);
        }

        while (hostDeck.Count != 0)
        {
            AdjustDeckPositions(BJ_Game.Decks.HOST);
            bustHostDeck.Add(hostDeck[0]);
            card = bustHostDeck[bustHostDeck.Count - 1].Second.GetComponent<CardMovement>();
            hostDeck.RemoveAt(0);
        }
    }

    public void MoveCurrentHandAway(bool includingPlayerHand = true)
    {
        if (includingPlayerHand)
        {
            for (int index = 0; index < playerDeck.Count; ++index)
            {
                playerDeck[index].Second.GetComponent<CardMovement>().SetDestination(bustPlayerDeckPosition);
            }
        }

        for (int index = 0; index < hostDeck.Count; ++index)
        {
            hostDeck[index].Second.GetComponent<CardMovement>().SetDestination(bustHostDeckPosition);
        }
    }

    public IEnumerator SwitchHands()
    {
        for (int index = 0; index < playerDeck.Count; ++index)
        {
            playerDeck[index].Second.GetComponent<CardMovement>().SetDestination(playerDeckSwitchingPosition);
        }

        for (int index = 0; index < hostDeck.Count; ++index)
        {
            hostDeck[index].Second.GetComponent<CardMovement>().SetDestination(hostDeckSwitchingPosition);
        }

        yield return new WaitForSeconds(handSwitchingPhaseDuration);

        for (int index = 0; index < playerDeck.Count; ++index)
        {
            playerDeck[index].Second.GetComponent<CardMovement>().SetDestination(BJ_Game.Decks.HOST);
        }

        for (int index = 0; index < hostDeck.Count; ++index)
        {
            hostDeck[index].Second.GetComponent<CardMovement>().SetDestination(BJ_Game.Decks.PLAYER);
        }

        // Swapping container contents.
        List<Pair<string, GameObject>> temp = new List<Pair<string, GameObject>>();
               
        while (playerDeck.Count != 0)
        {
            temp.Add(playerDeck[0]);
            playerDeck.RemoveAt(0);
        }

        while (hostDeck.Count != 0)
        {
            MoveCardFromToDeck(ref hostDeck, BJ_Game.Decks.PLAYER);
        }

        while (temp.Count != 0)
        {
            MoveCardFromToDeck(ref temp, BJ_Game.Decks.HOST);
        }

        SwapCardsPosition(hostDeck[0].Second, hostDeck[1].Second);

        blackjack.handsUITotalUpdateRequest();
        StartCoroutine(blackjack.DelayedShowHandResults(2));
    }

    public void UpdatePlayerHandTotalText(int total)
    {
        if (!playerHandTotal.transform.parent.gameObject.activeSelf)
        {
            playerHandTotal.transform.parent.gameObject.SetActive(true);
            hostHandTotal.transform.parent.gameObject.SetActive(true);
        }

        if (total != 0)
            playerHandTotal.text = total.ToString();
        else
            playerHandTotal.text = "";
    }

    public void UpdateHostHandTotalText(int total)
    {
        if (!hostHandTotal.gameObject.activeSelf)
            hostHandTotal.gameObject.SetActive(true);

        if (total == 0)
            hostHandTotal.text = "";
        else if (total == -1)
            hostHandTotal.text = "?";
        else
            hostHandTotal.text = total.ToString();
    }

    public void ToggleHostHandTotalUIIndication(bool showing, int total, bool isBlackjack)
    {
        if (showing)
        {
            if (total == 21 && isBlackjack)
                hostHandBlackjackIndicator.SetActive(true);

            if (total > 21)
                hostHandBustIndicator.SetActive(true);
        }
        else
        {
            hostHandBlackjackIndicator.SetActive(false);
            hostHandBustIndicator.SetActive(false);
        }
    }

    public IEnumerator DelayedUpdatePlayerHandTotalText(int total, float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        UpdatePlayerHandTotalText(total);
    }

    public IEnumerator DelayedUpdateHostHandTotalText(int total, float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        UpdateHostHandTotalText(total);
    }

    #endregion


    #region private methods

    void AdjustDeckPositions(BJ_Game.Decks deckType)
    {
        switch (deckType)
        {
            case BJ_Game.Decks.PLAYING:
                for (int i = 0; i < playingDeck.Count; ++i)
                {
                    CardMovement cm = (playingDeck[i].Second).GetComponent<CardMovement>();
                    cm.SetDestination(new Vector2(cm.GetDestination().x - 0.15f, cm.GetDestination().y - 0.05f));
                }
                break;
            case BJ_Game.Decks.DISCARD:
                for (int i = 0; i < discardDeck.Count; ++i)
                {
                    CardMovement cm = (discardDeck[i].Second).GetComponent<CardMovement>();
                    cm.SetDestination(new Vector2(cm.GetDestination().x - 0.15f, cm.GetDestination().y - 0.05f));
                }
                break;
            case BJ_Game.Decks.PLAYER:
                for (int i = 0; i < playerDeck.Count; ++i)
                {
                    CardMovement cm = (playerDeck[i].Second).GetComponent<CardMovement>();
                    cm.SetDestination(new Vector2(cm.GetDestination().x - cardOffsetInHand, cm.GetDestination().y));
                }
                break;
            case BJ_Game.Decks.HOST:
                for (int i = 0; i < hostDeck.Count; ++i)
                {
                    CardMovement cm = (hostDeck[i].Second).GetComponent<CardMovement>();
                    cm.SetDestination(new Vector2(cm.GetDestination().x - cardOffsetInHand, cm.GetDestination().y));
                }
                break;
        }
    }

    void UpdatePlayingDeckCounter()
    {
        playingDeckCardCounter.text = playingDeck.Count.ToString() + "/52";
    }

    #endregion
}