﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RR_RevolverScreen : MonoBehaviour {

    class SlideShowCallback : UtilityMethods.SlideCallback {

        GameObject spinCylinderPromptText;
        string[] hostLineEvents;
        
        public SlideShowCallback(GameObject spinCylinderPromptText, string[] hostLineEvents) {
            this.spinCylinderPromptText = spinCylinderPromptText;

            this.hostLineEvents = new string[hostLineEvents.Length];

            for (int i = 0; i < hostLineEvents.Length; ++i) {
                this.hostLineEvents[i] = hostLineEvents[i];
            }
        }

        public void OnSlideDone() {
            spinCylinderPromptText.SetActive(true);
            Options.PlayOneShot(hostLineEvents[UnityEngine.Random.Range(0, hostLineEvents.Length)], Options.AudioType.VOICE_ACTING);
        }
    }

    public GameObject spinCylinderPromptText;
    public RectTransform revolver;
    public Vector3 offScreenPosition;
    public Image swipeIndicator;

    [FMODUnity.EventRef]
    public string[] hostLineEvents;

    Vector3 revolverPosition;
    SlideShowCallback slideShowCallback;
    Color swipeIndicatorColor;

    void Awake() {
        swipeIndicatorColor = swipeIndicator.color;
        swipeIndicatorColor.a = 0f;
        swipeIndicator.color = swipeIndicatorColor;

        slideShowCallback = new SlideShowCallback(spinCylinderPromptText, hostLineEvents);
        revolverPosition = revolver.anchoredPosition;
        Hide();
	}

    public void HideSwipeIndicator () {
        swipeIndicatorColor.a = 0f;
        swipeIndicator.color = swipeIndicatorColor;
        StopAllCoroutines();
    }

    public void Show(string playerName) {
        StartCoroutine(ShowSwipeIndicator());
        revolver.gameObject.SetActive(true);
        UtilityMethods.Slide(this, revolver, revolverPosition, 30f, 0f, slideShowCallback);
        spinCylinderPromptText.GetComponent<Text>().text = playerName + ",\nSpin the cylinder!";
    }

    public void Hide() {
        UtilityMethods.Slide(this, revolver, offScreenPosition, 60f, 0f);
        spinCylinderPromptText.SetActive(false);
    }
    
    IEnumerator ShowSwipeIndicator() {
        yield return new WaitForSeconds(3f);

        float speed = 0.5f;

        while (true) {
            swipeIndicatorColor.a += speed * Time.deltaTime;
            swipeIndicator.color = swipeIndicatorColor;
            if (speed > 0 && swipeIndicatorColor.a >= 1f) speed = -speed;
            else if (speed < 0 && swipeIndicatorColor.a < 0f) speed = -speed;
            yield return null;
        }
    }
}
