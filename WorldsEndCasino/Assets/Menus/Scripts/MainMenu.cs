﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour, AlphaFader.CallbackInterface, UtilityMethods.SlideCallback {

    class TabChanger : UtilityMethods.SlideCallback {

        const float SLIDE_SPEED = 7f;
        const float SLIDE_ACCELERATION = 4f;

        MonoBehaviour monoBehaviour;
        RectTransform currentTab;
        RectTransform nextTab;

        Vector3 currentTabDestination;
        Vector3 nextTabDestination;

        public bool locked = false;

        public TabChanger(MonoBehaviour monoBehaviour, RectTransform currentTab, RectTransform nextTab) {
            this.monoBehaviour = monoBehaviour;
            this.currentTab = currentTab;
            this.nextTab = nextTab;

            currentTabDestination = currentTab.anchoredPosition;
            currentTabDestination.x = 200f;
            nextTabDestination = nextTab.anchoredPosition;
        }

        public void ChangeTab() {
            locked = true;
            UtilityMethods.Slide(monoBehaviour, currentTab, currentTabDestination, SLIDE_SPEED, SLIDE_ACCELERATION, this);
        }

        public void OnSlideDone() {
            UtilityMethods.Slide(monoBehaviour, nextTab, nextTabDestination, SLIDE_SPEED, SLIDE_ACCELERATION);
            locked = false;
        }
    }

    #region public variables

    public AlphaFader black;
    public Zoomer zoomer;
    public HandSwipeTransition handSwipeTransition;

    [Space]
    [Header("Tabs")]
    public GameObject mainTab;
    public GameObject optionsTab;

    [Space]
    public GameObject credits;

    // The buttons that need to be inactive during scene change animation
    public Button[] buttons;

    public Toggle vibrationToggle;
    public Toggle musicToggle;
    public Toggle sfxToggle;
    public Toggle voiceActingToggle;

    [FMODUnity.EventRef]
    public string[] entryHostLines;

    [FMODUnity.EventRef]
    public string[] idleTaunts;

    #endregion

    EventInstancePlayer themePlayer;

    MainMenu.TabChanger mainTabToOptionsTab;
    MainMenu.TabChanger optionsTabToMainTab;

    bool leftFromCredits = false;
    float tauntTimer = 0f;
    bool musicToggled = false;

    #region Monobehaviour Callbacks

    void Awake() {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        black.RegisterCallbackInterface(this);
        SetButtonsActive(false);
        
        mainTabToOptionsTab = new TabChanger(this, mainTab.GetComponent<RectTransform>(), optionsTab.GetComponent<RectTransform>());
        optionsTabToMainTab = new TabChanger(this, optionsTab.GetComponent<RectTransform>(), mainTab.GetComponent<RectTransform>());

        // Hide options tab
        Vector3 fuckThis = optionsTab.GetComponent<RectTransform>().anchoredPosition;
        fuckThis.x = 200f;
        optionsTab.GetComponent<RectTransform>().anchoredPosition = fuckThis;

        mainTab.SetActive(true);
        optionsTab.SetActive(true);
    }

    void Start() {
        PlayerPrefs.DeleteKey("BJRetry");

        vibrationToggle.isOn = Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsHelper.VIBRATION_KEY, 1));
        musicToggle.isOn = Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsHelper.MUSIC_KEY, 1));
        sfxToggle.isOn = Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsHelper.SFX_KEY, 1));
        voiceActingToggle.isOn = Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsHelper.VOICE_ACTING_KEY, 1));

        // Scene entry animation
        black.FadeOut();
        zoomer.Zoom(1f, 4f);

        themePlayer = GameObject.Find("ThemePlayer").GetComponent<EventInstancePlayer>();
        if (!themePlayer.IsPlaying()) {// else we have the instanciated object already in the scene
            DontDestroyOnLoad(themePlayer.gameObject);
            themePlayer.StartInstance(Options.AudioType.MUSIC);
            Options.PlayOneShot(entryHostLines[UnityEngine.Random.Range(0, entryHostLines.Length)], Options.AudioType.VOICE_ACTING);
        }
    }
	
	void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        if (tauntTimer < 30f) {
            tauntTimer += Time.deltaTime;
        }
        else {
            tauntTimer = 0f;
            Options.PlayOneShot(idleTaunts[UnityEngine.Random.Range(0, idleTaunts.Length)], Options.AudioType.VOICE_ACTING);
        }
	}
    
    #endregion

    #region Private Methods

    void SetButtonsActive(bool state) {
        for (int i = 0; i < buttons.Length; ++i) {
            buttons[i].interactable = state;
        }
    }

    #endregion

    #region public methods

    public void SelectRussianRoulette() {
        tauntTimer = 0f;

        SetButtonsActive(false);

        // MultiplayerMenu needs to know what game was selected
        PlayerPrefs.SetInt(PlayerPrefsHelper.GAME_KEY, PlayerPrefsHelper.RUSSIAN_ROULETTE);
        handSwipeTransition.ChangeScene("MultiplayerMenu");
    }

    public void SelectBlackJack() {
        tauntTimer = 0f;

        SetButtonsActive(false);

        // MultiplayerMenu needs to know what game was selected
        PlayerPrefs.SetInt(PlayerPrefsHelper.GAME_KEY, PlayerPrefsHelper.BLACK_JACK);
        handSwipeTransition.ChangeScene("MultiplayerMenu");
    }

    public void SelectMainTab() {
        mainTab.GetComponent<CanvasGroup>().interactable = true;
        optionsTab.GetComponent<CanvasGroup>().interactable = false;

        tauntTimer = 0f;

        if (musicToggled) {

            musicToggled = false;

            if (Convert.ToBoolean(PlayerPrefs.GetInt(PlayerPrefsHelper.MUSIC_KEY, 1))) themePlayer.StartInstance(Options.AudioType.MUSIC);
            else themePlayer.Stop();
        }
        if (!mainTabToOptionsTab.locked && !optionsTabToMainTab.locked) optionsTabToMainTab.ChangeTab();
    }

    public void SelectOptionsTab() {
        mainTab.GetComponent<CanvasGroup>().interactable = false;
        optionsTab.GetComponent<CanvasGroup>().interactable = true;

        tauntTimer = 0f;

        if (!optionsTabToMainTab.locked && !mainTabToOptionsTab.locked) mainTabToOptionsTab.ChangeTab();
    }

    public void ShowCredits() {
        tauntTimer = 0f;

        handSwipeTransition.SwipeToDark(this);
    }

    public void HideCredits() {
        tauntTimer = 0f;

        leftFromCredits = true;

        // Override the scene entry trigger point.
        // It's not needed after the scene entry so no need to reset it after this.
        black.fadeOutCallbackTriggerPoint = 0f;
        black.FadeIn();

        zoomer.Zoom(zoomer.initialScale, 2f);
    }

    #endregion

    #region Options

    public void ToggleVibration() {
        tauntTimer = 0f;

        Options.SetVibration(vibrationToggle.isOn);
    }

    public void ToggleMusic() {
        tauntTimer = 0f;

        if (!musicToggled) musicToggled = true;
        else musicToggled = false;// Music was toggled back to the last setting before exiting Options

        Options.SetMusic(musicToggle.isOn);
    }

    public void ToggleSFX() {
        tauntTimer = 0f;

        Options.SetSoundEffects(sfxToggle.isOn);
    }

    public void ToggleVoiceActing() {
        tauntTimer = 0f;

        Options.SetVoiceActing(voiceActingToggle.isOn);
    }

    #endregion

    #region Callbacks

    // Scene entry and leaving from credits are handled in the same UtilityMethods.CallbackInterface
    // so use leftForCredits(bool) to determine which fading is handled
    public void OnFadeIn() {
        if (leftFromCredits) {
            credits.SetActive(false);
            black.FadeOut();
            zoomer.Zoom(1f, 2f);
        }
    }

    public void OnFadeOut() {
        if (!leftFromCredits) SetButtonsActive(true);
    }

    public void OnSlideDone() {
        credits.SetActive(true);
        handSwipeTransition.EnterScene();
    }
    
    #endregion
}
