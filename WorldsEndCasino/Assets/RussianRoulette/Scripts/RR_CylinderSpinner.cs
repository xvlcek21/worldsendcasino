﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WorldsEndCasino {

    public class RR_CylinderSpinner : MonoBehaviour {

        public Camera mainCamera;
        public RectTransform sweepVectorLeft;
        public RectTransform sweepVectorRight;
        public RectTransform sweepVectorCenter;
        public RectTransform sweepVectorCenterDirection;
        public RR_RevolverScreen revolverScreen;

        [FMODUnity.EventRef]
        public string cylinderSpinEvent;

        Animator cylinderAnimator;
        public bool blockInput = true;

        bool signSet = false;
        bool positiveSign = true;

        void Start() {
            cylinderAnimator = GetComponent<Animator>();
        }

        void Update() {
            if (!blockInput) {

                // MOUSE

                // Buggy but works
                if (Input.GetMouseButton(0)) {
                    Vector3 center = sweepVectorCenter.position;
                    Vector3 centerDirection = sweepVectorCenterDirection.position;
                    Vector3 centerToCenterDirection = (centerDirection - center).normalized;

                    Vector3 mousePosition = Input.mousePosition;
                    Vector3 mouseToCenter = (mousePosition - center).normalized;
                    
                    float dot = Vector3.Dot(centerToCenterDirection, mouseToCenter);

                    if (!signSet) {
                        signSet = true;

                        if (dot >= 0) positiveSign = true;
                        else positiveSign = false;
                    }
                    if ((positiveSign && dot < 0) || (!positiveSign && dot >= 0)) {

                        Vector3 left = sweepVectorLeft.position;
                        Vector3 right = sweepVectorRight.position;
                        Vector3 leftToRight = (right - left).normalized;

                        Vector3 mouseToLeft = (mousePosition - left).normalized;
                        Vector3 mouseToRight = (mousePosition - right).normalized;

                        float mouseToLeftDot = Vector3.Dot(leftToRight, mouseToLeft);
                        float mouseToRightDot = Vector3.Dot(-leftToRight, mouseToRight);

                        if (mouseToLeftDot > 0f && mouseToRightDot > 0f) {
                            signSet = false;
                            if (positiveSign) StartCoroutine(SpinCylinder(true));
                            else StartCoroutine(SpinCylinder(false));
                        }
                    }
                }
            }
        }

        public void Spin(bool spinDown = true) {
            StartCoroutine(SpinCylinder(spinDown));
        }

        IEnumerator SpinCylinder(bool spinDown = true) {
            revolverScreen.HideSwipeIndicator();
            blockInput = true;

            float speed = -1f;
            if (!spinDown) speed = 1f;
            
            Options.PlayOneShot(cylinderSpinEvent, Options.AudioType.SFX);

            while (true) {
                yield return new WaitForSeconds(0.2f);
                cylinderAnimator.SetFloat("speed", speed);

                if (spinDown) speed += 0.1f;
                else speed -= 0.1f;

                if ((spinDown && speed >= 0.1f) || (!spinDown && speed <= -0.1f)) {
                    cylinderAnimator.SetFloat("speed", 0f);
                    cylinderAnimator.SetBool("spinned", true);
                    yield break;
                }
            }
        }
    }

}