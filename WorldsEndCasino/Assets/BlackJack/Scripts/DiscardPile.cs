﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscardPile : MonoBehaviour {

    #region private variables

    private bool shown = true;
    private bool isMoving = false;
    private float shownCoordsX = 0;
    private float hiddenCoordsX = 81;
    private float discardPileTogglingDuration = 0.3f;
    private Transform discardPile;

    #endregion

    void Awake () {
        discardPile = transform.GetChild(0);
    }

    #region private methods

    void UpdateDiscardPileUI(ref Dictionary<BJ_Game.Values, int> histogram)
    {
        string text = "";
        string temptext;
        int iterator;

        foreach (BJ_Game.Values value in Enum.GetValues(typeof(BJ_Game.Values)))
        {
            iterator = 0;
            temptext = "";

            while (iterator < histogram[value])
            {
                temptext += "I";
                ++iterator;
            }

            text += temptext + "\n";
        }

        Text t = transform.GetChild(0).GetChild(0).GetComponent<Text>();
        t.text = text;
    }

    #endregion

    #region public methods

    public void RefreshDiscardPile(ref List<Pair<BJ_Game.Suits, BJ_Game.Values>> discardDeck)
    {
        Dictionary<BJ_Game.Values, int> discardPileHistogram = new Dictionary<BJ_Game.Values, int>();
        foreach (BJ_Game.Values value in Enum.GetValues(typeof(BJ_Game.Values)))
        {
            discardPileHistogram[value] = 0;
        }

        for (int i = 0; i < discardDeck.Count; ++i)
        {
            ++discardPileHistogram[discardDeck[i].Second];
        }

        UpdateDiscardPileUI(ref discardPileHistogram);
    }

    public void ToggleDiscardPile()
    {
        StartCoroutine(Move());
    }

    #endregion

    #region coroutiones

    IEnumerator Move()
    {
        if (!isMoving)
        {
            float startingCoordsX, targetCoordsX; 
            isMoving = true;

            if (shown)
            {
                startingCoordsX = shownCoordsX;
                targetCoordsX = hiddenCoordsX;
            }
            else
            {
                startingCoordsX = hiddenCoordsX;
                targetCoordsX = shownCoordsX;
            }

            float timeElapsed = 0.0f;

            while (timeElapsed < discardPileTogglingDuration)
            {
                timeElapsed += Time.deltaTime;
                float localPositionX = Mathf.Lerp(startingCoordsX, targetCoordsX, timeElapsed / discardPileTogglingDuration);

                discardPile.localPosition = new Vector2(localPositionX, discardPile.localPosition.y);
                yield return null;
            }

            shown = !shown;

            isMoving = false;
        }

        yield return null;
    }

    #endregion
}
