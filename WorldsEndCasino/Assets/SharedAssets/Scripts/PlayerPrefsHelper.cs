﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsHelper {

    // Game Selection
    public static string GAME_KEY = "game";
    public static int RUSSIAN_ROULETTE = 0;
    public static int BLACK_JACK = 1;

    // Pre-game Setup
    public static string PLAYER_AMOUNT_KEY = "numberOfPlayers";
    public static string SURVIVOR_AMOUNT_KEY = "numberOfSurvivors";
    public static string PLAYER_NAME_KEY = "playerName";
    public static string RANDOMIZED_KEY = "randomizedPlayerOrder";

    // Options
    public static string VIBRATION_KEY = "vibration";
    public static string MUSIC_KEY = "music";
    public static string SFX_KEY = "sfx";
    public static string VOICE_ACTING_KEY = "voiceActing";

    public static void SaveGamePrefs(List<string> playerNames, bool turnOrderRandomized, int numberOfSurvivors = 1) {

        // Clear old prefs
        for (int i = 0; i < Constants.MAX_PLAYERS; ++i) {
            string indexString = i.ToString();
            PlayerPrefs.DeleteKey(PLAYER_NAME_KEY + indexString);
        }
        PlayerPrefs.DeleteKey(RANDOMIZED_KEY);

        // Save new prefs
        PlayerPrefs.SetInt(PLAYER_AMOUNT_KEY, playerNames.Count);
        PlayerPrefs.SetInt(SURVIVOR_AMOUNT_KEY, numberOfSurvivors);

        if (turnOrderRandomized) PlayerPrefs.SetInt(RANDOMIZED_KEY, 1);
        else PlayerPrefs.SetInt(RANDOMIZED_KEY, 0);

        for (int i = 0; i < playerNames.Count; ++i) {
            string indexString = i.ToString();
            PlayerPrefs.SetString(PLAYER_NAME_KEY + indexString, playerNames[i]);
        }
    }

    public static bool LoadGamePrefs(ref List<string> playerNames, ref int numberOfPlayers, ref int numberOfSurvivors, ref bool turnOrderRandomized) {
        int tempInt;
        string tempString;

        tempInt = PlayerPrefs.GetInt(PLAYER_AMOUNT_KEY, -666);
        if (tempInt == -666) return false;
        numberOfPlayers = tempInt;

        tempInt = PlayerPrefs.GetInt(SURVIVOR_AMOUNT_KEY, -666);
        if (tempInt == -666) return false;
        numberOfSurvivors = tempInt;

        tempInt = PlayerPrefs.GetInt(RANDOMIZED_KEY, -666);
        if (tempInt == -666) return false;
        else if (tempInt == 1) turnOrderRandomized = true;
        else turnOrderRandomized = false;

        for (int i = 0; i < numberOfPlayers; ++i) {
            string indexString = i.ToString();

            tempString = PlayerPrefs.GetString(PLAYER_NAME_KEY + indexString, null);
            if (tempString == null) return false;

            playerNames.Add(tempString);
        }
        return true;
    }

    // Obsolete
    public static void SaveGamePrefs(List<string> playerNames, int numberOfSurvivors = 1) {

        // Clear old prefs
        for (int i = 0; i < Constants.MAX_PLAYERS; ++i) {
            string indexString = i.ToString();
            PlayerPrefs.DeleteKey(PLAYER_NAME_KEY + indexString);
        }
        PlayerPrefs.DeleteKey(RANDOMIZED_KEY);

        PlayerPrefs.SetInt(PLAYER_AMOUNT_KEY, playerNames.Count);
        PlayerPrefs.SetInt(SURVIVOR_AMOUNT_KEY, numberOfSurvivors);

        for (int i = 0; i < playerNames.Count; ++i) {
            string indexString = i.ToString();
            PlayerPrefs.SetString(PLAYER_NAME_KEY + indexString, playerNames[i]);
        }
    }

    // Obsolete
    public static bool LoadGamePrefs(ref List<string> playerNames, ref int numberOfPlayers, ref int numberOfSurvivors) {
        int tempInt;
        string tempString;

        tempInt = PlayerPrefs.GetInt(PLAYER_AMOUNT_KEY, -666);
        if (tempInt == -666) return false;
        numberOfPlayers = tempInt;

        tempInt = PlayerPrefs.GetInt(SURVIVOR_AMOUNT_KEY, -666);
        if (tempInt == -666) return false;
        numberOfSurvivors = tempInt;
        
        for (int i = 0; i < numberOfPlayers; ++i) {
            string indexString = i.ToString();

            tempString = PlayerPrefs.GetString(PLAYER_NAME_KEY + indexString, null);
            if (tempString == null) return false;

            playerNames.Add(tempString);
        }
        return true;
    }
}
