﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnRandomizationScreen : MonoBehaviour, Zoomer.CallbackInterface {

    public interface CallbackInterface {

        // Called when the menu has fallen down beneath the screen after pressing "Let's go!"
        void OnHidden();
    }

    class HostFlyingAnimation : UtilityMethods.SlideCallback {

        TurnRandomizationScreen turnRandomizationScreen;
        GameObject host;
        Animator hostAnimator;
        string[] hostLineEvents;

        enum NextState { CENTER, OFF_SCREEN_TOP }
        NextState nextState = NextState.CENTER;

        public HostFlyingAnimation(TurnRandomizationScreen monoBehaviour, GameObject host, string []hostLineEvents, Animator hostAnimator) {
            this.turnRandomizationScreen = monoBehaviour;
            this.host = host;
            this.hostLineEvents = new string[hostLineEvents.Length];

            for (int i = 0; i < hostLineEvents.Length; ++i) {
                this.hostLineEvents[i] = hostLineEvents[i];
            }
            this.hostAnimator = hostAnimator;
        }

        public void StartAnimation() {
            host.SetActive(true);
            RectTransform hostRectTransform = host.GetComponent<RectTransform>();
            Vector3 destination = hostRectTransform.anchoredPosition;
            Vector3 position = destination;
            position.y = -700;
            host.GetComponent<RectTransform>().anchoredPosition = position;
            
            UtilityMethods.Slide(turnRandomizationScreen, hostRectTransform, destination, 15f, 1.5f, this);
        }

        public void OnSlideDone() {
            switch(nextState) {
                case NextState.CENTER:
                    hostAnimator.SetTrigger("Trigger");
                    host.GetComponent<HostFloater>().on = true;
                    turnRandomizationScreen.StartCoroutine(HostLine(this));
                    break;
                case NextState.OFF_SCREEN_TOP:
                    host.SetActive(false);
                    turnRandomizationScreen.OnDone();
                    break;
            }
        }

        IEnumerator HostLine(UtilityMethods.SlideCallback callbackInterface) {
            nextState = NextState.OFF_SCREEN_TOP;
            RectTransform hostRectTransform = host.GetComponent<RectTransform>();
            Vector3 destination = hostRectTransform.anchoredPosition;
            destination.y = 700;
            Options.PlayOneShot(hostLineEvents[UnityEngine.Random.Range(0, hostLineEvents.Length)], Options.AudioType.VOICE_ACTING);

            yield return new WaitForSeconds(2f);
            host.GetComponent<HostFloater>().on = false;
            hostAnimator.SetTrigger("Trigger");
            UtilityMethods.Slide(turnRandomizationScreen, hostRectTransform, destination, 40f, 1.5f, callbackInterface);

        }
    }

    #region Public Variables

    public RectTransform[] playerBoxes;

    [Space]
    [FMODUnity.EventRef]
    public string playerEntryEvent;

    [FMODUnity.EventRef]
    public string hereComesTheDeathRowEntryEvent;

    [FMODUnity.EventRef]
    public string deathRowEntryEvent;

    [FMODUnity.EventRef]
    public string[] hostLineEvents;

    [Tooltip("Brute force bitch! Length of the above ")]
    public float[] hostLineEventLengths;


    [FMODUnity.EventRef]
    public string[] players2Events;

    [FMODUnity.EventRef]
    public string[] players3Events;

    [FMODUnity.EventRef]
    public string[] players4Events;

    [FMODUnity.EventRef]
    public string[] players5Events;

    [FMODUnity.EventRef]
    public string[] players6Events;

    public RectTransform continueButton;

    [Space]
    public GameObject here;
    public GameObject comes;
    public GameObject the;
    public GameObject deathRow;

    [Space]
    public GameObject host;
    
    #endregion

    #region Private Variables

    int numberOfPlayers;
    Vector2 initialOffset = Vector2.zero;
    Vector3 initialScale = new Vector3(2f, 2f, 1f);
    Vector3 initialContinueButtonPosition;

    #endregion

    void Awake() {
        initialContinueButtonPosition = continueButton.anchoredPosition;
    }

    #region Public Methods

    public void Initialize(int numberOfPlayers) {
        this.numberOfPlayers = numberOfPlayers;

        here.SetActive(false);
        here.GetComponent<RectTransform>().localScale = initialScale;
        comes.SetActive(false);
        comes.GetComponent<RectTransform>().localScale = initialScale;
        the.SetActive(false);
        the.GetComponent<RectTransform>().localScale = initialScale;
        deathRow.SetActive(false);
        deathRow.GetComponent<RectTransform>().localScale = initialScale;
        deathRow.GetComponent<Zoomer>().RegisterCallbackInterface(this);

        for (int i = 0; i < Constants.MAX_PLAYERS; ++i) {
            playerBoxes[i].gameObject.SetActive(false);
            playerBoxes[i].localScale = initialScale;
        }

        continueButton.anchoredPosition = initialContinueButtonPosition;
        continueButton.gameObject.SetActive(false);
    }
        
    public void Show(Player[] player, bool firstRound = true) {
        for (int i = 0; i < player.Length; ++i) {
            Text[] texts = playerBoxes[i].GetComponentsInChildren<Text>();

            for (int j = 0; j < texts.Length; ++j) {
                if (texts[j].tag.Equals("PlayerRandomizationName")) {
                    texts[j].text = player[i].name;
                    break;
                }
            }
        }
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.offsetMin = initialOffset;
        rectTransform.offsetMax = initialOffset;

        gameObject.SetActive(true);

        if (firstRound) new HostFlyingAnimation(this, host, hostLineEvents, host.GetComponent<Animator>()).StartAnimation();
        else StartCoroutine(ShowTurnOrderAnimation(firstRound));
    }
    // if firstRound was true & host animation is done->
    public void OnDone() {
        StartCoroutine(ShowTurnOrderAnimation(true));
    }

    // callbackInterface's default value set null for backwards compatibility.
    //  You should definitelly set it for handling the transition to the next game state
    public void Hide(TurnRandomizationScreen.CallbackInterface callbackInterface = null) {
        StartCoroutine(FallDownAnimation(callbackInterface));
    }

    // Copied from BJ_Game.cs
    public void Shuffle<T>(ref List<T> list) {
        System.Random rng = new System.Random();

        int n = list.Count;
        while (n > 1) {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        n = list.Count;
    }


    #endregion

    #region Coroutines

    IEnumerator ShowTurnOrderAnimation(bool firstRound) {
        float headingZoomSpeed = 18f;
        float zoomSpeed = 5f;

        if (!firstRound) yield return new WaitForSeconds(0.8f);

        // HERE
        here.SetActive(true);
        here.GetComponent<Zoomer>().Zoom(1f, headingZoomSpeed);
        Options.PlayOneShot(hereComesTheDeathRowEntryEvent, Options.AudioType.SFX);
        yield return new WaitForSeconds(0.4f);

        // COMES
        comes.SetActive(true);
        comes.GetComponent<Zoomer>().Zoom(1f, headingZoomSpeed);
        Options.PlayOneShot(hereComesTheDeathRowEntryEvent, Options.AudioType.SFX);
        yield return new WaitForSeconds(0.4f);

        // THE
        the.SetActive(true);
        the.GetComponent<Zoomer>().Zoom(1f, headingZoomSpeed);
        Options.PlayOneShot(hereComesTheDeathRowEntryEvent, Options.AudioType.SFX);
        yield return new WaitForSeconds(0.4f);

        // DEATH ROW!
        deathRow.SetActive(true);
        deathRow.GetComponent<Zoomer>().Zoom(1f, headingZoomSpeed);
        Options.PlayOneShot(deathRowEntryEvent, Options.AudioType.SFX);
        yield return new WaitForSeconds(0.8f);

        for (int i = 0; i < numberOfPlayers; ++i) {
            playerBoxes[i].gameObject.SetActive(true);
            playerBoxes[i].GetComponent<Zoomer>().Zoom(1f, zoomSpeed);

            Options.PlayOneShot(playerEntryEvent, Options.AudioType.SFX);
            yield return new WaitForSeconds(0.2f);
        }

        //yield return new WaitForSeconds(0.8f);
        
        FMOD.Studio.EventInstance eventInstance = null;

        switch (numberOfPlayers) {
            case 2:
                eventInstance = FMODUnity.RuntimeManager.CreateInstance(players2Events[UnityEngine.Random.Range(0, players2Events.Length)]);
                break;
            case 3:
                eventInstance = FMODUnity.RuntimeManager.CreateInstance(players3Events[UnityEngine.Random.Range(0, players3Events.Length)]);
                break;
            case 4:
                eventInstance = FMODUnity.RuntimeManager.CreateInstance(players4Events[UnityEngine.Random.Range(0, players4Events.Length)]);
                break;
            case 5:
                eventInstance = FMODUnity.RuntimeManager.CreateInstance(players5Events[UnityEngine.Random.Range(0, players5Events.Length)]);
                break;
            case 6:
                eventInstance = FMODUnity.RuntimeManager.CreateInstance(players6Events[UnityEngine.Random.Range(0, players6Events.Length)]);
                break;
        }
        Options.StartFmodInstance(eventInstance, Options.AudioType.VOICE_ACTING);

        if (eventInstance != null) {
            FMOD.Studio.PLAYBACK_STATE playbackState;

            while (true) {
                eventInstance.getPlaybackState(out playbackState);
                if (playbackState != FMOD.Studio.PLAYBACK_STATE.STOPPED) yield return null;
                else break;
            }
        }
        eventInstance.release();
        continueButton.gameObject.SetActive(true);
        UtilityMethods.Slide(this, continueButton, new Vector3(0f, 42, 0f), 5f, 1f);
    }

    IEnumerator FallDownAnimation(TurnRandomizationScreen.CallbackInterface callbackInterface) {
        RectTransform rectTransform = GetComponent<RectTransform>();

        // offsetMax.y = top, offsetMin.y = bottom;
        Vector2 offsetMax = rectTransform.offsetMax;
        Vector2 offsetMin = rectTransform.offsetMin;

        while (offsetMax.y > -1000f) {
            offsetMax.y -= 1500f * Time.deltaTime;
            offsetMin.y -= 1500f * Time.deltaTime;

            rectTransform.offsetMax = offsetMax;
            rectTransform.offsetMin = offsetMin;
            yield return null;
        }
        gameObject.SetActive(false);

        if (callbackInterface != null) callbackInterface.OnHidden();
    }

    public void OnZoomedIn() {
    }

    public void OnZoomedOut() {
        deathRow.GetComponent<Shaker>().Shake();
    }

    #endregion
}
