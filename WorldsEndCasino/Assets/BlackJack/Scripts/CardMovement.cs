﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardMovement : MonoBehaviour {

    #region private variables

    private static readonly float cardMovementSpeed = 4f;// 0.096f;
    private static readonly float cardFlippingDuration = 0.7f;
    private static readonly float cardTurningDuration = 0.4f;
    private Vector2 destination;
    private CardsUI cardsUI;
    private bool cardShown;
    private bool cardMoving = false;

    private GameObject front;
    private GameObject back;

    #endregion

    #region public variables

    public bool cardTurning;

    #endregion

    #region monobehaviour callbacks

    private void Awake()
    {
        cardsUI = FindObjectOfType<CardsUI>();
        destination = transform.position;
        cardShown = false;

        front = transform.GetChild(0).gameObject;
        back = transform.GetChild(1).gameObject;

        transform.localEulerAngles = new Vector3(0, 0, -90);
    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	}

    #endregion

    #region public methods

    public void InitializeCard(BJ_Game.Suits suit, BJ_Game.Values value)
    {
        string cardName = "";
        Image i = front.GetComponent<Image>();
        Text[] texts = front.GetComponentsInChildren<Text>();

        // Put the correct suit image on the card itself.
        switch (suit)
        {
            case BJ_Game.Suits.DIAMONDS:
                i.sprite = Resources.Load<Sprite>("CardSuits/Diamonds");
                cardName = "DIAMONDS_";
                texts[0].color = new Color(1, 0, 0);
                break;
            case BJ_Game.Suits.HEARTS:
                i.sprite = Resources.Load<Sprite>("CardSuits/Hearts");
                cardName = "HEARTS_";
                texts[0].color = new Color(1, 0, 0);
                break;
            case BJ_Game.Suits.SPADES:
                i.sprite = Resources.Load<Sprite>("CardSuits/Spades");
                cardName = "SPADES_";
                texts[0].color = new Color(0, 0, 0);
                break;
            case BJ_Game.Suits.CLUBS:
                i.sprite = Resources.Load<Sprite>("CardSuits/Clubs");
                cardName = "CLUBS_";
                texts[0].color = new Color(0, 0, 0);
                break;
            default:
                break;
        }

        // Put the correct value symbol on the card itself.
        switch (value)
        {
            case BJ_Game.Values.TWO:
            case BJ_Game.Values.THREE:
            case BJ_Game.Values.FOUR:
            case BJ_Game.Values.FIVE:
            case BJ_Game.Values.SIX:
            case BJ_Game.Values.SEVEN:
            case BJ_Game.Values.EIGHT:
            case BJ_Game.Values.NINE:
                texts[0].text = ((int)value).ToString();
                break;
            case BJ_Game.Values.TEN:
                texts[0].text = " " + ((int)value).ToString();
                break;
            case BJ_Game.Values.JACK:
                texts[0].text = "J";
                break;
            case BJ_Game.Values.QUEEN:
                texts[0].text = "Q";
                break;
            case BJ_Game.Values.KING:
                texts[0].text = "K";
                break;
            case BJ_Game.Values.ACE:
                texts[0].text = "A";
                break;
        }

        texts[1].text = texts[0].text;
        texts[1].color = texts[0].color;
        cardName += value.ToString();
        gameObject.name = cardName;
    }

    public Vector2 GetDestination()
    {
        return destination;
    }

    public void SetDestination(Vector2 destinationVector)
    {
        if (destination != destinationVector && (Vector2)transform.localPosition != destinationVector)
        {
            destination = destinationVector;

            if (!cardMoving)
            {
                cardMoving = true;
                StartCoroutine(CardMoving());
            }
        }
    }

    public void SetDestination(BJ_Game.Decks deck)
    {
        switch (deck)
        {
            case BJ_Game.Decks.PLAYING:
                if (destination != CardsUI.playingDeckPosition)
                {
                    destination = CardsUI.playingDeckPosition;
                    cardTurning = true;
                    StartCoroutine(CardTurning(false));
                }
                break;
            case BJ_Game.Decks.DISCARD:
                destination = CardsUI.discardDeckPosition;
                break;
            case BJ_Game.Decks.PLAYER:
                destination = new Vector2(CardsUI.playerDeckPosition.x + cardsUI.cardOffsetInHand * cardsUI.playerDeck.Count, CardsUI.playerDeckPosition.y);
                cardTurning = true;
                StartCoroutine(CardTurning(true));
                break;
            case BJ_Game.Decks.HOST:
                destination = new Vector2(CardsUI.hostDeckPosition.x + cardsUI.cardOffsetInHand * cardsUI.hostDeck.Count, CardsUI.hostDeckPosition.y);
                cardTurning = true;
                StartCoroutine(CardTurning(true));
                break;
            default:
                break;
        }

        if (!cardMoving)
        {
            cardMoving = true;
            StartCoroutine(CardMoving());
        }
    }

    public void ShowCard(bool secondHostCard)
    {
        if (cardShown)
            return;

        StartCoroutine(CardFlipping(secondHostCard));
    }

    public void HideCard()
    {
        if (!cardShown)
            return;

        StartCoroutine(CardFlipping(false));
    }

    #endregion

    #region coroutines

    private IEnumerator CardMoving()
    {
        Vector2 currentDestination = destination;

        while ((Vector2)transform.localPosition != currentDestination)
        { // Move the card towards its destination, if it was not reached yet.
            transform.localPosition = Vector2.Lerp(transform.localPosition, currentDestination, cardMovementSpeed * Time.deltaTime);

            if (Vector2.Distance(transform.localPosition, currentDestination) < cardMovementSpeed * Time.deltaTime)
                transform.localPosition = currentDestination;

            yield return null;// new WaitForSeconds(0.01f);
            if (currentDestination != destination)
                currentDestination = destination;
        }
        cardMoving = false;
    }

    private IEnumerator CardFlipping(bool secondHostCard)
    {
        while (cardTurning) // DONT LOOK AT THIS CODE!!! IT NEVER HAPPENED!
            yield return null;

        bool cardStatus = cardShown;
        float timeElapsed = 0.0f;

        Options.PlayOneShot(Options.ChooseRandom(cardsUI.blackjack.cardFlippingSound), Options.AudioType.SFX);

        Vector3 rotation = new Vector3();

        while (timeElapsed < cardFlippingDuration)
        {
            timeElapsed += Time.deltaTime;

            if (timeElapsed < cardFlippingDuration / 2)
            {
                float rotationY = Mathf.Lerp(0, 180, timeElapsed / cardFlippingDuration);
                rotation.x = transform.rotation.x;
                rotation.y = rotationY;
                rotation.z = transform.rotation.z;
                transform.localEulerAngles = rotation;
            }
            else
            {
                float rotationY = Mathf.Lerp(180, 0, timeElapsed / cardFlippingDuration);
                rotation.x = transform.rotation.x;
                rotation.y = rotationY;
                rotation.z = transform.rotation.z;
                transform.localEulerAngles = rotation;
                if (cardStatus == cardShown)
                {
                    if (secondHostCard)
                        cardsUI.SwapCardsPosition(cardsUI.hostDeck[0].Second, cardsUI.hostDeck[1].Second);

                    cardStatus = !cardShown;
                    back.SetActive(!cardStatus);
                }
            }
            yield return null;
        }

        cardShown = cardStatus;
    }

    public IEnumerator CardTurning(bool toPortraitOrientation)
    {
        float timeElapsed = 0.0f;

        int startingAngle, endingAngle;

        if (toPortraitOrientation)
        {
            startingAngle = -90;
            endingAngle = 0;
        }
        else
        {
            startingAngle = 0;
            endingAngle = -90;
        }
        if (endingAngle != transform.localEulerAngles.z)
        {
            Vector3 rotation = new Vector3();

            while (timeElapsed <= cardTurningDuration)
            {
                timeElapsed += Time.deltaTime;

                float rotationZ = Mathf.Lerp(startingAngle, endingAngle, timeElapsed / cardTurningDuration);
                rotation.x = transform.rotation.x;
                rotation.y = transform.rotation.y;
                rotation.z = rotationZ;
                transform.localEulerAngles = rotation;

                yield return null;
            }
        }
        cardTurning = false;
    }

    #endregion
}
