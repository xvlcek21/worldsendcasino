﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyPopup : MonoBehaviour {

    public Color incrementOutlineColor;
    public Color decrementOutlineColor;

    enum Direction {
        UP, DOWN
    }

    const float OFFSET_Y = 20f;

    Text text;
    Color color = new Color(0f, 0f, 0f, 0f);
    Vector2 position;
    float initialPositionY;

	void Start () {
        text = GetComponent<Text>();
        text.color = color;
        position = GetComponent<RectTransform>().anchoredPosition;
        initialPositionY = position.y;
	}

    public void ShowIncrease(string amount) {
        text.text = "+ " + amount + " $";

        color.r = 0f;
        color.g = 1f;

        position.y = initialPositionY - OFFSET_Y;

        GetComponent<Outline>().effectColor = incrementOutlineColor;
        StartCoroutine(FadeInFadeOut(Direction.UP));
    }

    public void ShowDecrease(string amount) {
        text.text = "- " + amount + " $";

        color.r = 1f;
        color.g = 0f;

        position.y = initialPositionY + OFFSET_Y;

        GetComponent<Outline>().effectColor = decrementOutlineColor;
        StartCoroutine(FadeInFadeOut(Direction.DOWN));
    }
    
    IEnumerator FadeInFadeOut(Direction direction) {

        float positionIncrement;
        float alphaIncrement = 1.5f;

        switch(direction) {
            case Direction.UP:
                positionIncrement = 35f;
                break;
            case Direction.DOWN:
                positionIncrement = -35f;
                break;
            default:
                positionIncrement = 0f;
                break;
        }

        RectTransform rectTransform = GetComponent<RectTransform>();

        color.a = 0f;
        text.color = color;

        // Fade in
        while (true) {
            color.a += alphaIncrement * Time.deltaTime;
            if (color.a < 1f) {
                text.color = color;
                position.y += positionIncrement * Time.deltaTime;
                rectTransform.anchoredPosition = position;
            } else {
                color.a = 1f;
                text.color = color;
                break;
            }

            yield return null;
        }
        // Stay fully visible for a while
        for (int i = 0; i < 40; ++i) {
            position.y += positionIncrement * Time.deltaTime;
            rectTransform.anchoredPosition = position;

            yield return null;
        }
        // Fade out
        while (true) {
            color.a -= alphaIncrement * Time.deltaTime;

            if (color.a > 0f) {
                text.color = color;
                position.y += positionIncrement * Time.deltaTime;
                rectTransform.anchoredPosition = position;
            }
            else {
                color.a = 0f;
                text.color = color;
                break;
            }

            yield return null;
        }
    }
}