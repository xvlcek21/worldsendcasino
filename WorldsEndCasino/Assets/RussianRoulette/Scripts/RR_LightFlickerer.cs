﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RR_LightFlickerer : MonoBehaviour {

    public Sprite onSprite;
    public Sprite offSprite;

    enum CurrentSprite { ON, OFF };
    CurrentSprite currentSprite = CurrentSprite.OFF;

    float flickeringTime = 0f;

    void Start() {
        GetComponent<Image>().sprite = offSprite;
    }

	void Update() {
        // Adjust the magic numbers to the flickering
        if (flickeringTime <= 0f) {
            if (currentSprite == CurrentSprite.OFF) {
                GetComponent<Image>().sprite = onSprite;
                currentSprite = CurrentSprite.ON;
                flickeringTime = Random.Range(600f, 1500f) * Time.deltaTime;
            }
            else {
                GetComponent<Image>().sprite = offSprite;
                currentSprite = CurrentSprite.OFF;
                flickeringTime = Random.Range(5f, 70f) * Time.deltaTime;
            }
        }
        else flickeringTime -= 0.2f;
	}
}