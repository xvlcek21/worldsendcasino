﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RR_PickUpGun : MonoBehaviour {

    public GameObject gun;

	public void GunPickedUp(float x) {
        gun.SetActive(false);
    }
}
