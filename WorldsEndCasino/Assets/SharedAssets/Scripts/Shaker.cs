﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shaker : MonoBehaviour {

    #region private variables

    RectTransform body;
    Vector2 position = Vector2.zero;
    Vector2 initialPostition;
    Coroutine shakeCoroutine;

    #endregion

    #region public variables
    
    [Header("Try your luck")]
    public float movementSpeed;
    public float spread = 0.8f;
    public float interpolationSpeed;
    public float springToCenterSpeed = 60f;

    [Space]
    public Movement bodyMovement;

    #endregion

    void Awake() {
        body = GetComponent<RectTransform>();
        initialPostition = body.anchoredPosition;

        InitializeMovementClass(ref body, ref bodyMovement);
    }

    public void ShakeHorizontally() {
        if (shakeCoroutine != null) StopCoroutine(shakeCoroutine);
        shakeCoroutine = StartCoroutine(ShakeItBabyHorizontally());
    }
    IEnumerator ShakeItBabyHorizontally() {
        float timer = 0;

        // Shake for a while
        while (timer < 0.7f) {
            UpdateMovementSpeedHorizontally(ref body, ref bodyMovement);

            position.x = body.anchoredPosition.x + bodyMovement.movementSpeed;
            position.y = body.anchoredPosition.y;
            body.anchoredPosition = position;
            timer += Time.deltaTime;
            yield return null;
        }

        if (position.y < initialPostition.y) {
            while (position.x + springToCenterSpeed * Time.deltaTime < initialPostition.x) {
                position.x += springToCenterSpeed * Time.deltaTime;
                body.anchoredPosition = position;
                yield return null;
            }
        }
        else {
            while (position.x - springToCenterSpeed * Time.deltaTime > initialPostition.x) {
                position.x -= springToCenterSpeed * Time.deltaTime;
                body.anchoredPosition = position;
                yield return null;
            }
        }
        position.x = initialPostition.x;
        body.anchoredPosition = position;
    }

    public void Shake() {
        if (shakeCoroutine != null) StopCoroutine(shakeCoroutine);
        shakeCoroutine = StartCoroutine(ShakeItBaby());
    }
    IEnumerator ShakeItBaby() {
        float timer = 0;

        // Shake for a while
        while (timer < 0.7f) {
            UpdateMovementSpeed(ref body, ref bodyMovement);

            position.x = body.anchoredPosition.x;
            position.y = body.anchoredPosition.y + bodyMovement.movementSpeed;
            body.anchoredPosition = position;
            timer += Time.deltaTime;
            yield return null;
        }
        
        if(position.y < initialPostition.y) {
           while (position.y + springToCenterSpeed * Time.deltaTime < initialPostition.y) {
                position.y += springToCenterSpeed * Time.deltaTime;
                body.anchoredPosition = position;
                yield return null;
            }
        }
        else {
            while (position.y - springToCenterSpeed * Time.deltaTime > initialPostition.y) {
                position.y -= springToCenterSpeed * Time.deltaTime;
                body.anchoredPosition = position;
                yield return null;
            }
        }
        position.y = initialPostition.y;
        body.anchoredPosition = position;
    }

    #region private methods

    private void InitializeMovementClass(ref RectTransform t, ref Movement m) { // Since these properties are not going to be reused and/or modified anywhere else, we can justify leaving the initialisation here.
        m.movementSpeed = movementSpeed;
        m.initialMovementSpeed = m.movementSpeed;
        m.spread = spread;
        m.interpolationSpeed = interpolationSpeed;
        m.pivotPointY = t.anchoredPosition.y;
    }

    void UpdateMovementSpeed(ref RectTransform t, ref Movement m) {
        if (t.anchoredPosition.y > m.pivotPointY + m.spread) {
            if (m.movementSpeed > 0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, -m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);

        }
        else if (t.anchoredPosition.y < m.pivotPointY - m.spread) {
            if (m.movementSpeed < -0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);
        }
    }

    void UpdateMovementSpeedHorizontally(ref RectTransform t, ref Movement m) {
        if (t.anchoredPosition.x > m.pivotPointY + m.spread) {
            if (m.movementSpeed > 0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, -m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);

        }
        else if (t.anchoredPosition.x < m.pivotPointY - m.spread) {
            if (m.movementSpeed < -0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);
        }
    }

    #endregion
}