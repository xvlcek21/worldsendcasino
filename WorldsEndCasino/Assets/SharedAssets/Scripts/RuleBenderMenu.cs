﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class RuleBenderInfo {
    public string name;

    [FMODUnity.EventRef]
    public string[] hostLineEvent;

    [Tooltip("The sprite that pops up when you buy this rule bender")]
    public Sprite purchaseInfoSprite;
}

public class RuleBenderMenu : MonoBehaviour, UtilityMethods.FadeCallback {

    public interface CallbackInterface {

        // Called when the Change Your Fate/Cancel button is pressed
        // state: true if the menu is open, false if it is closed        
        // Opening of the menu is handled by RuleBenderMenu class so you can ignore this unless you need to change the UI when the menu opens/closes etc.
        void OnToggle(bool state);

        // The following callbacks are always called in this order after a succesfull transaction

        // 1. Called right after the player has pressed the buying button. Menu is not closed yet
        // You need to keep track of the player's spent money, and the updated cost of the used rule bender
        void OnRuleBenderBought(string ruleBender, int spentMoney, int updatedCost);

        // 2. Called when the menu was closed after a purchase
        void OnClosedAfterPurchase();

        // 3. Called after the purchase info and price decrease have been shown
        void OnInfoShown();
    }

    #region Public Variables

    // Some bad design here
    [Header("Drag the PurchaseInfo prefab to the scene and place it here")]
    public GameObject notEnoughMoney;
    public Image purchaseInfo;
    public GameObject purchaseInfoMaster;
    public GameObject shadow;

    [Space]
    public MoneyPopup moneyPopupScript;

    [Space]
    public RuleBenderInfo[] ruleBenderInfos;

    [Space]
    public Material CanBuyPrice;
    public Material CantBuyPrice;
    public Material CanBuyName;
    public Material CantBuyName;

    [Header("No need to change these settings")]
    public GameObject ruleBenderPrefab;
    public Sprite OpenRuleBenderMenuSprite;
    public Sprite CloseRuleBenderMenuSprite;
    public Image toggleMenuButton;
    public GameObject theMenu;

    [FMODUnity.EventRef]
    public string priceIncreaseEvent;

    [FMODUnity.EventRef]
    public string[] NotEnoughMoneyEvents;

    public bool pulsatePriceOnPurchase = false;

    #endregion

    #region Private Variables

    GameObject[] ruleBenderObjects;

    const int PRICE_INCREASE = 2000;

    int playerMoney;
    int[] prices;

    bool inputLocked = false;
    bool fadeIn = false;

    RuleBenderMenu.CallbackInterface callbackInterface;

    Coroutine purchaseInfoCoroutine;

    FMOD.Studio.EventInstance notEnoughMoneyInstance = null;

    #endregion

    #region Public Interface

    void OnDestroy() {
        if (notEnoughMoneyInstance != null) {
            notEnoughMoneyInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            notEnoughMoneyInstance.release();
        }
    }

    // Call on Awake (Whats the point off this?)
    public void Initialize(RuleBenderMenu.CallbackInterface callbackInterface, int priceIncrease = PRICE_INCREASE) {
        this.callbackInterface = callbackInterface;

        purchaseInfo.gameObject.SetActive(false);
        notEnoughMoney.SetActive(false);

        ruleBenderObjects = new GameObject[ruleBenderInfos.Length];

        for (int i = 0; i < ruleBenderObjects.Length; ++i) {
            ruleBenderObjects[i] = Instantiate(ruleBenderPrefab, theMenu.transform);

            // Stack rule bender buttons from top to bottom
            Vector3 position = ruleBenderObjects[i].GetComponent<RectTransform>().anchoredPosition;
            position.x = 0f;
            if (ruleBenderInfos.Length > 2) // There are less rule benders on the blackjack rule bender screen, they have to be repositioned.
                position.y = i * -100f;
            else
                position.y = (i + 1) * -100f;
            ruleBenderObjects[i].GetComponent<RectTransform>().anchoredPosition = position;

            // Set the name and initial price of the rule bender
            Text[] texts = ruleBenderObjects[i].GetComponentsInChildren<Text>();

            for (int j = 0; j < texts.Length; ++j) {
                if (texts[j].CompareTag("RuleBenderName")) {
                    texts[j].text = ruleBenderInfos[i].name;
                    break;
                }
            }
            int index = i;// needs a temp variable in same scope as AddListener method for the i to be valid
            ruleBenderObjects[i].GetComponent<Button>().onClick.AddListener(delegate { OnBuy(this.ruleBenderInfos[index].name, index, priceIncrease); });
        }
    }

    // Loads the prices of the rule benders.
    // Call this before the player's turn starts
    public void Prepare(int playerMoney, int[] updatedPrices) {
        inputLocked = false;
        toggleMenuButton.sprite = OpenRuleBenderMenuSprite;

        this.playerMoney = playerMoney;
        this.prices = new int[updatedPrices.Length];

        for (int i = 0; i < prices.Length; ++i) {
            prices[i] = updatedPrices[i];
        }

        for (int i = 0; i < ruleBenderObjects.Length; ++i) {
            Text[] texts = ruleBenderObjects[i].GetComponentsInChildren<Text>();

            for (int j = 0; j < texts.Length; ++j) {
                if (texts[j].CompareTag("RuleBenderPrice")) {
                    if (playerMoney >= updatedPrices[i]) texts[j].material = CanBuyPrice;
                    else texts[j].material = CantBuyPrice;

                    texts[j].text = updatedPrices[i].ToString() + " $";
                    break;
                }
                else if (playerMoney >= updatedPrices[i]) texts[j].material = CanBuyName;
                else texts[j].material = CantBuyName;
            }
        }
    }

    // Close the menu by force if it's open when the turn changes.
    // Opening/Closing the menu with player input is handled automatically by this class
    public void Close() {
        toggleMenuButton.sprite = OpenRuleBenderMenuSprite;
        theMenu.SetActive(false);
    }

    public void Open()
    {
        toggleMenuButton.sprite = CloseRuleBenderMenuSprite;
        theMenu.SetActive(true);
    }

    #endregion

    #region Callbacks

    // Change your fate/Cancel button onClick
    public void ToggleMenu() {
        if (inputLocked) return;

        if (!theMenu.activeSelf) {
            if (purchaseInfoCoroutine != null) {
                StopCoroutine(purchaseInfoCoroutine);
                purchaseInfo.gameObject.SetActive(false);
                notEnoughMoney.SetActive(false);
            }
            Open();
        } else {
            Close();
        }
    }

    public void ShutUp() {
        if (notEnoughMoneyInstance != null) notEnoughMoneyInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    // Also closes the menu and displays purchase info on the screen
    public void OnBuy(string ruleBenderName, int id, int priceIncrease) {
        ShutUp();

        int price = prices[id];

        // Not enough money
        if (price > playerMoney) {
            notEnoughMoneyInstance = FMODUnity.RuntimeManager.CreateInstance(Options.ChooseRandom(NotEnoughMoneyEvents));
            notEnoughMoneyInstance.start();
            Close();
            purchaseInfoCoroutine = StartCoroutine(ShowNotEnoughMoney());
            return;
        }
        if (inputLocked) return;
        inputLocked = true;

        // Make the purchase and inform others about it
        callbackInterface.OnRuleBenderBought(ruleBenderName, price, price + priceIncrease);

        Text[] texts = ruleBenderObjects[id].GetComponentsInChildren<Text>();

        for (int i = 0; i < texts.Length; ++i) {
            if (texts[i].CompareTag("RuleBenderPrice")) {
                StartCoroutine(AnimatePlaySoundsAndCloseMenu(ruleBenderInfos[id].purchaseInfoSprite, texts[i], price + priceIncrease, price, ruleBenderInfos[id].hostLineEvent));
                break;
            }
        }
    }

    public bool isInputLocked()
    {
        return inputLocked;
    }

    public void setInputLocked(bool x)
    {
        inputLocked = x;
    }

    #endregion

    #region Coroutines

    public IEnumerator ShowNotEnoughMoney (string obsolete=null) {
        notEnoughMoney.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        notEnoughMoney.SetActive(false);
    }

    // Called just after callbackInterface.OnRuleBenderBought
    IEnumerator AnimatePlaySoundsAndCloseMenu(Sprite purchaseInfoSprite, Text priceText, int updatedPrice, int moneySpent, string[] hostLineEvent) {
        Options.PlayOneShot(priceIncreaseEvent, Options.AudioType.SFX);

        fadeIn = true;
        UtilityMethods.FadeCanvasGroupVisible(this, true, purchaseInfoMaster, 0.02f, this);

        priceText.text = updatedPrice.ToString() + " $";

        if (pulsatePriceOnPurchase) UtilityMethods.Pulsate(this, priceText.rectTransform);

        yield return new WaitForSeconds(0.5f);
        if (hostLineEvent != null && hostLineEvent.Length > 0)
            Options.PlayOneShot(hostLineEvent[UnityEngine.Random.Range(0, hostLineEvent.Length)], Options.AudioType.VOICE_ACTING);

        yield return new WaitForSeconds(0.3f);

        //Close();
        callbackInterface.OnClosedAfterPurchase();

        purchaseInfo.sprite = purchaseInfoSprite;

        Vector3 scale = purchaseInfo.GetComponent<RectTransform>().localScale;
        scale.x = 0.5f;
        scale.y = 0.5f;
        purchaseInfo.GetComponent<RectTransform>().localScale = scale;
        purchaseInfo.gameObject.SetActive(true);
        purchaseInfo.GetComponent<Zoomer>().Zoom(1f, 5f);

        yield return new WaitForSeconds(1.5f);

        fadeIn = false;
        UtilityMethods.FadeCanvasGroupVisible(this, false, purchaseInfoMaster, 0.2f, this);

        Close();
        //moneyPopupScript.ShowDecrease(moneySpent.ToString());
        callbackInterface.OnInfoShown();
    }

    public void OnFadeDone() {
        switch(fadeIn) {
            case true:
                break;
            case false:
                purchaseInfo.gameObject.SetActive(false);
                break;
        }
    }

    #endregion
}