﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostFloater : MonoBehaviour {

    #region private variables
    
    Transform body;
    Vector2 position = Vector2.zero;

    #endregion

    #region public variables

    public Movement bodyMovement;
    public bool on = true;

    #endregion

    void Start() {
        body = transform;

        InitializeMovementClass(ref body, ref bodyMovement);
    }

    void Update() {
        if (!on) return;
        UpdateMovementSpeed(ref body, ref bodyMovement);
        // Move
        position.x = body.localPosition.x;
        position.y = body.localPosition.y + bodyMovement.movementSpeed;
        body.localPosition = position;
    }

    #region private methods

    private void InitializeMovementClass(ref Transform t, ref Movement m) { // Since these properties are not going to be reused and/or modified anywhere else, we can justify leaving the initialisation here.
        m.movementSpeed = 0.3f;
        m.initialMovementSpeed = m.movementSpeed;
        m.spread = 0.8f;
        m.interpolationSpeed = 1.2f;
        m.pivotPointY = t.localPosition.y;
    }

    void UpdateMovementSpeed(ref Transform t, ref Movement m) {
        if (t.localPosition.y > m.pivotPointY + m.spread) {
            if (m.movementSpeed > 0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, -m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);

        }
        else if (t.localPosition.y < m.pivotPointY - m.spread) {
            if (m.movementSpeed < -0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);
        }
    }

    #endregion
}
