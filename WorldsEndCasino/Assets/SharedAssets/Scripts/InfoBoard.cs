﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBoard : MonoBehaviour {

    public Text infoText;
    public Text moneyText;
    public Animator infoBoardAnimator;

    Color infoColor;
    Color moneyColor;

    Coroutine infoCoroutine;
    Coroutine moneyCoroutine;

    void Awake() {
        infoColor = infoText.color;
        moneyColor = moneyText.color;
    }

    public void EraseInfo() {
        if (infoCoroutine != null) StopCoroutine(infoCoroutine);
        infoCoroutine = StartCoroutine(FadeOutInfo());
    }

    public void EraseMoney() {
        if (moneyCoroutine != null) StopCoroutine(moneyCoroutine);
        moneyCoroutine = StartCoroutine(FadeOutMoney());
    }

    public void SetInfoAndFlicker(string info) {
        UtilityMethods.Pulsate(this, infoText.rectTransform, null, true);
        infoText.text = info;
        infoBoardAnimator.SetBool("Flicker", true);
    }

    public void SetInfo(string info) {
        UtilityMethods.Pulsate(this, infoText.rectTransform, null, true);
        infoText.text = info;
    }

    public void SetMoney(int money) {
        if (moneyCoroutine != null) StopCoroutine(moneyCoroutine);
        moneyCoroutine = StartCoroutine(FadeInMoney(money));
    }
    
    public void SetMoneyAndPulsate(int money) {
        UtilityMethods.Pulsate(this, moneyText.rectTransform, null, true);
        moneyText.text = money.ToString() + " $";
    }

    IEnumerator FadeInMoney(int money) {
        moneyColor.a = 0f;
        moneyText.color = moneyColor;
        moneyText.text = money.ToString() + " $";

        while (moneyColor.a < 1f) {
            moneyColor.a += 2f * Time.deltaTime;
            moneyText.color = moneyColor;
            yield return null;
        }
        moneyColor.a = 1f;
        moneyText.color = moneyColor;
    }

    IEnumerator FadeOutInfo() {
        while (infoColor.a > 0f) {
            infoColor.a -= 2f * Time.deltaTime;
            infoText.color = infoColor;
            yield return null;
        }
        infoText.text = "";
        infoColor.a = 1f;
        infoText.color = infoColor;
    }

    IEnumerator FadeOutMoney() {
        while (moneyColor.a > 0f) {
            moneyColor.a -= 8f * Time.deltaTime;
            moneyText.color = moneyColor;
            yield return null;
        }
        moneyText.text = "";
        moneyColor.a = 1f;
        moneyText.color = infoColor;
    }
}