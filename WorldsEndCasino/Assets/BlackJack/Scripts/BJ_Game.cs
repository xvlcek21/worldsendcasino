﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class BJ_Game : MonoBehaviour, BackButtonConfirmation.BackButtonCallbackInterface, Timer.CallbackInterface, RuleBenderMenu.CallbackInterface,
    DeathScreen.CallbackInterface, UtilityMethods.SlideCallback, UtilityMethods.FadeCallback {

    class BJ_Player : Player
    {
        public int doubleDownCost;
        public int switchHandsCost;
        public int undoTheHitCost;
        public int passTheBustCost;
        public int passTheBustCostIncrease;

        public List<Pair<Suits, Values>> bustPlayerHand;
        public List<Pair<Suits, Values>> bustHostHand;

        public List<Pair<string, GameObject>> bustPlayerDeck;
        public List<Pair<string, GameObject>> bustHostDeck;

        public BJ_Player(int turnIndex, int money, string name) : base(turnIndex, money, name)
        {
            doubleDownCost = 1000;
            switchHandsCost = 1000;
            undoTheHitCost = 1000;
            passTheBustCost = 1000;
            passTheBustCostIncrease = 1000;

            bustPlayerHand = new List<Pair<Suits, Values>>();
            bustHostHand = new List<Pair<Suits, Values>>();

            bustPlayerDeck = new List<Pair<string, GameObject>>();
            bustHostDeck = new List<Pair<string, GameObject>>();
        }
    }

    enum RuleBending
    {
        PRE_DEAL,
        POST_DEAL
    }

    enum TurnAction
    {
        NONE,
        DOUBLE_DOWN,
        SWITCH_HANDS,
        UNDO_THE_HIT,
        PASS_THE_BUST
    }

    #region card definitions

    public enum Suits
    {
        DIAMONDS,
        HEARTS,
        SPADES,
        CLUBS
    }

    public enum Values
    {
        ACE,
        TWO = 2,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        TEN,
        JACK,
        QUEEN,
        KING
    }

    public enum Decks
    {
        PLAYER,
        HOST,
        PLAYING,
        DISCARD
    }

    public enum SumCountingMethod
    {
        MIN,
        MAX,
        BEST
    }

    public enum HandEndState
    {
        PLAYER_WON,
        PLAYER_BLACKJACK,
        HOST_WON,
        HOST_BLACKJACK,
        PUSH
    }

    const int DECK_SIZE = 52;

    List<Pair<Suits, Values>> playingDeck;
    List<Pair<Suits, Values>> discardDeck;

    List<Pair<Suits, Values>> playerHand;
    List<Pair<Suits, Values>> hostHand;

    #endregion

    #region private variables

    // Constants
    const int STARTING_TIME = 7;
    const float TICK_DURATION = 1.5f;
    const float FADE_SPEED = 0.2f;
    const float FADE_SPEED_SLOW = 0.008f;
    const float BUTTON_FLIPPING_DURATION = 0.3f;

    // player related variables
    private int numberOfPlayers;
    private int numberOfSurvivors;
    private List<BJ_Player> alivePlayers;
    private List<BJ_Player> deadPlayers;
    private bool isPlayerOrderRandomized;
    private TurnAction turnAction;

    private const int priceIncreaseModifier = 0;

    private int currentPlayerID;
    private const int hostID = -1;

    private bool lastPlayerDied;
    private bool bustPassed = false;
    private bool preDealPhase = true;

    private MoneyManager moneyManager;
    private CardsUI cardsUI;

    // current hand related variables
    private bool lettingGo = false;
    private bool switchingHands = false;
    private bool undoingTheHit = false;
    private bool bustPassing = false;
    private bool doublingDown = false;
    private bool samePlayerDied = false; // initiator died to his own passed bust
    private bool isHandDealt;
    private bool isTurnOrderReversed;
    private int bustPassingIndex = 1;

    // game flow related variables
    private bool isUILocked;
    private int voiceLineRandomizingCounter = 0;

    // ambient music
    FMOD.Studio.EventInstance blackjackThemeInstance;
    FMOD.Studio.EventInstance passTheBustThemeInstance;

    #endregion

    #region sounds
    [Space]
    [Header("Sounds")]
    [Space]

    [Header("Music")]

    [FMODUnity.EventRef]
    public string blackjackTheme; // plays during the hit/stand part of the game, only ambient during pre- and post-deal rule bender usage sections

    [FMODUnity.EventRef]
    public string passTheBustTheme; // plays during PTB sequence

    [Header("SFX")]

    [FMODUnity.EventRef]
    public string passTheBustTransitionSound; // selfexplanatory

    [FMODUnity.EventRef]
    public string[] cardFlippingSound; // Card flapping sounds when cards are moved on the table or when flipping from face down position to face up position

    [FMODUnity.EventRef]
    public string bustSound; // The respective sound effects for both Bust and Blackjack that are played at the same time as the respective voice acted bits, just with a bit lower volume

    [FMODUnity.EventRef]
    public string blackjackSound;

    [Header("VoiceActing")]

    [FMODUnity.EventRef]
    public string[] hitStandSequenceStartVoiceLine;

    [FMODUnity.EventRef]
    public string[] playerWonVoiceLine;

    [FMODUnity.EventRef]
    public string[] hostWonVoiceLine;

    [FMODUnity.EventRef]
    public string[] playerBlackjackVoiceLine;

    [FMODUnity.EventRef]
    public string[] hostBlackjackVoiceLine;

    [FMODUnity.EventRef]
    public string[] pushVoiceLine;

    [FMODUnity.EventRef]
    public string[] passTheBustVoiceLine; // When passing the Bust hand to other players, every time another player receives the Bust hand 
                                      // OR only at the start of the sequence, whichever works better / is less annoying:

    [FMODUnity.EventRef]
    public string[] takeItVoiceLine;

    [FMODUnity.EventRef]
    public string[] deathBrokeVoiceLine;

    [Space]
    [Header("Gameplay")]
    #endregion

    #region Convenient Stuff To Set In The Editor

    public string backButtonSceneName;

    #endregion

    #region UI elements

    public Timer timer;
    public const float passingTheBustSequenceDuration = 1.0f;
    public const float UIIndicationDelayDuration = 1.0f;
    public const float UIIndicationZoomInDuration = 0.6f;
    public const float UIIndicationFadeOutDuration = 0.8f;
    public const float UIIndicationShownDuration = 1.0f;
    public HandSwipeTransition handSwipeTransition;
    public BJ_Host host;
    public PlayerInfoBoxes playerInfoBoxes;
    public MoneyPopup moneyPopup;
    public BackButtonConfirmation backButtonConfirmation;
    public TurnRandomizationScreen turnRandomizationScreen;
    public DeathScreen deathScreen;
    public BJ_Hand hand;
    public RetryScreen retryScreen;
    public Text deathPanelInstructions;
    public Text centerIndicationText;
    public GameObject mainButtons;
    public GameObject bustScreen;
    public GameObject blackjackScreen;
    public GameObject hitButton;
    public GameObject standButton;
    public GameObject dealWithItButton;
    public GameObject bendTheRulesButton;
    public GameObject takeItButton;
    public GameObject passItButton;
    public GameObject everythingButBackground;
    public GameObject darkness;
    public GameObject cardPrefab;
    public GameObject currentPlayerUi;
    public GameObject discardPile;
    public GameObject preDealRuleBenderMenu;
    public GameObject postDealRuleBenderMenu;

    public GameObject background;
    public GameObject table;
    public GameObject hostImage;

    public InfoBoard infoBoard;

    #endregion

    #region monobehavior callbacks

    private void Awake() {
        isUILocked = true;
        backButtonConfirmation.RegisterCallbackInterface(this);
        InitializeGame();
        AssembleDecks(ref playingDeck, ref discardDeck);
        discardPile.GetComponent<DiscardPile>().RefreshDiscardPile(ref discardDeck);
        preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Initialize(this, 0);
        postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Initialize(this, 0);
        currentPlayerUi.GetComponent<CanvasGroup>().alpha = 0f;// Switches it invisible

        blackjackThemeInstance = FMODUnity.RuntimeManager.CreateInstance(blackjackTheme);
        passTheBustThemeInstance = FMODUnity.RuntimeManager.CreateInstance(passTheBustTheme);

        Options.StartFmodInstance(blackjackThemeInstance, Options.AudioType.MUSIC);
    }

    void Start() {
        Shuffle(ref playingDeck);

        isUILocked = true;
        if (!Convert.ToBoolean(PlayerPrefs.GetInt("BJRetry", 0))) handSwipeTransition.EnterScene(this);
        else handSwipeTransition.DestroyGameObject();
    }
    // handSwipeTransition.EnterScene(this) callback->
    public void OnSlideDone() {
        handSwipeTransition.DestroyGameObject();
    }

    void Update() {
    }

    void OnDestroy() {
        // Stop and release all FMOD resources here
        blackjackThemeInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        passTheBustThemeInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        blackjackThemeInstance.release();
        passTheBustThemeInstance.release();

        timer.ReleaseResources();
    }

    #endregion

    #region private methods

    void InitializeGame()
    {
        isHandDealt = lastPlayerDied = isTurnOrderReversed = false;
        playerHand = new List<Pair<Suits, Values>>();
        hostHand = new List<Pair<Suits, Values>>();

        // register players, set up everything
        moneyManager = new MoneyManager();
        moneyManager.Init(ref moneyPopup);

        SetUpPlayers();

        cardsUI = FindObjectOfType<CardsUI>();

        dealWithItButton.transform.parent.gameObject.SetActive(false);
        bendTheRulesButton.transform.parent.gameObject.SetActive(false);
        takeItButton.transform.parent.gameObject.SetActive(false);
        passItButton.transform.parent.gameObject.SetActive(false);
    }

    void SetUpPlayers()
    {
        List<string> playerNames = new List<string>();
        alivePlayers = new List<BJ_Player>(numberOfPlayers);
        deadPlayers = new List<BJ_Player>(numberOfPlayers);

        currentPlayerID = 0;

        PlayerPrefsHelper.LoadGamePrefs(ref playerNames, ref numberOfPlayers, ref numberOfSurvivors);

        for (int i = 0; i < numberOfPlayers; ++i)
            alivePlayers.Add(new BJ_Player(i, moneyManager.GetStartingBalance(), playerNames[i]));

        isPlayerOrderRandomized = Convert.ToBoolean(PlayerPrefs.GetInt("randomizedPlayerOrder"));

        if (isPlayerOrderRandomized)
            Shuffle(ref alivePlayers);

        turnRandomizationScreen.Initialize(alivePlayers.Count);
        bool afterRetry = !Convert.ToBoolean(PlayerPrefs.GetInt("BJRetry", 0));

        turnRandomizationScreen.Show(alivePlayers.ToArray(), afterRetry);
        host.FadeOut(FADE_SPEED);

        // player list GUI initialization
        playerInfoBoxes.Initialize(numberOfPlayers);

        for (int playerIndex = 0; playerIndex < numberOfPlayers; ++playerIndex)
            playerInfoBoxes.SetInfo(playerIndex, alivePlayers[playerIndex].name, alivePlayers[playerIndex].money.ToString());

    }

    void AssembleDecks(ref List<Pair<Suits, Values>> d, ref List<Pair<Suits, Values>> dd)
    {
        // Playing deck.
        d = new List<Pair<Suits, Values>>(DECK_SIZE);
        // Discard deck.
        dd = new List<Pair<Suits, Values>>(DECK_SIZE);

        // Fill the dictionary with cards.
        foreach (Suits suit in Enum.GetValues(typeof(Suits)))
        {
            foreach (Values value in Enum.GetValues(typeof(Values)))
            {
                d.Add(new Pair<Suits, Values>(suit, value));
            }
        }
    }

    void Shuffle<T>(ref List<T> list)
    {
        System.Random rng = new System.Random();

        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }

        n = list.Count;
    }

    bool ReshuffleNeeded()
    { // Here, we simulate the worst case scenario for dealing (the most possible cards used for the upcoming hand).
      // This helps us determine, if the remaining deck needs to be reshuffled, since we could run out of cards.
        List<Pair<Suits, Values>> tempPlayerDeck = new List<Pair<Suits, Values>>();
        List<Pair<Suits, Values>> tempHostDeck = new List<Pair<Suits, Values>>();

        if (playingDeck.Count < 4)
            return true;

        bool enoughCardsForPlayer = true;
        bool enoughCardsForHost = true;

        int index = 0;
        tempPlayerDeck.Add(playingDeck[index++]);
        tempHostDeck.Add(playingDeck[index++]);
        tempPlayerDeck.Add(playingDeck[index++]);
        tempHostDeck.Add(playingDeck[index++]);

        // Check the worst case scenario for the player (most cards used, before the player has blackjack or goes bust). 
        while (GetCardSum(ref tempPlayerDeck, SumCountingMethod.BEST) != 21 && GetCardSum(ref tempPlayerDeck, SumCountingMethod.MIN) < 21)
        {
            if (index < playingDeck.Count)
                tempPlayerDeck.Add(playingDeck[index++]);
            else
            {
                enoughCardsForPlayer = false;
                break;
            }
        }

        // Check the worst case scenario from the remaining cards for the host (most cards used, before the host ends his turn). 
        // We have to consider the MIN metric instead of BEST, because of the possible 1 or 11 ace value interpretation depending
        // on the amount of cards, that the player takes. Consider the following situation:
        // Player total so far: 15.
        // Host total so far: 5.
        // The remaining deck card values: SIX, ACE, TWO, ACE.
        // Depending on if the player takes another card (SIX) or not, the host could end up not being able to draw more cards.
        // Hence we chose the approach described above, so we do not risk running out of cards at any time.
        while (GetCardSum(ref tempHostDeck, SumCountingMethod.MIN) < 17)
        {
            if (index < playingDeck.Count)
                tempHostDeck.Add(playingDeck[index++]);
            else
            {
                enoughCardsForHost = false;
                break;
            }
        }



 //       Debug_PrintReshuffleHands(ref tempPlayerDeck, ref tempHostDeck);
        // If the worst case scenario does not meet the requirements, we need to reshuffle!
        return !(enoughCardsForPlayer && enoughCardsForHost);
    }

    void Reshuffle()
    {
        while (discardDeck.Count > 0)
        {
            playingDeck.Add(discardDeck[0]);
            discardDeck.RemoveAt(0);
        }

        Shuffle(ref playingDeck);
        StartCoroutine(DelayedReshuffle());
    }

    void DealHand()
    {
        // Empty the hand containers first.
        if (playerHand.Count > 0)
            playerHand.Clear();
        if (hostHand.Count > 0)
            hostHand.Clear();

        // The card are dealt in a specific order in blackjack, which is as follows.
        Hit(currentPlayerID);
        Hit(hostID);
        Hit(currentPlayerID);
        Hit(hostID);

        isHandDealt = true;

 //       Debug_PrintHands();
    }

    void DealBustHand()
    {
        // Empty the hand containers first.
        if (playerHand.Count > 0)
            playerHand.Clear();
        if (hostHand.Count > 0)
            hostHand.Clear();

        // Recreate the taken bust hand.

        while(alivePlayers[currentPlayerID].bustPlayerHand.Count != 0)
            BustHit(currentPlayerID);

        while (alivePlayers[currentPlayerID].bustHostHand.Count != 0)
            BustHit(hostID);

        // Fix the card positions due to immediate dealing and showing of the host hand.
        cardsUI.SwapCardsPosition(cardsUI.hostDeck[0].Second, cardsUI.hostDeck[1].Second);

        isHandDealt = true;

        alivePlayers[currentPlayerID].bustPlayerHand.Clear();
        alivePlayers[currentPlayerID].bustHostHand.Clear();

        StartCoroutine(cardsUI.DelayedUpdatePlayerHandTotalText(GetCardSum(ref playerHand, SumCountingMethod.BEST), 1));
        StartCoroutine(cardsUI.DelayedUpdateHostHandTotalText(GetCardSum(ref hostHand, SumCountingMethod.BEST), 1));

//        Debug_PrintHands();

        StartCoroutine(UIIndicate("BUST"));
    }

    // Dealing a single card to a player, or the host, if the ID is not specified.
    void Hit(int playerID)
    {
        if (playerID != hostID)
        {
            if (IsBust(ref playerHand) || Is21(ref playerHand))
                return;
            playerHand.Add(playingDeck[0]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.playingDeck, Decks.PLAYER);
        }
        else
        {
            hostHand.Add(playingDeck[0]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.playingDeck, Decks.HOST);

            if (hostHand.Count <= 2)
                StartCoroutine(cardsUI.DelayedUpdateHostHandTotalText(GetCardValue(hostHand[0].Second, SumCountingMethod.MAX), 1));
            else
                StartCoroutine(cardsUI.DelayedUpdateHostHandTotalText(GetCardSum(ref hostHand, SumCountingMethod.BEST), 1));
        }

        playingDeck.RemoveAt(0);

        StartCoroutine(cardsUI.DelayedUpdatePlayerHandTotalText(GetCardSum(ref playerHand, SumCountingMethod.BEST), 1));

        if ((playerID != hostID) && (IsBust(ref playerHand) || Is21(ref playerHand)) && !doublingDown)
        {
            OnStandLocked();
        }
    }

    void JustHit(int playerID)
    {
        playerHand.Add(playingDeck[0]);
        cardsUI.MoveCardFromToDeck(ref cardsUI.playingDeck, Decks.PLAYER);
        playingDeck.RemoveAt(0);

        StartCoroutine(cardsUI.DelayedUpdatePlayerHandTotalText(GetCardSum(ref playerHand, SumCountingMethod.BEST), 1));
    }

    void BustHit(int playerID)
    {
        if (playerID != hostID)
        {
            playerHand.Add(alivePlayers[playerID].bustPlayerHand[0]);
            cardsUI.MoveCardFromToDeck(ref alivePlayers[playerID].bustPlayerDeck, Decks.PLAYER);
            alivePlayers[playerID].bustPlayerHand.RemoveAt(0);
        }
        else
        {
            hostHand.Add(alivePlayers[currentPlayerID].bustHostHand[0]);
            cardsUI.MoveCardFromToDeck(ref alivePlayers[currentPlayerID].bustHostDeck, Decks.HOST);
            alivePlayers[currentPlayerID].bustHostHand.RemoveAt(0);
        }
    }

    int GetCardSum(ref List<Pair<Suits, Values>> hand, SumCountingMethod method)
    {
        int total = 0;
        // The following boolean has to be registered, in case there could be a better
        // sum for the hand depending on if we interpret the ace as '1' or '11'.
        bool acePresent = false;

        for (int i = 0; i < hand.Count; ++i)
        {
            total += GetCardValue(hand[i].Second, method);
            if (hand[i].Second == Values.ACE)
                acePresent = true;
        }

        // If the interpretation of one ace as '11' instead of '1' still makes the value
        // less or equal to 21, we adjust the total value accordingly (when looking for
        // the best possible value).
        if (method == SumCountingMethod.BEST && acePresent && total <= 11)
            return (total + 10);

        if (method == SumCountingMethod.BEST && acePresent && hand.Count == 1)
            return (total + 10);

        return total;
    }

    int GetCardValue(Values value, SumCountingMethod method)
    {
        switch (value)
        {
            case Values.ACE:
                switch (method)
                {
                    case SumCountingMethod.MIN:
                    // In case we are trying to calculate the best possible value of the hand, we save
                    // the presence of an ace in the hand (see int GetCardSum(...)).
                    case SumCountingMethod.BEST:
                        return 1;
                    case SumCountingMethod.MAX:
                        return 11;
                    default:
                        return -666;
                }
            case Values.TWO:
                return 2;
            case Values.THREE:
                return 3;
            case Values.FOUR:
                return 4;
            case Values.FIVE:
                return 5;
            case Values.SIX:
                return 6;
            case Values.SEVEN:
                return 7;
            case Values.EIGHT:
                return 8;
            case Values.NINE:
                return 9;
            case Values.TEN:
                return 10;
            case Values.JACK:
                return 10;
            case Values.QUEEN:
                return 10;
            case Values.KING:
                return 10;
        }

        return 0;
    }

    bool IsBust(ref List<Pair<Suits, Values>> hand)
    {
        return (GetCardSum(ref hand, SumCountingMethod.MIN) >= 22);
    }

    bool Is21(ref List<Pair<Suits, Values>> hand)
    {
        return (GetCardSum(ref hand, SumCountingMethod.BEST) == 21);
    }

    bool IsHostHitting()
    {
        return (GetCardSum(ref hostHand, SumCountingMethod.BEST) < 17);
    }

    void StartTurn()
    {
        doublingDown = undoingTheHit = bustPassing = lastPlayerDied = samePlayerDied = false;
        preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(false);
        bustPassingIndex = 1;

        ResetBustPassingPrice();

        if (!moneyManager.IsBroke(ref alivePlayers[currentPlayerID].money))
        {
            moneyManager.Bet(ref alivePlayers[currentPlayerID].money, 1000);

            if (!HasBustHandAlready(currentPlayerID))
            {
                DealHand();
                if (!Is21(ref playerHand)) infoBoard.SetInfo("Want to prepare, " + alivePlayers[currentPlayerID].name + "?");
                preDealPhase = true;
            }
            else
            {
                DealBustHand();
                preDealPhase = false;
            }
            isUILocked = true;
            if (!Is21(ref playerHand) || IsBust(ref playerHand))
            {
                ShowRuleBendingUI();
                StartCountDown();
            }
            else
            {
                HideRuleBendingUI();
            }
        }
        else
        {
            isUILocked = true;
            StartCoroutine(DelayedKillCurrentPlayer(1));
            return;
        }

        UpdatePlayerMoney();
        playerInfoBoxes.SetMoney(currentPlayerID, (alivePlayers[currentPlayerID].money).ToString());
        PrepareRuleBenderMenu();
    }

    IEnumerator DelayedStartTurn(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        StartTurn();
    }

    void ResetBustPassingPrice()
    {
        for (int index = 0; index < alivePlayers.Count; ++index)
            alivePlayers[index].passTheBustCost = 1000;
    }

    void ShowHandResults()
    {
        // The dealer has his final hand. It is time to look for the winner.
        switch (GetHandResult())
        {
            case HandEndState.PLAYER_WON:
                infoBoard.SetInfo("Won the hand!");
                moneyManager.Win(ref alivePlayers[currentPlayerID].money, false);
                infoBoard.SetMoneyAndPulsate(alivePlayers[currentPlayerID].money);
                Options.PlayOneShot(Options.ChooseRandom(playerWonVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);

                break;
            case HandEndState.PLAYER_BLACKJACK:
                infoBoard.SetInfo("Hand won with a blackjack!");
                moneyManager.Win(ref alivePlayers[currentPlayerID].money, true);
                infoBoard.SetMoneyAndPulsate(alivePlayers[currentPlayerID].money);
                Options.PlayOneShot(Options.ChooseRandom(playerBlackjackVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);

                break;
            case HandEndState.HOST_WON:
                infoBoard.SetInfo("Lost the hand!");
                moneyManager.Lose(ref alivePlayers[currentPlayerID].money, false);
                //infoBoard.SetMoneyAndPulsate(alivePlayers[currentPlayerID].money);
                Options.PlayOneShot(Options.ChooseRandom(hostWonVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);

                break;
            case HandEndState.HOST_BLACKJACK:
                infoBoard.SetInfo("Hand lost against a blackjack!");
                moneyManager.Lose(ref alivePlayers[currentPlayerID].money, true);
                //infoBoard.SetMoneyAndPulsate(alivePlayers[currentPlayerID].money);
                Options.PlayOneShot(Options.ChooseRandom(hostBlackjackVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);

                break;
            case HandEndState.PUSH:
                infoBoard.SetInfo("It's a push!");
                moneyManager.Push(ref alivePlayers[currentPlayerID].money);
                Options.PlayOneShot(Options.ChooseRandom(pushVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);
                
                break;
        }

        playerInfoBoxes.SetMoney(currentPlayerID, (alivePlayers[currentPlayerID].money).ToString(), false);

        StartCoroutine(DelayedEndTurn(2));
    }

    void EndTurn()
    {
        playerInfoBoxes.SetHighlight();

        if (!moneyManager.IsBroke(ref alivePlayers[currentPlayerID].money))
            NextTurnOrWin();
        else
            KillCurrentPlayer();
    }

    void ShowRuleBendingUI()
    {
        if (hitButton.transform.parent.gameObject.activeSelf)
        {
            StartCoroutine(ButtonFlipping(true));
        }
    }

    void ShowRuleBendingUIWithoutTimer()
    {
        if (hitButton.transform.parent.gameObject.activeSelf)
            StartCoroutine(ButtonFlipping(true));
    }

    void HideRuleBendingUI()
    {
        if (!hitButton.transform.parent.gameObject.activeSelf)
        {
            StartCoroutine(ButtonFlipping(false));
            if (preDealPhase)
            {
                preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
                Options.PlayOneShot(Options.ChooseRandom(hitStandSequenceStartVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);
            }
            else
                postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
        }
    }

    IEnumerator DelayedReshuffle()
    {
        yield return new WaitForSeconds(1.5f);
        cardsUI.RearrangeDeckAfterShuffle(ref playingDeck);
        discardPile.GetComponent<DiscardPile>().RefreshDiscardPile(ref discardDeck);
    }

    IEnumerator ButtonFlipping(bool flipToRuleBending, bool flipToBustPassing = false)
    {
        isUILocked = true;

        float timeElapsed = 0.0f;

        bool buttonsSwitched = false;

        while (timeElapsed < BUTTON_FLIPPING_DURATION)
        {
            timeElapsed += Time.deltaTime;

            if (timeElapsed < BUTTON_FLIPPING_DURATION / 2)
            {
                float rotationY = Mathf.Lerp(0, 180, timeElapsed / BUTTON_FLIPPING_DURATION);
                if (flipToRuleBending)
                {
                    hitButton.transform.parent.localEulerAngles = new Vector3(hitButton.transform.rotation.x, rotationY, hitButton.transform.rotation.z);
                    standButton.transform.parent.localEulerAngles = new Vector3(standButton.transform.rotation.x, rotationY, standButton.transform.rotation.z);
                }
                else if (takeItButton.transform.parent.gameObject.activeSelf && !flipToBustPassing)
                {
                    takeItButton.transform.parent.localEulerAngles = new Vector3(takeItButton.transform.rotation.x, rotationY, takeItButton.transform.rotation.z);
                    passItButton.transform.parent.localEulerAngles = new Vector3(passItButton.transform.rotation.x, rotationY, passItButton.transform.rotation.z);
                }
                else
                {
                    dealWithItButton.transform.parent.localEulerAngles = new Vector3(dealWithItButton.transform.rotation.x, rotationY, dealWithItButton.transform.rotation.z);
                    bendTheRulesButton.transform.parent.localEulerAngles = new Vector3(bendTheRulesButton.transform.rotation.x, rotationY, bendTheRulesButton.transform.rotation.z);
                }
                

            }
            else
            {
                if (!buttonsSwitched)
                {
                    buttonsSwitched = true;

                    if (flipToBustPassing)
                    {
                        dealWithItButton.transform.parent.gameObject.SetActive(!dealWithItButton.transform.parent.gameObject.activeSelf);
                        bendTheRulesButton.transform.parent.gameObject.SetActive(!bendTheRulesButton.transform.parent.gameObject.activeSelf);
                        takeItButton.transform.parent.gameObject.SetActive(!takeItButton.transform.parent.gameObject.activeSelf);
                        passItButton.transform.parent.gameObject.SetActive(!passItButton.transform.parent.gameObject.activeSelf);
                    }
                    else if (takeItButton.transform.parent.gameObject.activeSelf && !flipToBustPassing)
                    {
                        hitButton.transform.parent.gameObject.SetActive(!hitButton.transform.parent.gameObject.activeSelf);
                        standButton.transform.parent.gameObject.SetActive(!standButton.transform.parent.gameObject.activeSelf);
                        takeItButton.transform.parent.gameObject.SetActive(!takeItButton.transform.parent.gameObject.activeSelf);
                        passItButton.transform.parent.gameObject.SetActive(!passItButton.transform.parent.gameObject.activeSelf);
                    }
                    else if (flipToRuleBending || (!flipToRuleBending && dealWithItButton.transform.parent.gameObject.activeSelf))
                    {
                        hitButton.transform.parent.gameObject.SetActive(!hitButton.transform.parent.gameObject.activeSelf);
                        standButton.transform.parent.gameObject.SetActive(!standButton.transform.parent.gameObject.activeSelf);
                        dealWithItButton.transform.parent.gameObject.SetActive(!dealWithItButton.transform.parent.gameObject.activeSelf);
                        bendTheRulesButton.transform.parent.gameObject.SetActive(!bendTheRulesButton.transform.parent.gameObject.activeSelf);
                    }

                }

                float rotationY = Mathf.Lerp(180, 0, timeElapsed / BUTTON_FLIPPING_DURATION);

                if (dealWithItButton.transform.parent.gameObject.activeSelf)
                {
                    dealWithItButton.transform.parent.localEulerAngles = new Vector3(dealWithItButton.transform.rotation.x, rotationY, dealWithItButton.transform.rotation.z);
                    bendTheRulesButton.transform.parent.localEulerAngles = new Vector3(bendTheRulesButton.transform.rotation.x, rotationY, bendTheRulesButton.transform.rotation.z);
                }
                else if (hitButton.transform.parent.gameObject.activeSelf)
                {
                    hitButton.transform.parent.localEulerAngles = new Vector3(hitButton.transform.rotation.x, rotationY, hitButton.transform.rotation.z);
                    standButton.transform.parent.localEulerAngles = new Vector3(standButton.transform.rotation.x, rotationY, standButton.transform.rotation.z);
                }
                else
                {
                    takeItButton.transform.parent.localEulerAngles = new Vector3(takeItButton.transform.rotation.x, rotationY, takeItButton.transform.rotation.z);
                    passItButton.transform.parent.localEulerAngles = new Vector3(passItButton.transform.rotation.x, rotationY, passItButton.transform.rotation.z);
                }
            }

            yield return null;
        }

        if (preDealPhase && hitButton.transform.parent.gameObject.activeSelf && !doublingDown && !lastPlayerDied && !Is21(ref playerHand))
        {
            StartCountDown(2.2f);
        }
        else if (IsBust(ref playerHand) && !undoingTheHit && dealWithItButton.transform.parent.gameObject.activeSelf)
        {
            StartCountDown();
        }

        if (preDealPhase && !flipToRuleBending && !flipToBustPassing)
            preDealPhase = false;
    }

    void NextTurnOrWin() {
        if (!Is21(ref playerHand))
            HideRuleBendingUI();
        if (lastPlayerDied) deathScreen.Hide();

        if (alivePlayers.Count <= numberOfSurvivors) {
            UtilityMethods.FadeCanvasGroupVisible(this, false, everythingButBackground, FADE_SPEED);
            host.FadeOut(FADE_SPEED);
            Win();
        }
        else
            // Money awarded, start the turn of next player.
            StartCoroutine(NextTurn());
    }

    IEnumerator DelayedNextTurnOrWin(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        NextTurnOrWin();
    }

    // Shows Retry Screen
    void Win() {
        retryScreen.SetWinner(alivePlayers[0].name);

        List<string> loosersNames = new List<string>();

        for (int i = 0; i < deadPlayers.Count; ++i) {
            loosersNames.Add(deadPlayers[i].name);
        }
        deathScreen.Hide();
        retryScreen.SetLoosers(loosersNames);
        retryScreen.Show(true);
    }

    HandEndState GetHandResult()
    {
        int playerTotal = GetCardSum(ref playerHand, SumCountingMethod.BEST);
        int hostTotal = GetCardSum(ref hostHand, SumCountingMethod.BEST);
        // Detect a natural blackjack in the game on both sides.
        bool playerBlackjack = (playerTotal == 21 && playerHand.Count == 2);
        bool hostBlackjack = (hostTotal == 21 && hostHand.Count == 2);

        if (IsBust(ref playerHand))
        {
            if (hostBlackjack)
                return HandEndState.HOST_BLACKJACK;
            else
                return HandEndState.HOST_WON;
        }

        if (playerBlackjack && !hostBlackjack)
            return HandEndState.PLAYER_BLACKJACK;
        else if (!playerBlackjack && hostBlackjack)
            return HandEndState.HOST_BLACKJACK;
        else if (playerTotal == hostTotal)
            return HandEndState.PUSH;
        else if (IsBust(ref hostHand) || playerTotal > hostTotal)
            return HandEndState.PLAYER_WON;
        else
            return HandEndState.HOST_WON;
    }

    void DiscardHand()
    {
        if (playerHand.Count > 0)
        {
            discardDeck.Add(playerHand[0]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.playerDeck, Decks.DISCARD);
        }
        if (hostHand.Count > 0)
        {
            discardDeck.Add(hostHand[0]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.hostDeck, Decks.DISCARD);
        }
        if (playerHand.Count > 1)
        {
            discardDeck.Add(playerHand[1]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.playerDeck, Decks.DISCARD);
        }
        if (hostHand.Count > 1)
        {
            discardDeck.Add(hostHand[1]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.hostDeck, Decks.DISCARD);
        }

        for (int i = 2; i < playerHand.Count; ++i)
        {
            discardDeck.Add(playerHand[i]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.playerDeck, Decks.DISCARD);
        }
        for (int i = 2; i < hostHand.Count; ++i)
        {
            discardDeck.Add(hostHand[i]);
            cardsUI.MoveCardFromToDeck(ref cardsUI.hostDeck, Decks.DISCARD);
        }
        playerHand.Clear();
        hostHand.Clear();

        discardPile.GetComponent<DiscardPile>().RefreshDiscardPile(ref discardDeck);
        cardsUI.UpdatePlayerHandTotalText(0);
        cardsUI.UpdateHostHandTotalText(0);
    }

    void SwitchTurn()
    {
        UpdateCurrentPlayerID();
        infoBoard.EraseMoney();

        string passThePhoneMessage = "";
        if (alivePlayers.Count > numberOfSurvivors)
            passThePhoneMessage = "Pass the phone to " + alivePlayers[currentPlayerID].name + "!";

        if (alivePlayers.Count > numberOfSurvivors && !(bustPassing && samePlayerDied))
        {
            playerInfoBoxes.ChangePlayer(alivePlayers[currentPlayerID].name, alivePlayers[currentPlayerID].money.ToString(), isTurnOrderReversed);
        }
    }

    private string GetPassThePhoneMessage(bool lastPlayerDied, int playerID = -1) {
        int tempCurrentPlayerID;

        if (playerID == -1)
            tempCurrentPlayerID = currentPlayerID;
        else
            tempCurrentPlayerID = playerID;

        if (!lastPlayerDied) {
            ++tempCurrentPlayerID;
        }
        if (tempCurrentPlayerID >= alivePlayers.Count) tempCurrentPlayerID = 0;

        if (alivePlayers.Count > numberOfSurvivors) return "Pass the phone to " + alivePlayers[tempCurrentPlayerID].name + "!";
        else return "";
    }

    private void UpdateCurrentPlayerID()
    {
        if (isTurnOrderReversed)
            currentPlayerID = (currentPlayerID - 1) % alivePlayers.Count;
        else
        {
            if (!lastPlayerDied)
                currentPlayerID = (currentPlayerID + 1) % alivePlayers.Count;
            else
                currentPlayerID = currentPlayerID % alivePlayers.Count;
        }
    }

    private void KillCurrentPlayer(bool timeUp = false)
    {
        if (lastPlayerDied) // The lone crusader, protecting from any repetitive calls of this function.
            return;

        if (!timeUp)
            Options.PlayOneShot(Options.ChooseRandom(deathBrokeVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);

        cardsUI.ToggleHostHandTotalUIIndication(false, 0, false);

        int tempID = currentPlayerID;
        samePlayerDied = false;

        DiscardHand();

        if (bustPassing)
        {
            if (currentPlayerID != (currentPlayerID + bustPassingIndex) % alivePlayers.Count)
            {
                moneyManager.BustPassed(ref alivePlayers[currentPlayerID].money, false);
                UpdatePlayerMoney();
                playerInfoBoxes.SetMoney(currentPlayerID, (alivePlayers[currentPlayerID].money).ToString());
            }
            else
                samePlayerDied = true;
            currentPlayerID = (currentPlayerID + bustPassingIndex) % alivePlayers.Count;
        }

        isUILocked = true;
        lastPlayerDied = true;
        deadPlayers.Add(alivePlayers[currentPlayerID]);
        alivePlayers.Remove(alivePlayers[currentPlayerID]);
        playerInfoBoxes.KillCurrentPlayer(isTurnOrderReversed, currentPlayerID);

        postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
        preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();

        StartCoroutine(HandLungeAnimation(timeUp));

        if (bustPassing)
        {
            if (currentPlayerID < tempID)
                currentPlayerID = tempID;
            else // We need to check, if we continue by the next ID, or we start from 0 again.
            {
                if (samePlayerDied)
                    if (tempID < alivePlayers.Count)
                        currentPlayerID = tempID;
                    else
                        currentPlayerID = 0;
                else
                {
                    if (tempID < alivePlayers.Count)
                        currentPlayerID = tempID + 1;
                    else
                        currentPlayerID = 0;
                }
            }

        }
        else
            currentPlayerID = tempID % alivePlayers.Count;
           
    }

    IEnumerator HandLungeAnimation(bool timeUp) {
        UtilityMethods.FadeCanvasGroupVisible(this, false, currentPlayerUi, FADE_SPEED_SLOW);
        infoBoard.EraseMoney();

        if (timeUp) infoBoard.SetInfoAndFlicker("TIME'S UP!");
        hand.ShowFinger();

        //Options.PlayOneShot(timeOutEvent[UnityEngine.Random.Range(0, timeOutEvent.Length)], Options.AudioType.VOICE_ACTING);
        yield return new WaitForSeconds(3.5f);

        infoBoard.SetInfoAndFlicker(GetPassThePhoneMessage(true));
        hand.LungeAndShowDeathScreen(ref deathScreen, GetPassThePhoneMessage(true), this);
    }
    // hand.LungeAndShowDeathScreen Callback->
    public void OnScreenCracked(string message) {
        // Host's hand to hovering animation + empty player turn text before deathScreen fades away
        if (alivePlayers.Count > numberOfSurvivors)
            currentPlayerUi.GetComponent<CanvasGroup>().alpha = 1f;// Switches it visible
        mainButtons.GetComponent<CanvasGroup>().alpha = 0f;
        hand.Hover();
        infoBoard.EraseInfo(); 
        StartCoroutine(DelayedNextTurnOrWin(3f));
    }

    private void UpdatePlayerMoney(int ID = -1, bool pulsate=false)
    {
        if (ID == -1)
            ID = currentPlayerID;

        if (pulsate)
            infoBoard.SetMoneyAndPulsate(alivePlayers[ID].money);
        else
            infoBoard.SetMoney(alivePlayers[ID].money);
    }

    private void DoubleDown()
    {
        isUILocked = true;
        HideRuleBendingUI();
        Hit(currentPlayerID);
        OnStandLocked();
    }

    private void SwitchHands()
    {
        List<Pair<Suits, Values>> temp = new List<Pair<Suits, Values>>();

        while (playerHand.Count != 0)
        {
            temp.Add(playerHand[0]);
            playerHand.RemoveAt(0);
        }

        while (hostHand.Count != 0)
        {
            playerHand.Add(hostHand[0]);
            hostHand.RemoveAt(0);
        }

        while (temp.Count != 0)
        {
            hostHand.Add(temp[0]);
            temp.RemoveAt(0);
        }

        StartCoroutine(cardsUI.SwitchHands());
        switchingHands = false;
    }

    private void UndoTheHit()
    {
        cardsUI.MoveCardFromToDeck(ref cardsUI.playerDeck, Decks.DISCARD, (playerHand.Count - 1));
        discardDeck.Add(playerHand[playerHand.Count - 1]);
        playerHand.RemoveAt(playerHand.Count - 1);

        cardsUI.readjustPlayerHand();
        StartCoroutine(DelayedJustHit(1f));
        cardsUI.UpdatePlayerHandTotalText(GetCardSum(ref playerHand, SumCountingMethod.BEST));
    }

    private void PassTheBust()
    {
        bustPassing = true;
        postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
        ShowPassingTheBustUI();

        while (alivePlayers[(currentPlayerID + bustPassingIndex) % alivePlayers.Count].bustPlayerHand.Count != 0)
        {
            ++bustPassingIndex;
        }

        StartCoroutine(PassingTransition());


    }

    private void UpdateScreenCenterMessage()
    {
        UpdatePlayerMoney((currentPlayerID + bustPassingIndex) % alivePlayers.Count);
        centerIndicationText.text = "You have " + alivePlayers[currentPlayerID].name + "'s bust hand now!\nPassing the bust costs " +
                                    alivePlayers[(currentPlayerID + bustPassingIndex) % alivePlayers.Count].passTheBustCost + "$!";
    }

    private void AssignBustHand(int playerID, ref List<Pair<Suits, Values>> pHand, ref List<Pair<Suits, Values>> hHand)
    {
        for (int index = 0; index < pHand.Count; ++index)
            alivePlayers[playerID].bustPlayerHand.Add(pHand[index]);

        for (int index = 0; index < hHand.Count; ++index)
            alivePlayers[playerID].bustHostHand.Add(hHand[index]);

        pHand.Clear();
        hHand.Clear();

        cardsUI.AssignBustHand(ref alivePlayers[playerID].bustPlayerDeck, ref alivePlayers[playerID].bustHostDeck);
    }

    private bool HasBustHandAlready(int playerID)
    {
        return (alivePlayers[playerID].bustPlayerHand.Count != 0);
    }

    #endregion

    #region Timer Methods And Callbacks

    public IEnumerator DelayedStartCountDown(int delayInSeconds, float tickModifier = TICK_DURATION)
    {
        yield return new WaitForSeconds(delayInSeconds);
    }

    public void StartCountDown(float tickModifier = TICK_DURATION) {
        // Start count down music & do stuff here
        
        timer.StartCountDown(STARTING_TIME, tickModifier, this, tickModifier != TICK_DURATION);
        isUILocked = false;
    }

    void StopCountdown() {
        // Stop count down music & do whatever here
        RuleBenderMenu ruleBenderMenu = preDealRuleBenderMenu.GetComponent<RuleBenderMenu>();
        ruleBenderMenu.ShutUp();

        ruleBenderMenu = postDealRuleBenderMenu.GetComponent<RuleBenderMenu>();
        ruleBenderMenu.ShutUp();

        timer.StopCountdown();
    }

    public void OnTick(int number) {
        // Don't do shit
    }

    public void OnTimeUp() {
        RuleBenderMenu ruleBenderMenu = preDealRuleBenderMenu.GetComponent<RuleBenderMenu>();
        ruleBenderMenu.setInputLocked(true);
        ruleBenderMenu.ShutUp();

        ruleBenderMenu = postDealRuleBenderMenu.GetComponent<RuleBenderMenu>();
        ruleBenderMenu.setInputLocked(true);
        ruleBenderMenu.ShutUp();

        isUILocked = true;
        if (bustPassing)
        {
            HidePassingTheBustUI();
        }

        KillCurrentPlayer(true);
        centerIndicationText.text = "";
    }

    #endregion

    #region public methods

    public void StartScene()
    {
        cardsUI.CreateDeck();
        StartCoroutine(StartGame());
    }

    public void Retry()
    {
        PlayerPrefs.SetInt("BJRetry", 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Exit()
    {
        SceneManager.LoadScene(backButtonSceneName);
    }

    public void LetsGo()
    {
        if (!lettingGo)
        {
            isUILocked = lettingGo = true;
            turnRandomizationScreen.Hide();
            UtilityMethods.FadeCanvasGroupVisible(this, true, currentPlayerUi, FADE_SPEED);
            host.FadeIn(FADE_SPEED);
            StartScene();
            ShowRuleBendingUIWithoutTimer();
            StartCoroutine(DelayedStartTurn(1));
        }
    }

    public List<Pair<Suits, Values>> GetPlayerHand()
    {
        return playerHand;
    }

    public List<Pair<Suits, Values>> GetHostHand()
    {
        return hostHand;
    }

    public List<Pair<Suits, Values>> GetPlayingDeck()
    {
        return playingDeck;
    }

    // BackButtonConfirmation.BackButtonCallbackInterface
    public void OnBackButtonConfirmed() {
        Exit();
    }

    #endregion

    #region coroutines

    public void ShowPassingTheBustUI()
    {
        infoBoard.SetInfo("TIME TO PASS THE BUST!");
        isUILocked = true;
        TogglePassingTheBustUI(true);
        cardsUI.MoveCurrentHandAway(false);
        Options.PlayOneShot(passTheBustTransitionSound, Options.AudioType.SFX);
        Options.StartFmodInstance(passTheBustThemeInstance, Options.AudioType.MUSIC);
        blackjackThemeInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        StartCoroutine(ButtonFlipping(false, true));
    }

    public void HidePassingTheBustUI()
    {
        isUILocked = true;
        TogglePassingTheBustUI(false);
        StartCoroutine(ButtonFlipping(false, false));
        passTheBustThemeInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        Options.StartFmodInstance(blackjackThemeInstance, Options.AudioType.MUSIC);
    }

    public IEnumerator DelayedHidePassingTheBustUI(int delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        HidePassingTheBustUI();
    }

    public void TogglePassingTheBustUI(bool show)
    {
        StartCoroutine(ToggleBustUISequence(show));

    }

    public void handsUITotalUpdateRequest()
    {
        cardsUI.UpdatePlayerHandTotalText(GetCardSum(ref playerHand, SumCountingMethod.BEST));
        cardsUI.UpdateHostHandTotalText(GetCardSum(ref hostHand, SumCountingMethod.BEST));
    }

    public IEnumerator ToggleBustUISequence(bool showing)
    {
        float timeElapsed = 0.0f;

        float originalScale = 1f;
        float zoomedInScale = 1.15f;
        float scaleModifier;

        float originalPosOffset = 0.0f;
        float modifiedPosOffset = 200.0f;
        float discardPileStartingXPosition = discardPile.transform.position.x;
        float discardPileEndingXPosition = discardPile.transform.position.x;
        float sideUIElementsModifier;

        while (timeElapsed < passingTheBustSequenceDuration)
        {
            timeElapsed += Time.deltaTime;

            sideUIElementsModifier = Mathf.Lerp(originalPosOffset, modifiedPosOffset, timeElapsed / passingTheBustSequenceDuration);
            if (showing)
            {
                scaleModifier = Mathf.Lerp(originalScale, zoomedInScale, timeElapsed / passingTheBustSequenceDuration);

            }
            else
            {
                scaleModifier = Mathf.Lerp(zoomedInScale, originalScale, timeElapsed / passingTheBustSequenceDuration);
            }

            // zooming in
            background.transform.localScale = new Vector3(scaleModifier, 1);
            hostImage.transform.localScale = new Vector3(scaleModifier, scaleModifier);
            table.transform.localScale = new Vector3(scaleModifier, scaleModifier);

            // moving elements
            if (showing)
            {
//                playerInfoBoxes.transform.position = new Vector3(playerInfoBoxesStartingXPosition - sideUIElementsModifier, playerInfoBoxes.transform.position.y);
                discardPile.transform.position = new Vector3(discardPileStartingXPosition + sideUIElementsModifier, discardPile.transform.position.y);
            }
            else
            {
//                playerInfoBoxes.transform.position = new Vector3(playerInfoBoxesStartingXPosition + sideUIElementsModifier, playerInfoBoxes.transform.position.y);
                discardPile.transform.position = new Vector3(discardPileStartingXPosition - sideUIElementsModifier, discardPile.transform.position.y);
            }

            yield return null;
        }
    }

    public IEnumerator StartGame()
    {
        yield return new WaitForSeconds(0);
    }

    public IEnumerator NextTurn()
    {
        UtilityMethods.FadeCanvasGroupVisible(this, false, mainButtons, FADE_SPEED);
        infoBoard.SetInfoAndFlicker(GetPassThePhoneMessage(lastPlayerDied));
        isUILocked = true;
        yield return new WaitForSeconds(1);

        DiscardHand();

        if (ReshuffleNeeded())
            Reshuffle();

        if (!lastPlayerDied || bustPassing)
        {
            SwitchTurn();
        }
        yield return new WaitForSeconds(4f);

        UtilityMethods.FadeCanvasGroupVisible(this, true, mainButtons, FADE_SPEED);
        StartTurn();
    }

    public IEnumerator HostTurn()
    {
        // Here, the player has finished all his actions, and is waiting for dealer to finish his hand.
        isUILocked = true;

        cardsUI.hostDeck[1].Second.GetComponent<CardMovement>().ShowCard(true);
        StartCoroutine(cardsUI.DelayedUpdateHostHandTotalText(GetCardSum(ref hostHand, SumCountingMethod.BEST), 1));

        yield return new WaitForSeconds(1);

        while (IsHostHitting())
        {
            Hit(hostID);
            yield return new WaitForSeconds(1);
        }

        if (switchingHands)
        {
            SwitchHands();
            DelayedShowHandResults(4);
        }
        else if (IsBust(ref playerHand) && !undoingTheHit)
        {
            ShowRuleBendingUI();
        }
        else
            ShowHandResults();

        yield return null;
    }

    public IEnumerator DelayedShowHandResults(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        ShowHandResults();
    }

    public IEnumerator DelayedEndTurn(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        EndTurn();
    }

    public IEnumerator DelayedHit(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        Hit(currentPlayerID);
    }

    public IEnumerator DelayedJustHit(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        JustHit(currentPlayerID);
    }
   
    public IEnumerator DelayedKillCurrentPlayer(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        KillCurrentPlayer();
    }

    public IEnumerator DelayedHostTurn(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        StartCoroutine(HostTurn());
        centerIndicationText.text = "";
    }

    public void OnHit()
    {
        if (!isUILocked)
        {
            Options.PlayOneShot(Options.ChooseRandom(cardFlippingSound), Options.AudioType.SFX);
            Hit(currentPlayerID);
        }
    }

    public void OnStandLocked()
    {
        infoBoard.EraseInfo();
        isUILocked = true;
        preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
        postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
        preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(true);
        postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(true);
        StopCountdown();

        if (IsBust(ref playerHand) || (Is21(ref playerHand) && playerHand.Count == 2))
        {
            if (IsBust(ref playerHand))
                StartCoroutine(UIIndicate("BUST"));
            else
                StartCoroutine(UIIndicate("BLACKJACK"));

            StartCoroutine(DelayedHostTurn(2));
        }
        else
        {
            if (Is21(ref playerHand))
                StartCoroutine(DelayedHostTurn(2));
            else
                StartCoroutine(HostTurn());

        }
    }

    public void OnStand()
    {
        if (!isUILocked) {
            isUILocked = true;
            infoBoard.EraseInfo();
            preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
            postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
            preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(true);
            postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(true);
            StopCountdown();

            if (IsBust(ref playerHand) || (Is21(ref playerHand) && playerHand.Count == 2))
            {
                if (IsBust(ref playerHand))
                    StartCoroutine(UIIndicate("BUST"));
                else
                    StartCoroutine(UIIndicate("BLACKJACK"));

                StartCoroutine(DelayedHostTurn(2));
            }
            else
            {
                if (Is21(ref playerHand))
                    StartCoroutine(DelayedHostTurn(2));
                else
                    StartCoroutine(HostTurn());

            }
        }
    }

    public void OnDealWithIt()
    {
        if (!isUILocked) {
            preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
            postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Close();
            preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(true);
            postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(true);
            isUILocked = true;
            StopCountdown();
            if (!preDealPhase)
                ShowHandResults();
            else {
                infoBoard.SetInfo("Whats it gonna be, " + alivePlayers[currentPlayerID].name + "?");
                HideRuleBendingUI();
            }
        }
    }

    public void OnTakeIt()
    {
        if (!isUILocked)
        {
            StopCountdown();
            centerIndicationText.text = "";

            infoBoard.SetInfo("You took the bust!");

            Options.PlayOneShot(Options.ChooseRandom(takeItVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);

            isUILocked = true;
            StopCountdown();
            StartCoroutine(DelayedHidePassingTheBustUI(2));

            cardsUI.MoveCurrentHandAway();

            if ((currentPlayerID + bustPassingIndex) % alivePlayers.Count == currentPlayerID)
            {
                StartCoroutine(DelayedShowHandResults(2));
            }
            else
            {
                moneyManager.BustPassed(ref alivePlayers[currentPlayerID].money);
                playerInfoBoxes.SetMoney(currentPlayerID, (alivePlayers[currentPlayerID].money).ToString());
                AssignBustHand((currentPlayerID + bustPassingIndex) % alivePlayers.Count, ref playerHand, ref hostHand);
                StartCoroutine(DelayedEndTurn(2));
            }

            cardsUI.ToggleHostHandTotalUIIndication(false, 0, false);

            cardsUI.UpdatePlayerHandTotalText(0);
            cardsUI.UpdateHostHandTotalText(0);
        }
    }

    public void OnPassIt()
    {
        if (!isUILocked)
        {
            BJ_Player p = alivePlayers[(currentPlayerID + bustPassingIndex) % alivePlayers.Count];

            if (moneyManager.IsPossibleToBuy(ref p.money, ref p.passTheBustCost))
            {
                centerIndicationText.text = "";
                StopCountdown();
                isUILocked = true;
                // ID modifier workaround
                int tempID = currentPlayerID;
                currentPlayerID = (currentPlayerID + bustPassingIndex) % alivePlayers.Count;
                OnRuleBenderBought("Pass the Bust On", p.passTheBustCost, p.passTheBustCost + p.passTheBustCostIncrease);
                currentPlayerID = tempID;

                do
                {
                    ++bustPassingIndex;
                } while (alivePlayers[(currentPlayerID + bustPassingIndex) % alivePlayers.Count].bustPlayerHand.Count != 0);

                StartCoroutine(PassingTransition());
            }
            else
            {
                StartCoroutine(postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().ShowNotEnoughMoney("NOT ENOUGH MONEY"));
            }
        }
    }

    public IEnumerator PassingTransition()
    {
        isUILocked = true;
        UtilityMethods.FadeCanvasGroupVisible(this, false, mainButtons, FADE_SPEED);
        infoBoard.SetInfoAndFlicker(GetPassThePhoneMessage(lastPlayerDied, (currentPlayerID + bustPassingIndex - 1) % alivePlayers.Count));

        playerInfoBoxes.SetHighlight((currentPlayerID + bustPassingIndex) % alivePlayers.Count);

        Options.PlayOneShot(Options.ChooseRandom(passTheBustVoiceLine, ref voiceLineRandomizingCounter), Options.AudioType.VOICE_ACTING);

        yield return new WaitForSeconds(5f);

        UtilityMethods.FadeCanvasGroupVisible(this, true, mainButtons, FADE_SPEED, this);
        infoBoard.SetInfo("It's in your hands, " + alivePlayers[(currentPlayerID + bustPassingIndex) % alivePlayers.Count].name + "!");
        UpdateScreenCenterMessage();

        cardsUI.ToggleHostHandTotalUIIndication(true, GetCardSum(ref hostHand, SumCountingMethod.BEST), hostHand.Count == 2);
    }
    // Callback->
    // Call only on fade in (don't pass this callback interface to the FadeCanvasGroupVisible when fading out)
    public void OnFadeDone() {
        StartCountDown();
    }

    public IEnumerator UIIndicate(string s)
    {
        GameObject g;

        yield return new WaitForSeconds(UIIndicationDelayDuration);

        if (s.Equals("BUST"))
        {
            g = bustScreen;
            Options.PlayOneShot(bustSound, Options.AudioType.SFX);
        }
        else
        {
            g = blackjackScreen;
            Options.PlayOneShot(blackjackSound, Options.AudioType.SFX);
        }

        float timeElapsed = 0.0f;

        while (timeElapsed < UIIndicationZoomInDuration)
        {
            timeElapsed += Time.deltaTime;

            if (timeElapsed < UIIndicationZoomInDuration)
            {
                float scaling = Mathf.Lerp(0, 1f, timeElapsed / UIIndicationZoomInDuration);
                g.transform.localScale = new Vector3(scaling, scaling, g.transform.localScale.z);
            }

            yield return null;
        }

        timeElapsed = 0;

        Image i = g.GetComponent<Image>();

        yield return new WaitForSeconds(UIIndicationShownDuration);

        while (timeElapsed < UIIndicationFadeOutDuration)
        {
            timeElapsed += Time.deltaTime;

            if (timeElapsed < UIIndicationFadeOutDuration)
            {
                float alpha = Mathf.Lerp(1f, 0, timeElapsed / UIIndicationFadeOutDuration);
                i.color = new Color(i.color.r, i.color.g, i.color.b, alpha);
            }

            yield return null;
        }

        g.transform.localScale = new Vector3(0, 0, g.transform.localScale.z);
        i.color = new Color(i.color.r, i.color.g, i.color.b, 1f);

        if (s.Equals("BUST"))
            infoBoard.SetInfo("It's in your hands, " + alivePlayers[currentPlayerID].name + "!");

        preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(false);
        postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().setInputLocked(false);
    }

    #endregion coroutines

    #region rule bender menu

    public void ToggleRuleBenderMenu()
    {
        if (preDealPhase)
            preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().ToggleMenu();
        else
            postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().ToggleMenu();
    }

    public void PrepareRuleBenderMenu()
    {
        BJ_Player player = alivePlayers[currentPlayerID];
        preDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Prepare(player.money, new[] { player.doubleDownCost, player.switchHandsCost });
        postDealRuleBenderMenu.GetComponent<RuleBenderMenu>().Prepare(player.money, new[] { player.undoTheHitCost, player.passTheBustCost });
    }

    public void OnToggle(bool x) {}

    public void OnRuleBenderBought(string ruleBender, int spentMoney, int updatedCost)
    {
        isUILocked = true;
        StopCountdown();

        BJ_Player currentPlayer = alivePlayers[currentPlayerID];
        turnAction = TurnAction.NONE;

        if (ruleBender.Equals("Double Down"))
        {
            doublingDown = true;
            moneyManager.DoubleDown(ref currentPlayer.money);
            turnAction = TurnAction.DOUBLE_DOWN;
        }
        else if (ruleBender.Equals("Switch Hands"))
        {
            switchingHands = true;
            moneyManager.BuyRuleBender(ref currentPlayer.money, ref currentPlayer.switchHandsCost, priceIncreaseModifier);
            turnAction = TurnAction.SWITCH_HANDS;
        }
        else if (ruleBender.Equals("Undo the Hit"))
        {
            undoingTheHit = true;
            moneyManager.BuyRuleBender(ref currentPlayer.money, ref currentPlayer.undoTheHitCost, priceIncreaseModifier);
            turnAction = TurnAction.UNDO_THE_HIT;
        }
        else if (ruleBender.Equals("Pass the Bust"))
        {
            moneyManager.BuyRuleBender(ref currentPlayer.money, ref currentPlayer.passTheBustCost, currentPlayer.passTheBustCostIncrease);
            turnAction = TurnAction.PASS_THE_BUST;
            PassTheBust();
        }
        else if (ruleBender.Equals("Pass the Bust On"))
        {
            moneyManager.BuyRuleBender(ref currentPlayer.money, ref currentPlayer.passTheBustCost, currentPlayer.passTheBustCostIncrease);
            turnAction = TurnAction.PASS_THE_BUST;
        }
        else Debug.Log("RR_Game.OnRuleBenderBought: Invalid rule bender");

//        infoBoard.EraseInfo();

        UpdatePlayerMoney(currentPlayerID, true);
        playerInfoBoxes.SetMoney(currentPlayerID, (alivePlayers[currentPlayerID].money).ToString(), true);
    }


    public void OnClosedAfterPurchase()
    {
        // Wasn't actually closed because this callback's name is obsolete. Do it yourself bitch!
        if (!bustPassing)
            HideRuleBendingUI();
    }

    public void OnInfoShown()
    {
        switch (turnAction)
        {
            case TurnAction.DOUBLE_DOWN:
                DoubleDown();
                break;

            case TurnAction.SWITCH_HANDS:
                // HideRuleBendingUI();
                break;

            case TurnAction.UNDO_THE_HIT:
                UndoTheHit();
                StartCoroutine(DelayedShowHandResults(2));
                break;
            case TurnAction.PASS_THE_BUST:
                break;
            default:
                break;
        }

        PrepareRuleBenderMenu();
    }

    #endregion

    #region debug
    void Debug_PrintHands()
    {
        string test = "playerHand:\n";
        for (int i = 0; i < playerHand.Count; ++i)
        {
            test = test + " " + playerHand[i].First.ToString() + "," + playerHand[i].Second.ToString() + "\n";
        }

        Debug.Log(GetCardSum(ref playerHand, SumCountingMethod.BEST) + "\n" + test);

        test = "hostHand:\n";
        for (int i = 0; i < hostHand.Count; ++i)
        {
            test = test + " " + hostHand[i].First.ToString() + "," + hostHand[i].Second.ToString() + "\n";
        }

        Debug.Log(GetCardSum(ref hostHand, SumCountingMethod.BEST) + "\n" + test);
        Debug.Log("---------------------------");
    }

    void Debug_PrintReshuffleHands(ref List<Pair<Suits, Values>> h1, ref List<Pair<Suits, Values>> h2)
    {
        string test = "RESHUFFLE??\nPLAYERMIN:\n";
        for (int i = 0; i < h1.Count; ++i)
        {
            test = test + " " + h1[i].First.ToString() + "," + h1[i].Second.ToString() + "\n";
        }

        test += "\nHOSTBEST:\n";
        for (int i = 0; i < h2.Count; ++i)
        {
            test = test + " " + h2[i].First.ToString() + "," + h2[i].Second.ToString() + "\n";
        }

        Debug.Log(test + "\nRemaining deck cards: " + playingDeck.Count);
    }
    #endregion
}
