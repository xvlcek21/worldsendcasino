﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventInstancePlayer : MonoBehaviour {

    [FMODUnity.EventRef]
    public string eventName;

    public bool playOnAwake = false;
    public Options.AudioType type;
    public bool singleton = false;

    static EventInstancePlayer instance;
    bool prematureDestruction = false;
    FMOD.Studio.EventInstance eventInstance;

    void Awake() {
        if (singleton) {
            if (!instance) instance = this;
            else {
                prematureDestruction = true;
                Destroy(gameObject);
                return;
            }
        }
        eventInstance = FMODUnity.RuntimeManager.CreateInstance(eventName);
        if (playOnAwake) StartInstance(type);
    }

    public void OnDestroy() {
        if (prematureDestruction) return;

        eventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        eventInstance.release();
    }

    public void StartInstance(Options.AudioType audioType) {
        Options.StartFmodInstance(eventInstance, audioType);
    }

    public bool IsPlaying() {
        FMOD.Studio.PLAYBACK_STATE playbackState;
        eventInstance.getPlaybackState(out playbackState);

        if (playbackState == FMOD.Studio.PLAYBACK_STATE.PLAYING) return true;
        return false;

    }

    public void Stop() {
        eventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }
}