﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RR_HelpCylinder : MonoBehaviour {
    
    public GameObject[] chambers;
    public RectTransform highlight;

    Vector3[] chamberPositions;

    int currentChamberIndex = 0;
    float cylinderRotationZ = 0f;
    Vector3 scale = Vector3.one;
    enum State { WAIT, SCALE_UP, SCALE_DOWN }
    State state = State.SCALE_UP;

    void Awake() {
        chamberPositions = new Vector3[chambers.Length];

        for (int i = 0; i < chambers.Length; ++i) {
            chamberPositions[i] = chambers[i].GetComponent<RectTransform>().anchoredPosition;
        }
    }

    void Update () {
        float deltaScale = 0.15f * Time.deltaTime;

        if (state == State.SCALE_UP) {
            scale.x += deltaScale;
            scale.y += deltaScale;
            if (scale.x >= 1.05f) state = State.SCALE_DOWN;
        }
        else if (state == State.SCALE_DOWN) {
            scale.x -= deltaScale;
            scale.y -= deltaScale;
            if (scale.x <= 1f) state = State.SCALE_UP;
        }
        else if (state == State.WAIT) return;

        chambers[currentChamberIndex].GetComponent<RectTransform>().localScale = scale;
        highlight.localScale = scale;
    }

    void StopPulsating () {
        scale.x = 1f;
        scale.y = 1f;
        state = State.WAIT;
        chambers[currentChamberIndex].GetComponent<RectTransform>().localScale = scale;
    }

    public void Reset() {
        currentChamberIndex = 0;
        cylinderRotationZ = 0f;
        GetComponent<RectTransform>().rotation = Quaternion.identity;

        for (int i = 0; i < chambers.Length; ++i) {
            chambers[i].SetActive(true);
            scale.x = 1f;
            scale.y = 1f;
            chambers[i].GetComponent<RectTransform>().localScale = scale;
            chambers[i].GetComponent<RectTransform>().rotation = Quaternion.identity;
        }

        highlight.anchoredPosition = chamberPositions[currentChamberIndex];
        highlight.gameObject.SetActive(true);

        state = State.SCALE_UP;
    }
    
    public void SpinCylinder() {
        Reset();// TODO Spin animation + fade in question marks?
    }

    public void Shoot(bool bulletInChamber, bool rotateAfter = true) {
        StopPulsating();
        highlight.gameObject.SetActive(false);
        chambers[currentChamberIndex].gameObject.SetActive(false);
        if (rotateAfter) StartCoroutine(WaitAndRotateCylinder());
    }

    public void SkipChamber() {
        StopPulsating();
        highlight.gameObject.SetActive(false);
        StartCoroutine(RotateCylinder());
    }

    IEnumerator WaitAndRotateCylinder() {
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(RotateCylinder());
    }

    IEnumerator RotateCylinder() {
        ++currentChamberIndex;
        if (currentChamberIndex >= chambers.Length) currentChamberIndex = 0;
        
        RectTransform cylinderTransform = GetComponent<RectTransform>();

        float speed = 5f;
        float stopAngle = cylinderRotationZ - 60f;

        Vector3 euler = Vector3.zero;

        while (cylinderRotationZ > stopAngle) {
            cylinderRotationZ -= speed;
            euler.z = cylinderRotationZ;
            cylinderTransform.rotation = Quaternion.Euler(euler);

            for (int i = 0; i < chambers.Length; ++i) {
                chambers[i].GetComponent<RectTransform>().localRotation = Quaternion.Euler(-euler);
            }
            yield return new WaitForSeconds(0.01f);
        }

        highlight.anchoredPosition = chamberPositions[currentChamberIndex];
        highlight.gameObject.SetActive(true);
        state = State.SCALE_UP;
    }
}
