﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilityMethods {

    public interface FadeCallback {
        void OnFadeDone();
    }

    public interface SlideCallback {
        void OnSlideDone();
    }

    public interface PulsateCallback {
        void OnPulsateDone();
    }

    #region Public Methods

    // Fades in/out an object which has a CanvasGroup as a component by modifying its Alpha attribute.
    // callback.OnFadeDone called after animation is completed.
    public static void FadeCanvasGroupVisible(MonoBehaviour monoBehaviour, bool state, GameObject objectWithCanvasGroupAsComponent, float speed,
        UtilityMethods.FadeCallback callback = null) {
        // multiply speed for backwards compatibility (WaitForSeconds() -> speed * Time.deltaTime)
        monoBehaviour.StartCoroutine(FadeCanvasGroupVisibleAnimation(state, objectWithCanvasGroupAsComponent, speed * 40f, callback));
    }

    // Slides an object which has a RectTransform as a component to the destination.
    // destination must be anchoredPosition.
    // callback.OnSlideDone called after animation is completed.
    public static void Slide(MonoBehaviour monoBehaviour, RectTransform rectTransform, Vector3 destination, float initialSpeed, float acceleration,
        UtilityMethods.SlideCallback callback = null) {
        // multiply initialSpeed and acceleration for backwards compatibility (WaitForSeconds() -> speed * Time.deltaTime)
        monoBehaviour.StartCoroutine(SlideAnimation(rectTransform, destination, initialSpeed * 40f, acceleration * 2800f, callback));
    }

    // Pulsates an object wich has a RectTransform as a component.
    // callback.OnPulsateDone called after animation is completed.
    public static void Pulsate(MonoBehaviour monoBehaviour, RectTransform rectTransform,
        UtilityMethods.PulsateCallback callback = null, bool originalScaleOne = false) {
        // multiply speed for backwards compatibility (WaitForSeconds() -> speed * Time.deltaTime)
        monoBehaviour.StartCoroutine(PulsateAnimation(rectTransform, 1.6f, 0.02f * 40f, callback, originalScaleOne));
    }

    // Same as Pulsate but you can set the scale (localScale of the rectTransform at its biggest) and speed of the pulsation.
    public static void PulsateCustom(MonoBehaviour monoBehaviour, RectTransform rectTransform, float scale, float speed,
    UtilityMethods.PulsateCallback callback = null, bool originalScaleOne = false) {
        monoBehaviour.StartCoroutine(PulsateAnimation(rectTransform, scale, speed, callback, originalScaleOne));
    }

    #endregion

    #region Coroutines

    static IEnumerator FadeCanvasGroupVisibleAnimation(bool state, GameObject objectWithCanvasGroupAsComponent, float speed, UtilityMethods.FadeCallback callback) {
        CanvasGroup canvasGroup = objectWithCanvasGroupAsComponent.GetComponent<CanvasGroup>();
        
        if (state) {
            while (canvasGroup.alpha < 1f) {
                canvasGroup.alpha += speed * Time.deltaTime;
                yield return null;
            }
            canvasGroup.alpha = 1f;
        }
        else {
            while (canvasGroup.alpha > 0f) {
                canvasGroup.alpha -= speed * Time.deltaTime;
                yield return null;
            }
            canvasGroup.alpha = 0f;
        }
        if (callback != null) callback.OnFadeDone();
    }

    static IEnumerator SlideAnimation(RectTransform rectTransform, Vector3 destination, float initialSpeed, float acceleration, UtilityMethods.SlideCallback callback) {
        Vector3 position = rectTransform.anchoredPosition;
        Vector3 positionToDestination = destination - position;
        Vector3 direction = positionToDestination.normalized;

        float speed = initialSpeed;
        float halfMagnitude = positionToDestination.magnitude / 2f;
        Vector3 deltaVelocity = direction * speed * Time.deltaTime;

        while (Vector3.Dot(direction, (destination - (position + deltaVelocity)).normalized) > 0f) {
            deltaVelocity = direction * speed * Time.deltaTime;
            position += deltaVelocity;
            rectTransform.anchoredPosition = position;

            if ((destination - position).magnitude >= halfMagnitude) speed += acceleration * Time.deltaTime;
            else if (speed > initialSpeed) speed -= acceleration * Time.deltaTime;

            yield return null;
        }
        rectTransform.anchoredPosition = destination;

        if (callback != null) callback.OnSlideDone();
    }

    static IEnumerator PulsateAnimation(RectTransform rectTransform, float scale, float speed, UtilityMethods.PulsateCallback callback, bool originalScaleOne) {
        Vector2 originaScale = rectTransform.localScale;

        if (originalScaleOne) originaScale = Vector2.one;

        Vector2 currentScale = originaScale * scale;
        
        float deltaSpeed = speed * Time.deltaTime;

        while (currentScale.x - deltaSpeed > originaScale.x) {
            deltaSpeed = speed * Time.deltaTime;
            currentScale.x -= deltaSpeed;
            currentScale.y -= deltaSpeed;
            rectTransform.localScale = currentScale;

            yield return null;
        }
        rectTransform.localScale = originaScale;

        if (callback != null) callback.OnPulsateDone();
    }
    #endregion
}
