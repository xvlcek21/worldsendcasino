﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour {

    public enum AudioType { MUSIC, SFX, VOICE_ACTING };

    public static string ChooseRandom(string[] s)
    {
        int index = (int)Random.Range(0, s.Length - 0.0001f);

        return s[index];
    }

    public static string ChooseRandom(string[] s, ref int voiceLineRandomizingCounter)
    {
        int index = voiceLineRandomizingCounter;
        while (index == voiceLineRandomizingCounter)
        {
            index = (int)Random.Range(0, s.Length - 0.0001f);
        }

        voiceLineRandomizingCounter = index;

        return s[index];
    }

    public static void SetVibration(bool state) {
        if (state) {
            PlayerPrefs.SetInt(PlayerPrefsHelper.VIBRATION_KEY, 1);
            Handheld.Vibrate();
        }
        else PlayerPrefs.SetInt(PlayerPrefsHelper.VIBRATION_KEY, 0);
    }

    public static void SetMusic(bool state) {
        if (state) PlayerPrefs.SetInt(PlayerPrefsHelper.MUSIC_KEY, 1);
        else PlayerPrefs.SetInt(PlayerPrefsHelper.MUSIC_KEY, 0);
    }

    public static void SetSoundEffects(bool state) {
        if (state) PlayerPrefs.SetInt(PlayerPrefsHelper.SFX_KEY, 1);
        else PlayerPrefs.SetInt(PlayerPrefsHelper.SFX_KEY, 0);
    }

    public static void SetVoiceActing(bool state) {
        if (state) PlayerPrefs.SetInt(PlayerPrefsHelper.VOICE_ACTING_KEY, 1);
        else PlayerPrefs.SetInt(PlayerPrefsHelper.VOICE_ACTING_KEY, 0);
    }

    // Vibrate if vibration is set on
    // Default: vibrate
    public static void Vibrate() {
        if (PlayerPrefs.GetInt(PlayerPrefsHelper.VIBRATION_KEY, 1) == 1) Handheld.Vibrate();
    }

    // Play one shot fmodEvent if that type of sounds are set on 
    // Default: play the sound
    public static void PlayOneShot(string fmodEvent, Options.AudioType audioType) {
        if (audioType == AudioType.MUSIC && PlayerPrefs.GetInt(PlayerPrefsHelper.MUSIC_KEY, 1) == 1) FMODUnity.RuntimeManager.PlayOneShot(fmodEvent);
        else if (audioType == AudioType.SFX && PlayerPrefs.GetInt(PlayerPrefsHelper.SFX_KEY, 1) == 1) FMODUnity.RuntimeManager.PlayOneShot(fmodEvent);
        else if (audioType == AudioType.VOICE_ACTING && PlayerPrefs.GetInt(PlayerPrefsHelper.VOICE_ACTING_KEY, 1) == 1) FMODUnity.RuntimeManager.PlayOneShot(fmodEvent);
    }

    // Start fmodInstance if that type of sounds are set on 
    // Default: play the sound
    public static void StartFmodInstance(FMOD.Studio.EventInstance instance, Options.AudioType audioType) {
        if (audioType == AudioType.MUSIC && PlayerPrefs.GetInt(PlayerPrefsHelper.MUSIC_KEY, 1) == 1) instance.start();
        else if (audioType == AudioType.SFX && PlayerPrefs.GetInt(PlayerPrefsHelper.SFX_KEY, 1) == 1) instance.start();
        else if (audioType == AudioType.VOICE_ACTING && PlayerPrefs.GetInt(PlayerPrefsHelper.VOICE_ACTING_KEY, 1) == 1) instance.start();
    }

    public interface OneShotCallback {

        // didPlay == false if sounds of the type that was played was turned off from the Options menu
        void OnOneShotPlayed(bool didPlay);
    }

    public static void PlayOneShot(MonoBehaviour monoBehaviour, string fmodEvent, Options.AudioType audioType, OneShotCallback callback) {
        monoBehaviour.StartCoroutine(PlayOneShotAndCallback(fmodEvent, audioType, callback));
    }
    static IEnumerator PlayOneShotAndCallback(string fmodEvent, Options.AudioType audioType, OneShotCallback callback) {
        bool permissionToPlay = false;

        if (audioType == AudioType.MUSIC && PlayerPrefs.GetInt(PlayerPrefsHelper.MUSIC_KEY, 1) == 1) permissionToPlay = true;
        else if (audioType == AudioType.SFX && PlayerPrefs.GetInt(PlayerPrefsHelper.SFX_KEY, 1) == 1) permissionToPlay = true;
        else if (audioType == AudioType.VOICE_ACTING && PlayerPrefs.GetInt(PlayerPrefsHelper.VOICE_ACTING_KEY, 1) == 1) permissionToPlay = true;

        if (permissionToPlay) {
            FMOD.Studio.EventInstance eventInstance = null;
            eventInstance = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);

            Options.StartFmodInstance(eventInstance, Options.AudioType.VOICE_ACTING);

            if (eventInstance != null) {
                FMOD.Studio.PLAYBACK_STATE playbackState;

                while (true) {
                    eventInstance.getPlaybackState(out playbackState);

                    if (playbackState != FMOD.Studio.PLAYBACK_STATE.STOPPED) yield return null;
                    break;
                }
            }
            eventInstance.release();
            callback.OnOneShotPlayed(true);
        }
        else callback.OnOneShotPlayed(false);
    }
}