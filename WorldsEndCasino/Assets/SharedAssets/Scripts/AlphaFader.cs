﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
Change objects alpha value during runtime in a smooth animation
The script must be attached to an object with an Image component
*/
public class AlphaFader : MonoBehaviour {

    // Call RegisterCallbackInterface, and pass it an object implementing this interface
    //  if you wan't to do something after the fade animation has ended (or is in some other trigger point)
    public interface CallbackInterface {

        // Called in AlphaFader.FadeOut() when Image.color.a passes fadeInCallbackTriggerPoint
        void OnFadeIn();

        // Called in AlphaFader.FadeIn() when Image.color.a passes fadeOutCallbackTriggerPoint
        void OnFadeOut();
    }

    public float speed = 0.5f;

    [Space]
    [Header("Values between 0 - 1")]
    public float initialAlpha = 0f;
    public float fadeInCallbackTriggerPoint = 1f;
    public float fadeOutCallbackTriggerPoint = 0f;

    enum Status { WAIT, FADE_IN, FADE_OUT }
    Status status = Status.WAIT;
    Color color;
    CallbackInterface callbackInterface;
    bool callbackTriggered = false;

    void Awake() {
        color = GetComponent<Image>().color;
        color.a = initialAlpha;
        GetComponent<Image>().color = color;
    }

    void Update() {
        switch(status) {
            case Status.WAIT:
                return;
            case Status.FADE_IN:
                color = GetComponent<Image>().color;
                color.a += speed * Time.deltaTime;
                GetComponent<Image>().color = color;

                if (callbackInterface != null && color.a >= fadeInCallbackTriggerPoint) {
                    if (color.a >= 1f) {
                        status = Status.WAIT;
                        callbackTriggered = false;
                    }
                    if (!callbackTriggered) callbackInterface.OnFadeIn();
                    callbackTriggered = true;
                }
                return;
            case Status.FADE_OUT:
                color = GetComponent<Image>().color;
                color.a -= speed * Time.deltaTime;
                GetComponent<Image>().color = color;

                if (callbackInterface != null && color.a <= fadeOutCallbackTriggerPoint) {
                    if (color.a <= 0f) {
                        callbackTriggered = false;
                        status = Status.WAIT;
                    }
                    if (!callbackTriggered) callbackInterface.OnFadeOut();
                    callbackTriggered = true;
                }
                return;
        }
    }

    public void RegisterCallbackInterface(AlphaFader.CallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public void FadeIn() {
        status = Status.FADE_IN;
        callbackTriggered = false;
    }

    public void FadeOut() {
        status = Status.FADE_OUT;
        callbackTriggered = false;
    }
}