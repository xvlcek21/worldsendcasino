﻿using UnityEngine;
using UnityEngine.UI;

public class TurnOrderInfo : MonoBehaviour
{

    public enum Direction
    {
        UP, DOWN
    }

    public Sprite nextUpSprite;
    public Sprite nextDownSprite;

    public void SetDirection(TurnOrderInfo.Direction direction)
    {
        switch (direction)
        {
            case TurnOrderInfo.Direction.UP:
                GetComponent<Image>().sprite = nextUpSprite;
                UtilityMethods.Pulsate(this, GetComponent<RectTransform>());
                return;
            case TurnOrderInfo.Direction.DOWN:
                GetComponent<Image>().sprite = nextDownSprite;
                UtilityMethods.Pulsate(this, GetComponent<RectTransform>());
                return;
        }
    }
}