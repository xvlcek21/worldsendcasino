﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfoBoxes : MonoBehaviour {

    public GameObject playerInfoBoxPrefab;
    public Sprite playerInfoboxSprite;
    public Sprite currentPlayerInfoboxSprite;

    float infoBoxHeight = 70f;

    List<GameObject> playerInfoBoxes;
    int currentPlayerIndex;

    Vector2 updatedPosition = Vector2.zero;

    void UpdatePositions() {
        int place = 0;
        updatedPosition.y = 0f;

        for (int infoBoxIndex = currentPlayerIndex; infoBoxIndex < playerInfoBoxes.Count; ++infoBoxIndex) {
            RectTransform rectTransform = playerInfoBoxes[infoBoxIndex].GetComponent<RectTransform>();
            updatedPosition.y = -infoBoxHeight * place++;
            rectTransform.anchoredPosition = updatedPosition;
        }
        updatedPosition.y = 0f;

        for (int infoBoxIndex = 0; infoBoxIndex < currentPlayerIndex; ++infoBoxIndex) {
            RectTransform rectTransform = playerInfoBoxes[infoBoxIndex].GetComponent<RectTransform>();
            updatedPosition.y = -infoBoxHeight * place++;
            rectTransform.anchoredPosition = updatedPosition;
        }
    }

    public void Initialize(int numberOfPlayers) {
        currentPlayerIndex = 0;

        if (playerInfoBoxes != null) {
            for (int infoBoxIndex = 0; infoBoxIndex < playerInfoBoxes.Count; ++infoBoxIndex) {
                Destroy(playerInfoBoxes[infoBoxIndex]);
            }
            playerInfoBoxes.Clear();
        }
        else playerInfoBoxes = new List<GameObject>(numberOfPlayers);

        for (int infoBoxIndex = 0; infoBoxIndex < numberOfPlayers; ++infoBoxIndex) {
            GameObject playerInfoBox = Instantiate(playerInfoBoxPrefab);
            playerInfoBox.transform.SetParent(transform, false);

            RectTransform rectTransform = playerInfoBox.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2(0f, -infoBoxHeight * infoBoxIndex);

            playerInfoBoxes.Add(playerInfoBox);
        }
        playerInfoBoxes[currentPlayerIndex].GetComponent<Image>().sprite = currentPlayerInfoboxSprite;
    }

    public void SetInfo(int playerIndex, string name, string money) {
        Text[] texts = playerInfoBoxes[playerIndex].GetComponentsInChildren<Text>();

        for (int textIndex = 0; textIndex < texts.Length; ++textIndex) {
            if (texts[textIndex].tag.Equals("PlayerInfoName")) texts[textIndex].text = name;
            else if (texts[textIndex].tag.Equals("PlayerInfoMoney")) texts[textIndex].text = money;
        }
    }

    public void SetMoney(int playerIndex, string money, bool pulsate = true) {
        Text[] texts = playerInfoBoxes[playerIndex].GetComponentsInChildren<Text>();

        for (int textIndex = 0; textIndex < texts.Length; ++textIndex) {
            if (texts[textIndex].tag.Equals("PlayerInfoMoney")) {
                texts[textIndex].text = money;
                if (pulsate) UtilityMethods.Pulsate(this, texts[textIndex].rectTransform);
                return;
            }
        }
    }

    public void ChangePlayer(string name, string money, bool reversed = false) {
        if (isActiveAndEnabled) StartCoroutine(ChangePlayerAnimation(name, money, reversed));
    }

    public void ResetHighlight()
    {
        for (int i = 0; i < playerInfoBoxes.Count; ++i)
        {
            playerInfoBoxes[i].GetComponent<Image>().sprite = playerInfoboxSprite;
        }
    }

    public void SetHighlight(int index = -1)
    {
        if (index == -1)
            index = currentPlayerIndex;

        ResetHighlight();

        playerInfoBoxes[index].GetComponent<Image>().sprite = currentPlayerInfoboxSprite;
    }

    public void KillCurrentPlayer(bool turnsReversed, int index = -1) {
        if (index == -1)
            index = currentPlayerIndex;
            
        Destroy(playerInfoBoxes[index]);
        playerInfoBoxes.RemoveAt(index);

        // Passing the bust index correction.
        if (index != -1 && index < currentPlayerIndex)
        {
            --currentPlayerIndex;
        }

        /*
         if deletedindex < currentindex -----> --currentindex;
         elseif deletedindex == currentindex -----> if (currentPlayerIndex >= playerInfoBoxes.Count) ----->
         elseif deletedindex > currentindex -----> currentindex stays the same
         */

        if (!turnsReversed) {
            if (currentPlayerIndex >= playerInfoBoxes.Count) currentPlayerIndex = 0;
        }
        else {
            --currentPlayerIndex;

            if (currentPlayerIndex < 0) currentPlayerIndex = playerInfoBoxes.Count - 1;
        }
        UpdatePositions();
        playerInfoBoxes[currentPlayerIndex].GetComponent<Image>().sprite = currentPlayerInfoboxSprite;
    }
    
    IEnumerator ChangePlayerAnimation(string name, string money, bool turnsReversed) {
        const float MOVEMENT_SPEED = 400f;

        int firstPlayerToMoveIndex = currentPlayerIndex;

        // Move the last player first if turns are reversed
        if (turnsReversed) {
            firstPlayerToMoveIndex = currentPlayerIndex - 1;
            if (firstPlayerToMoveIndex < 0) firstPlayerToMoveIndex = playerInfoBoxes.Count - 1;
        }

        // Move the current/last player left until invisible
        RectTransform rectTransform = playerInfoBoxes[firstPlayerToMoveIndex].GetComponent<RectTransform>();
        Vector2 newPosition = rectTransform.anchoredPosition;
        float originalX = newPosition.x;

        while (newPosition.x > -130) {
            newPosition.x -= MOVEMENT_SPEED * Time.deltaTime;
            rectTransform.anchoredPosition = newPosition;
            yield return null;
        }

        playerInfoBoxes[currentPlayerIndex].GetComponent<Image>().sprite = playerInfoboxSprite;

        // Move everybody else up/down
        float heightTraveled = 0;
        float deltaSpeed;

        newPosition.x = originalX;

        do {
            deltaSpeed = MOVEMENT_SPEED * Time.deltaTime;
            if (heightTraveled + deltaSpeed > infoBoxHeight) deltaSpeed = infoBoxHeight - heightTraveled;

            for (int infoBoxIndex = 0; infoBoxIndex < playerInfoBoxes.Count; ++infoBoxIndex) {
                if (infoBoxIndex != firstPlayerToMoveIndex) {
                    rectTransform = playerInfoBoxes[infoBoxIndex].GetComponent<RectTransform>(); // OPTIMIZE: Copy to a temporary RectTransform list?                    

                    if (!turnsReversed) newPosition.y = rectTransform.anchoredPosition.y + deltaSpeed;
                    else newPosition.y = rectTransform.anchoredPosition.y - deltaSpeed;

                    rectTransform.anchoredPosition = newPosition;
                }
            }
            yield return null;

            heightTraveled += deltaSpeed;
        } while (heightTraveled < infoBoxHeight);

        // Move the current/last player to the bottom/top(instantly) and back to the right
        rectTransform = playerInfoBoxes[firstPlayerToMoveIndex].GetComponent<RectTransform>();
        newPosition = rectTransform.anchoredPosition;

        if (!turnsReversed) newPosition.y = -infoBoxHeight * (playerInfoBoxes.Count - 1);
        else newPosition.y = 0;

        while (newPosition.x < originalX) {
            newPosition.x += MOVEMENT_SPEED * Time.deltaTime;
            rectTransform.anchoredPosition = newPosition;
            yield return null;
        }
        // Correct float rounding error when at the destination. Is this necessary?
        if (newPosition.x != originalX) {
            newPosition.x = originalX;
            rectTransform.anchoredPosition = newPosition;
        }

        if (!turnsReversed) {
            ++currentPlayerIndex;

            if (currentPlayerIndex >= playerInfoBoxes.Count) currentPlayerIndex = 0;
        }
        else {
            --currentPlayerIndex;

            if (currentPlayerIndex < 0) currentPlayerIndex = playerInfoBoxes.Count - 1;
        }
        playerInfoBoxes[currentPlayerIndex].GetComponent<Image>().sprite = currentPlayerInfoboxSprite;
    }
}