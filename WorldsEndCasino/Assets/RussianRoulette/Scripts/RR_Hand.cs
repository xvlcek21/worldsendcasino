﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RR_Hand : MonoBehaviour {

    #region Public Variables

    public GameObject host;

    [Space]
    public Image fingeringHand;
    public Image lungingHand;
    public Image fist;
    
    [Space]
    [FMODUnity.EventRef]
    public string crushEvent;
    
    #endregion

    #region Private Variables

    const float minScale = 0.8f;
            
    Vector3 handScale = new Vector3(minScale, minScale, 1f);

    Coroutine increaceHandSizeAnimationCoroutine = null;

    Vector3 initialHandPosition;
    #endregion

    #region MonoBehabviour Callbacks

    void Awake() {
        initialHandPosition = lungingHand.rectTransform.anchoredPosition;

        Reset();
    }

    #endregion

    #region Public Methods

    public void ShowFinger() {
        StopCoroutine(increaceHandSizeAnimationCoroutine);
        fingeringHand.GetComponent<RectTransform>().localScale = handScale;
        lungingHand.gameObject.SetActive(false);
        fingeringHand.gameObject.SetActive(true);
        fist.gameObject.SetActive(false);
    }

    public void LungeAndShowDeathScreen(ref DeathScreen deathScreen, string message, DeathScreen.CallbackInterface callbackInterface) {
        fingeringHand.gameObject.SetActive(false);
        lungingHand.gameObject.SetActive(true);
        fist.gameObject.SetActive(false);
        deathScreen.ShowHandCrush(callbackInterface, message, ref lungingHand);
    }

    public void ShowFistAndShrink() {
        fist.rectTransform.localScale = lungingHand.rectTransform.localScale;

        fingeringHand.gameObject.SetActive(false);
        lungingHand.gameObject.SetActive(false);
        fist.gameObject.SetActive(true);

        if (increaceHandSizeAnimationCoroutine != null) StopCoroutine(increaceHandSizeAnimationCoroutine);
        StartCoroutine(ShrinkHandBackToNormal());
    }

    public void Reset() {
        if (increaceHandSizeAnimationCoroutine != null) StopCoroutine(increaceHandSizeAnimationCoroutine);

        lungingHand.gameObject.SetActive(true);
        fist.gameObject.SetActive(false);
        fist.gameObject.SetActive(false);

        handScale.x = minScale;
        handScale.y = minScale;
        lungingHand.rectTransform.localScale = handScale;
        lungingHand.rectTransform.anchoredPosition = initialHandPosition;
    }

    public void IncreaseHandSize() {
        lungingHand.gameObject.SetActive(true);
        fingeringHand.gameObject.SetActive(false);

        if (increaceHandSizeAnimationCoroutine != null) StopCoroutine(increaceHandSizeAnimationCoroutine);
        increaceHandSizeAnimationCoroutine = StartCoroutine(IncreaseHandSizeAnimation());
    }

    public void StopHandSizeGrowing() {
        if (increaceHandSizeAnimationCoroutine != null) StopCoroutine(increaceHandSizeAnimationCoroutine);
    }

    public void FadeIn(float fadeSpeed) {
        UtilityMethods.FadeCanvasGroupVisible(this, true, gameObject, fadeSpeed);
        UtilityMethods.FadeCanvasGroupVisible(this, true, host, fadeSpeed);
    }

    public void FadeOut(float fadeSpeed) {
        UtilityMethods.FadeCanvasGroupVisible(this, false, gameObject, fadeSpeed);
        UtilityMethods.FadeCanvasGroupVisible(this, false, host, fadeSpeed);
    }

    #endregion
    
    IEnumerator IncreaseHandSizeAnimation() {
        float increment = 0.04f;
        float deltaIncrement;

        while (handScale.x < 2f) {
            deltaIncrement = increment * Time.deltaTime;
            handScale.x += deltaIncrement;
            handScale.y += deltaIncrement;
            lungingHand.rectTransform.localScale = handScale;
            yield return null;
        }
    }

    IEnumerator ShrinkHandBackToNormal() {
        float decrement = 0.04f;
        float deltaDecrement;

        while (handScale.x > minScale) {
            deltaDecrement = decrement * Time.deltaTime;
            handScale.x -= deltaDecrement;
            handScale.y -= deltaDecrement;
            fist.rectTransform.localScale = handScale;
            yield return null;
        }
        handScale.x = minScale;
        handScale.y = minScale;
        fist.rectTransform.localScale = handScale;
    }
}