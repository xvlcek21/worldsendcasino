﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace WorldsEndCasino {

    public class Rules : MonoBehaviour, BackButtonConfirmation.BackButtonCallbackInterface, AlphaFader.CallbackInterface, UtilityMethods.SlideCallback {

        public string nextSceneName;
        public string backButtonSceneName;

        [Space]
        public float contentOffsetX;
        public int numberOfRuleBenderPages;

        [Space]
        public RectTransform[] bulletPoints;
        public RectTransform buttons;

        [Space]
        public RectTransform content;
        public RectTransform headings;

        [Space]

        [FMODUnity.EventRef]
        public string[] entryHostLines;
        
        [FMODUnity.EventRef]
        public string bulletPointEntryEvent;

        [Space]
        public BackButtonConfirmation backbuttonConfirmationScript;
        public AlphaFader black;
        public HandSwipeTransition handSwipeAnimation;

        // -> Horrible design. BJ specific stuff
        [Space]
        public GameObject blackjackRulesButton;
        bool blackjackRulesZoomed = false;
        // <- Horrible design

        enum Page { RULES_PAGE, RULE_BENDER_PAGE }
        Page currentPage = Page.RULES_PAGE;

        // MIN == 1, MAX == numberOfRuleBenderPages
        int selectedRuleBenderPage = 1;

        bool rulesShown = false;

        FMOD.Studio.EventInstance hostLineInstance = null;

        Coroutine showRulesCoroutine = null;

        void Awake() {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            black.RegisterCallbackInterface(this);
            backbuttonConfirmationScript.RegisterCallbackInterface(this);
        }

        void Start() {
            black.FadeOut();
        }

        public void OnBackButtonConfirmed() {
            SceneManager.LoadScene(backButtonSceneName);
        }

        void OnDestroy() {
            if (hostLineInstance != null) {
                FMOD.Studio.PLAYBACK_STATE playbackState;
                hostLineInstance.getPlaybackState(out playbackState);
                if (playbackState != FMOD.Studio.PLAYBACK_STATE.STOPPED) hostLineInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                hostLineInstance.release();
            }
        }

        public void Continue() {
            if (currentPage == Page.RULES_PAGE) {
                selectedRuleBenderPage = 1;
                StartCoroutine(ChangePage(Page.RULE_BENDER_PAGE));
            }
            else if (currentPage == Page.RULE_BENDER_PAGE) {
                if (selectedRuleBenderPage < numberOfRuleBenderPages) {
                    ++selectedRuleBenderPage;
                    StartCoroutine(ChangePage(Page.RULE_BENDER_PAGE));
                }
                else {
                    PlayerPrefs.DeleteKey(PlayerPrefsHelper.GAME_KEY);
                    handSwipeAnimation.ChangeScene(nextSceneName);
                }
            }
        }

        public void Back() {
            if (currentPage == Page.RULE_BENDER_PAGE) {
                if (selectedRuleBenderPage > 1) {
                    --selectedRuleBenderPage;
                    StartCoroutine(ChangePage(Page.RULE_BENDER_PAGE));
                }
                else StartCoroutine(ChangePage(Page.RULES_PAGE));
            }
            else if (currentPage == Page.RULES_PAGE) SceneManager.LoadScene(backButtonSceneName);
        }

        public void SkipRules() {
            if (rulesShown) return;

            if (showRulesCoroutine != null) StopCoroutine(showRulesCoroutine);

            Vector3 destination;

            for (int i = 0; i < bulletPoints.Length; ++i) {
                destination = bulletPoints[i].anchoredPosition;
                destination.x = 0f;
                bulletPoints[i].anchoredPosition = destination;
            }
            OnSlideDone();

            destination = buttons.anchoredPosition;
            destination.y = 43f;
            buttons.anchoredPosition = destination;

            rulesShown = true;
        }

        IEnumerator ChangePage (Page page) {
            Vector3 destination = content.anchoredPosition;

            switch (page) {
                case Page.RULES_PAGE:
                    currentPage = Page.RULES_PAGE;
                    destination.x = 0f;
                    UtilityMethods.Slide(this, content, destination, 10f, 1.2f);
                    UtilityMethods.Slide(this, headings, destination, 10f, 1.2f);
                    break;
                case Page.RULE_BENDER_PAGE:
                    currentPage = Page.RULE_BENDER_PAGE;
                    destination.x = -contentOffsetX * selectedRuleBenderPage;
                    UtilityMethods.Slide(this, content, destination, 10f, 1.2f);

                    // Heading doesn't move if we are already on the rule bender page
                    if (selectedRuleBenderPage == 1) UtilityMethods.Slide(this, headings, destination, 10f, 1.2f);
                    break;
            }
            yield break;
        }

        IEnumerator HostEntryLine() {
            hostLineInstance = FMODUnity.RuntimeManager.CreateInstance(entryHostLines[UnityEngine.Random.Range(0, entryHostLines.Length)]);
            Options.StartFmodInstance(hostLineInstance, Options.AudioType.VOICE_ACTING);

            if (hostLineInstance != null) {
                FMOD.Studio.PLAYBACK_STATE playbackState;

                while (true) {
                    hostLineInstance.getPlaybackState(out playbackState);
                    if (playbackState != FMOD.Studio.PLAYBACK_STATE.STOPPED) yield return null;
                    else break;
                }
            }
            // rules are already shown if the screen was clicked before this
            if (!rulesShown) showRulesCoroutine = StartCoroutine(ShowRules());
        }

        IEnumerator ShowRules() {
            Vector3 destination;

            for (int i = 0; i < bulletPoints.Length; ++i) {
                destination = bulletPoints[i].anchoredPosition;
                destination.x = 0f;

                UtilityMethods.Slide(this, bulletPoints[i], destination, 80f, 0.2f, this);
                
                Options.PlayOneShot(bulletPointEntryEvent, Options.AudioType.SFX);
                yield return new WaitForSeconds(1.2f);
            }

            destination = buttons.anchoredPosition;
            destination.y = 43f;

            UtilityMethods.Slide(this, buttons, destination, 80f, 0.2f);

            Options.PlayOneShot(bulletPointEntryEvent, Options.AudioType.SFX);

            rulesShown = true;
        }

        public void OnFadeIn() {
        }

        public void OnFadeOut() {
            StartCoroutine(HostEntryLine());
        }

        // Horrible design. BJ specific stuff
        public void OnSlideDone() {
            if (blackjackRulesButton == null || blackjackRulesZoomed) return;
            blackjackRulesButton.SetActive(true);
            blackjackRulesButton.GetComponent<Zoomer>().Zoom(1f, 10f);
        }
    }
}