﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class MoneyManager
{
    #region constants
    const int startingBalance = 2000;
    const int minimalBet = 1000;
    #endregion 

    #region private variables

    int activeBetAmount;
    MoneyPopup moneyPopup;

    #endregion

    #region private methods
    #endregion

    #region public methods

    public void Init(ref MoneyPopup _moneyPopup)
    {
        activeBetAmount = 0;
        moneyPopup = _moneyPopup;
    }

    public void ModifyPlayerBalance(ref int balance, int changingValue, bool useMoneyPopup = true)
    {
        balance += changingValue;
        if (useMoneyPopup)
        {
            if (changingValue > 0)
                moneyPopup.ShowIncrease(changingValue.ToString());
            //else
                //moneyPopup.ShowDecrease((-changingValue).ToString());
        }
    }

    public bool IsBroke(ref int balance)
    {
        return balance < minimalBet;
    }

    public void Bet(ref int balance, int bet)
    {
        activeBetAmount = bet;
        ModifyPlayerBalance(ref balance, -bet);
        moneyPopup.ShowDecrease(bet.ToString());
    }

    public void BustTaken(ref int balance)
    {
        ModifyPlayerBalance(ref balance, -1000);
    }

    public void Push(ref int balance)
    {
        ModifyPlayerBalance(ref balance, activeBetAmount);
        activeBetAmount = 0;
    }

    public void Win(ref int balance, bool blackjack)
    {
        if (blackjack)
            ModifyPlayerBalance(ref balance, activeBetAmount * 3);
        else
            ModifyPlayerBalance(ref balance, activeBetAmount * 2);
        activeBetAmount = 0;
    }

    public void Lose(ref int balance, bool blackjack)
    {
        if (blackjack)
            ModifyPlayerBalance(ref balance, -1000);
        activeBetAmount = 0;
    }

    public void BuyRuleBender(ref int balance, ref int price, int priceIncrease)
    {
        ModifyPlayerBalance(ref balance, -price, false);
        price += priceIncrease;
    }

    public void DoubleDown(ref int balance)
    {
        ModifyPlayerBalance(ref balance, -1000);
        activeBetAmount *= 2;
    }

    public void BustPassed(ref int balance, bool popUp = true)
    {
        ModifyPlayerBalance(ref balance, activeBetAmount, popUp);
        activeBetAmount = 0;
    }

    public bool IsPossibleToBuy(ref int balance, ref int price)
    {
        return balance >= price;
    }

    public int GetStartingBalance()
    {
        return startingBalance;
    }

    public int GetActiveBetAmount()
    {
        return activeBetAmount;
    }

    public string GetActiveBetAmountAsString()
    {
        return activeBetAmount.ToString();
    }

    #endregion

}

