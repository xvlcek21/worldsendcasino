﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class BJHostMovement : MonoBehaviour {

    #region private variables

    public Transform hand;
    private Transform body;
    private Vector2 bodyPosition = new Vector2();
    private Vector2 handPosition = new Vector2();

    #endregion

    #region public variables

    public Movement bodyMovement;
    public Movement handMovement;

    public float handMovementOffset;

    #endregion

    void Start () {
        body = transform;

        bodyMovement = new Movement();

        InitializeMovementClass(ref body, ref bodyMovement);
        InitializeMovementClass(ref hand, ref handMovement);

        // the hand movement should not be synchronized with the body movement, henceforth we introduce following offset.
        handMovement.pivotPointY += handMovementOffset;
    }
	
	void Update () {
        UpdateMovementSpeed(ref body, ref bodyMovement);
        UpdateMovementSpeed(ref hand, ref handMovement);

        // Move
        bodyPosition.x = body.localPosition.x;
        bodyPosition.y = body.localPosition.y + bodyMovement.movementSpeed;
        body.localPosition = bodyPosition;

        handPosition.x = hand.localPosition.x;
        handPosition.y = hand.localPosition.y + handMovement.movementSpeed;
        hand.localPosition = handPosition;
    }

    #region private methods

    private void InitializeMovementClass(ref Transform t, ref Movement m)
    { // Since these properties are not going to be reused and/or modified anywhere else, we can justify leaving the initialisation here.
        m.movementSpeed = 0.2f;
        m.initialMovementSpeed = m.movementSpeed;
        m.spread = 0.8f;
        m.interpolationSpeed = 0.01f;
        m.pivotPointY = t.localPosition.y;
    }

    void UpdateMovementSpeed(ref Transform t, ref Movement m)
    {
        if (t.localPosition.y > m.pivotPointY + m.spread)
        {
            if (m.movementSpeed > 0.02f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, -m.initialMovementSpeed, m.interpolationSpeed);

        }
        else if (t.localPosition.y < m.pivotPointY - m.spread)
        {
            if (m.movementSpeed < -0.05f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, m.initialMovementSpeed, m.interpolationSpeed);
        }
    }

    #endregion



}
