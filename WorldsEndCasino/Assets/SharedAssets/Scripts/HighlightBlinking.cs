﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighlightBlinking : MonoBehaviour {

    private GameObject blinkingImage;
    private const float blinkingSpeed = 0.2f;
    
	void Awake () {
        blinkingImage = transform.GetChild(0).gameObject;
	}

    private void OnEnable()
    {
        StartCoroutine(Blink());
    }

    private IEnumerator Blink()
    {
        while (true)
        {
            if (gameObject.activeSelf)
            {
                blinkingImage.SetActive(!blinkingImage.activeSelf);
            }
            yield return new WaitForSeconds(blinkingSpeed);
        }
    }
}
