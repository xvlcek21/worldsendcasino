﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RR_FlashPanel : MonoBehaviour {
    
    public void Flash() {
        gameObject.SetActive(true);
        StartCoroutine(FlashAnimation());
    }

    void EndFlash() {
        gameObject.SetActive(false);
    }

    IEnumerator FlashAnimation() {
        yield return new WaitForSeconds(0.05f);
        EndFlash();
    }
}
