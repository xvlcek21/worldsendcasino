﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public interface CallbackInterface {
        void OnTick(int number);
        void OnTimeUp();
    }

    #region Public Variables

    public Sprite[] numberSprites;

    [FMODUnity.EventRef]
    public string[] timerClickEvent;

    [FMODUnity.EventRef]
    public string countDownEvent;

    [FMODUnity.EventRef]
    public string longerCountDownEvent = null;

    [FMODUnity.EventRef]
    public string[] timeOutEvents;

    int time;
    Coroutine countDownCoroutine = null;
    FMOD.Studio.EventInstance countDownInstance;
    FMOD.Studio.EventInstance longerCountDownInstance;

    #endregion

    void Start() {
        countDownInstance = FMODUnity.RuntimeManager.CreateInstance(countDownEvent);
        if (longerCountDownEvent != null && longerCountDownEvent.Length > 0) longerCountDownInstance = FMODUnity.RuntimeManager.CreateInstance(longerCountDownEvent);
    }

    #region Public Methods

    public void StopCountdown() {
        countDownInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        if (longerCountDownInstance != null) longerCountDownInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);

        if (countDownCoroutine != null) {
            StopCoroutine(countDownCoroutine);
            countDownCoroutine = null;
        }
    }

    public bool StartCountDown(int startingTime, float tickDuration, Timer.CallbackInterface callbackInterface, bool longer = false) {
        if (countDownCoroutine == null) countDownCoroutine = StartCoroutine(CountDown(startingTime, tickDuration, callbackInterface, longer));
        else return false;
        return true;
    }

    public void ReleaseResources() {
        countDownInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        countDownInstance.release();

        if (longerCountDownInstance != null) {
            longerCountDownInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            longerCountDownInstance.release();
        }
    }
    
    #endregion

    #region Coroutines

    IEnumerator CountDown(int startingTime, float tickDuration, Timer.CallbackInterface callbackInterface, bool longer = false) {
        time = startingTime;

        // Extra tick without a number on the screen
        yield return new WaitForSeconds(tickDuration);

        if (!longer) Options.StartFmodInstance(countDownInstance, Options.AudioType.MUSIC);
        else Options.StartFmodInstance(longerCountDownInstance, Options.AudioType.MUSIC);

        while (time > 0) {
            Options.PlayOneShot(timerClickEvent[time % 2], Options.AudioType.SFX);
            StartCoroutine(TickAnimation(time));
            callbackInterface.OnTick(time);
            yield return new WaitForSeconds(tickDuration);
            --time;
        }
        StartCoroutine(TickAnimation(0));

        countDownCoroutine = null;
        Options.PlayOneShot(timeOutEvents[UnityEngine.Random.Range(0, timeOutEvents.Length)], Options.AudioType.VOICE_ACTING);
        if (longerCountDownInstance != null) longerCountDownInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        callbackInterface.OnTimeUp();
    }

    IEnumerator TickAnimation(int number) {
        Image image = gameObject.GetComponent<Image>();
        image.sprite = numberSprites[number];

        RectTransform rectTransform = GetComponent<RectTransform>();
        Vector3 scale = rectTransform.localScale;

        float alpha = 1f;
        Color color = image.color;

        color.a = alpha;
        image.color = color;
        scale.x = 1f;
        scale.y = 1f;
        rectTransform.localScale = scale;

        while(alpha > 0f) {
            if (scale.x > 0f) {
                scale.x -= 0.008f;
                scale.y -= 0.008f;
                rectTransform.localScale = scale;
            }
            alpha -= 0.5f * Time.deltaTime;
            color.a = alpha;
            image.color = color;
            yield return null;
        }
    }

    #endregion
}