﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BJ_Host : MonoBehaviour {

    public GameObject hand;

    public void FadeIn(float fadeSpeed) {
        UtilityMethods.FadeCanvasGroupVisible(this, true, gameObject, fadeSpeed);
        UtilityMethods.FadeCanvasGroupVisible(this, true, hand, fadeSpeed);
    }

    public void FadeOut(float fadeSpeed) {
        UtilityMethods.FadeCanvasGroupVisible(this, false, gameObject, fadeSpeed);
        UtilityMethods.FadeCanvasGroupVisible(this, false, hand, fadeSpeed);
    }
}
