﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {
    
    public int id;
    public int money;
    public string name;

    public Player(int id, int money, string name) {
        this.id = id;
        this.money = money;
        this.name = name;
    }
}