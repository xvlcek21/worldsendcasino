﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RR_PlaceGunDown : MonoBehaviour {

    public GameObject gun;

    public void GunPlacedDown(float x) {
        gun.SetActive(true);
    }
}
