﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathScreen : MonoBehaviour, UtilityMethods.FadeCallback {

    public interface CallbackInterface {

        // Called when the screen gets cracked after calling ShowHandCursh()
        void OnScreenCracked(string message);
    }

    public enum Type { HAND_CRUSH, BULLET_HOLE };

    public Image crackImage;
    public Sprite crackSprite;
    public Sprite crackBulletHoleSprite;
    public Sprite handOpenSprite;
    public Shaker shaker;
    public Shaker backgroundShaker;

    [FMODUnity.EventRef]
    public string crackEvent;

    DeathScreen.CallbackInterface handZoomedInterface;
    string message;

    Image hand;
    Vector2 initialScale = Vector2.one;
    Sprite initialSprite;
    Text instructionsText;

    void Awake() {
        Text[] texts = GetComponentsInChildren<Text>();
        
        for (int i = 0; i < texts.Length; ++i) {
            if (texts[i].CompareTag("DeathScreenInstructions")) {
                instructionsText = texts[i];
                break;
            }
        }
        crackImage.sprite = crackSprite;
    }

    public void Hide() {
        UtilityMethods.FadeCanvasGroupVisible(this, false, gameObject, 0.2f);
    }

    // Only in Russian Roulette
    public void ShowBulletHole(string instructions) {
        if (instructions != null) instructionsText.text = instructions;

        crackImage.sprite = crackBulletHoleSprite;
        UtilityMethods.FadeCanvasGroupVisible(this, true, gameObject, 0.2f);
        shaker.Shake();
        backgroundShaker.ShakeHorizontally();
        Options.PlayOneShot(crackEvent, Options.AudioType.SFX);
        Options.Vibrate();
    }

    // Call when you die in blackjack (also time up in Russian Roulette)
    public void ShowHandCrush(DeathScreen.CallbackInterface callbackInterface, string message, ref Image hand) {
        this.hand = hand;
        
        initialScale.x = hand.GetComponent<RectTransform>().localScale.x;
        initialScale.y = hand.GetComponent<RectTransform>().localScale.y;
        initialSprite = hand.sprite;

        crackImage.sprite = crackSprite;
        this.handZoomedInterface = callbackInterface;
        this.message = message;
        instructionsText.text = message;
        StartCoroutine(ShowHandCrushAnimation(callbackInterface, hand));
    }

    IEnumerator ShowHandCrushAnimation(DeathScreen.CallbackInterface callbackInterface, Image hand) {
        hand.gameObject.SetActive(true);
        hand.sprite = handOpenSprite;
        Vector3 scale = hand.GetComponent<RectTransform>().localScale;

        // Hand lunges forward
        float deltaScale = 50 * Time.deltaTime;

        while (scale.x + deltaScale < 6.5f) {
            deltaScale = 50 * Time.deltaTime;
            scale.x += deltaScale;
            scale.y += deltaScale;
            hand.GetComponent<RectTransform>().localScale = scale;
            yield return null;
        }
        scale.x = 6.5f;
        scale.y = 6.5f;
        hand.GetComponent<RectTransform>().localScale = scale;

        // Screen cracks
        Options.PlayOneShot(crackEvent, Options.AudioType.SFX);
        shaker.Shake();
        backgroundShaker.ShakeHorizontally();
        Options.Vibrate();

        UtilityMethods.FadeCanvasGroupVisible(this, true, gameObject, 0.2f, this);
    }

    public void OnFadeDone() {
        hand.GetComponent<RectTransform>().localScale = initialScale;
        hand.sprite = initialSprite;
        if (handZoomedInterface != null) handZoomedInterface.OnScreenCracked(message);
    }
}