﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Polls for Back button/Escape, shows a confirmation dialog, and calls callbackInterface.onBackButtonConfirmed()
// if the back button/Escape is pressed while the dialog is shown
public class BackButtonConfirmation : MonoBehaviour {
    
    public interface BackButtonCallbackInterface {

        // Called when the back button has been pressed the second time when the confirmation dialog is shown.
        // This is where you should clean up stuff, and call Application.Quit() or SceneManager.LoadLevel()
        void OnBackButtonConfirmed();
    }

    #region Private Variables

    BackButtonCallbackInterface callbackInterface;
    Coroutine confirmationPopUpCoroutine = null;

    #endregion

    #region MonoBehaviour Callbacks

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (confirmationPopUpCoroutine == null) confirmationPopUpCoroutine = StartCoroutine(ConfirmationPopUp());
            else callbackInterface.OnBackButtonConfirmed();
        }
    }

    #endregion

    #region Public Methods

    // Register a callbackInterface before polling for Update(). MonoBehaviour.Awake() is a good place for that
    public void RegisterCallbackInterface(BackButtonConfirmation.BackButtonCallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    #endregion

    #region Coroutines

    IEnumerator ConfirmationPopUp() {
        GetComponent<CanvasGroup>().alpha = 1f;
        yield return new WaitForSeconds(2f);
        GetComponent<CanvasGroup>().alpha = 0f;
        confirmationPopUpCoroutine = null;
    }

    #endregion
}
