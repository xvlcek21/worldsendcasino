﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
Used for scene transitions where the Host's hand swipes the screen dark/
*/
public class HandSwipeTransition : MonoBehaviour, UtilityMethods.SlideCallback {

    public bool startDark;
    public float darknessPositionX;

    [Space]
    public Sprite swipeToDarkSprite;
    public Sprite swipeOutOfDarkSprite;

    [FMODUnity.EventRef]
    public string swooshIn;

    [FMODUnity.EventRef]
    public string swooshOut;

    Vector2 darknessPosition;
    RectTransform rectTransform;

    string nextScene;

    void Awake() {
        darknessPosition = new Vector2(darknessPositionX, 0f);
        rectTransform = GetComponent<RectTransform>();
        if (startDark) {
            rectTransform.anchoredPosition = darknessPosition;
            GetComponent<Image>().sprite = swipeOutOfDarkSprite;
        }
        else GetComponent<Image>().sprite = swipeToDarkSprite;
    }

    // Exits the current scene with swipe to black, and loads the next scene
    public void ChangeScene(string scene) {
        GetComponent<Image>().sprite = swipeToDarkSprite;
        nextScene = scene;
        UtilityMethods.Slide(this, rectTransform, darknessPosition, 50f, 0.2f, this);
        Options.PlayOneShot(swooshIn, Options.AudioType.SFX);
    }

    // Enters the scene by swiping the darkness off the screen
    // startDark should be set true before calling this method
    public void EnterScene(UtilityMethods.SlideCallback callbackInterface = null) {
        GetComponent<Image>().sprite = swipeOutOfDarkSprite;
        UtilityMethods.Slide(this, rectTransform, new Vector2(rectTransform.sizeDelta.x, 0f), 50f, 0.2f, callbackInterface);
        Options.PlayOneShot(swooshOut, Options.AudioType.SFX);
    }

    // Swipe to dark without changing the scene
    public void SwipeToDark(UtilityMethods.SlideCallback callback) {
        GetComponent<Image>().sprite = swipeToDarkSprite;
        UtilityMethods.Slide(this, rectTransform, darknessPosition, 50f, 0.2f, callback);
        Options.PlayOneShot(swooshIn, Options.AudioType.SFX);
    }

    // Called after ChangeScene sliding animation is done
    public void OnSlideDone() {
        SceneManager.LoadScene(nextScene);
    }

    public void DestroyGameObject() {
        Destroy(gameObject);
    }
}
