﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Change object's scale during runtime in a smooth animation.
The script must be attached to an object with an RectTransform component, which has the same x and y, and z == 1.
X and Y axes are scaled in the same speed to the same scale (no stretching, Z always == 1)
*/
public class Zoomer : MonoBehaviour {

    // Call RegisterCallbackInterface, and pass it an object implementing this interface
    //  if you wan't to do something after the fade animation has ended (or is in some other trigger point)
    public interface CallbackInterface {

        // Called in Zoomer.Zoom(float scale, float speed) after the zooming is done, and the new scale is larger than before
        void OnZoomedIn();

        // Called in Zoomer.Zoom(float scale, float speed) after the zooming is done, and the new scale is smaller than before
        void OnZoomedOut();
    }
    
    public bool overrideInitialScale = false;

    [Tooltip("Ignored if overrideInitialScale == false")]
    public float initialScale;

    float speed = 0f;
    float targetScale;
    Vector3 scale;

    enum Status { WAIT, ZOOM_IN, ZOOM_OUT }
    Status status = Status.WAIT;
    CallbackInterface callbackInterface;

    void Awake() {
        if (overrideInitialScale) GetComponent<RectTransform>().localScale = new Vector3(initialScale, initialScale, 1f);
        scale = GetComponent<RectTransform>().localScale;
    }

    void Update() {
        float nextScale;

        switch (status) {
            case Status.WAIT:
                return;
            case Status.ZOOM_IN:
                scale = GetComponent<RectTransform>().localScale;

                nextScale = speed * Time.deltaTime;
                scale.x += nextScale;
                scale.y += nextScale;

                if (scale.x < targetScale)
                    GetComponent<RectTransform>().localScale = scale;
                else {
                    // Make sure it's the targetScale
                    scale.x = targetScale;
                    scale.y = targetScale;
                    GetComponent<RectTransform>().localScale = scale;

                    status = Status.WAIT;

                    if (callbackInterface != null) callbackInterface.OnZoomedIn();
                }
                break;
            case Status.ZOOM_OUT:
                scale = GetComponent<RectTransform>().localScale;

                nextScale = speed * Time.deltaTime;
                scale.x -= nextScale;
                scale.y -= nextScale;

                if (scale.x > targetScale)
                    GetComponent<RectTransform>().localScale = scale;
                else {
                    // Make sure it's the targetScale
                    scale.x = targetScale;
                    scale.y = targetScale;
                    GetComponent<RectTransform>().localScale = scale;

                    status = Status.WAIT;

                    if (callbackInterface != null) callbackInterface.OnZoomedOut();
                }
                break;
        }
    }

    public void RegisterCallbackInterface(Zoomer.CallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    // speed must be > 0
    public void Zoom(float scale, float speed) {
        float currentScale = GetComponent<RectTransform>().localScale.x;

        if (scale > currentScale) status = Status.ZOOM_IN;
        else if (scale < currentScale) status = Status.ZOOM_OUT;
        else return;

        this.speed = speed;
        this.targetScale = scale;
    }
}