﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyBoard : MonoBehaviour {

    public Text moneyText;

    public void SetMoney(int money, bool pulsate = false) {
        if (pulsate) UtilityMethods.Pulsate(this, moneyText.rectTransform);
        moneyText.text = money.ToString() + " $";
    }
}
