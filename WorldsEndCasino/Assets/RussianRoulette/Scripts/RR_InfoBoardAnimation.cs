﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RR_InfoBoardAnimation : MonoBehaviour {

    Animator animator;

    void Awake() {
        animator = GetComponent<Animator>();
    }

    public void OnFlickeringEnded() {
        animator.SetBool("Flicker", false);
    }
}
