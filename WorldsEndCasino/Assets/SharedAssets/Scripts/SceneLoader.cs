﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    // Use delay if you need sounds or animations to have time to finish
    public float delaySeconds = 0f;

    public void LoadScene(string sceneName) {
        if (delaySeconds <= 0) SceneManager.LoadScene(sceneName);
        else StartCoroutine(Delay(sceneName, delaySeconds));
    }

    void DelayCallback(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    IEnumerator Delay(string sceneName, float seconds) {
        yield return new WaitForSeconds(seconds);
        DelayCallback(sceneName);
    }
}
