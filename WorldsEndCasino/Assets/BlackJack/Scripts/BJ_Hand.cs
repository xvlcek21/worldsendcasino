﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BJ_Hand : MonoBehaviour {
    
    [Space]
    public Image hoveringHand;
    public Image fingeringHand;
    public Image lungingHand;

    public void Hover() {
        hoveringHand.gameObject.SetActive(true);
        fingeringHand.gameObject.SetActive(false);
        lungingHand.gameObject.SetActive(false);
    }

    public void ShowFinger() {
        hoveringHand.gameObject.SetActive(false);
        fingeringHand.gameObject.SetActive(true);
        lungingHand.gameObject.SetActive(false);
    }

    public void LungeAndShowDeathScreen(ref DeathScreen deathScreen, string message, DeathScreen.CallbackInterface callbackInterface) {
        hoveringHand.gameObject.SetActive(false);
        fingeringHand.gameObject.SetActive(false);
        lungingHand.gameObject.SetActive(true);
        deathScreen.ShowHandCrush(callbackInterface, message, ref lungingHand);
    }
}