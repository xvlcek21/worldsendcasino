﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseInfo : MonoBehaviour {

    public GameObject notEnoughMoney;
    public Image ruleBenderUsed;

    Color color;
    float alpha = 0.5f;

    void Awake() {
        color = GetComponent<Image>().color;
    }

    void Start() {
        notEnoughMoney.SetActive(false);
        ruleBenderUsed.gameObject.SetActive(false);
    }

    public void ShowRuleBenderUsed(Sprite sprite) {
        ruleBenderUsed.sprite = sprite;
        ruleBenderUsed.gameObject.SetActive(true);
        color.a = alpha;
        GetComponent<Image>().color = color;
        StartCoroutine(RuleBenderUsed());
    }
    IEnumerator RuleBenderUsed() {
        yield return new WaitForSeconds(2f);
        ruleBenderUsed.gameObject.SetActive(false);
        color.a = 0f;
        GetComponent<Image>().color = color;
    }

    public void ShowNotEnoughMoney() {
        StartCoroutine(NotEnoughMoney());
    }
    IEnumerator NotEnoughMoney() {
        notEnoughMoney.SetActive(true);
        yield return new WaitForSeconds(2f);
        notEnoughMoney.SetActive(false);
    }
}
