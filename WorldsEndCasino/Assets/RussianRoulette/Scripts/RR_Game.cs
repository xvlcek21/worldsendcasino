﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

/*
    Game logic of local multiplayer russian roulette

    PlayerPrefs:
    [X] = number of player from 0 to 5:

    TYPE:  | KEY:
    string | playerName[X]
    int    | id[X]
    int    | numberOfPlayers
*/
namespace WorldsEndCasino {

    public class RR_Game : MonoBehaviour, BackButtonConfirmation.BackButtonCallbackInterface, Timer.CallbackInterface, RuleBenderMenu.CallbackInterface,
        UtilityMethods.SlideCallback, TurnRandomizationScreen.CallbackInterface, DeathScreen.CallbackInterface, UtilityMethods.FadeCallback {

        class RR_Player : Player {
            
            public int skipTurnCost;
            public int reverseTurnCost;
            public int skipChamberCost;
            public int spinCylinderCost;
            
            public RR_Player(int turnIndex, int money, string name) : base(turnIndex, money, name) { }
        }

        enum TurnAction {
            PULL_TRIGGER, SKIP_TURN, REVERSE_TURN, SKIP_CHAMBER, SPIN_CYLINDER, TIME_OUT
        }

        #region Public Variables

        public string backButtonSceneName;

        [Space]
        public int infoBoardFontSize;
        public int infoBoardFontSizeBig;

        [Space]
        [Header("UI Objects")]
        public Text spinCylinderPromptText;
        public GameObject canvas;
        public GameObject currentPlayerUi;
        public GameObject alivePlayerUi;
        public GameObject blockInputPanel;
        public RR_RevolverScreen revolverScreen;
        public RR_CylinderSpinner cylinderSpinner;
        public Button changeYourFateButton;
        public Animator cylinderAnimator;

        [Space]
        [Header("FMOD events")]
        [FMODUnity.EventRef]
        public string ambientEvent;
        
        [FMODUnity.EventRef]
        public string shootBlankEvent;

        [FMODUnity.EventRef]
        public string shootEvent;

        [FMODUnity.EventRef]
        public string[] cockTheRevolverEvent;

        [FMODUnity.EventRef]
        public string[] timerStartEvent;
        
        [FMODUnity.EventRef]
        public string[] survivedEvent;
        
        [FMODUnity.EventRef]
        public string[] deathEvent;

        [Space]
        [Header("Scripts")]
        public HandSwipeTransition handSwipeTransitionScript;
        public RR_PlayerInfoBoxes playerInfoBoxesScript;
        public TurnOrderInfo turnOrderInfoScript;
        public RR_HelpCylinder helpCylinderScript;
        public RR_Hand handScript;
        public InfoBoard infoBoardScript;
        public RR_FlashPanel flashPanelScript;
        public RuleBenderMenu ruleBenderMenu;
        public BackButtonConfirmation backButtonConfirmatorScript;
        public Timer timerScript;
        public MoneyPopup moneyPopupScript;
        public TurnRandomizationScreen turnRandomizationScreenScript;
        public RetryScreen retryScreenScript;
        public DeathScreen deathScreenScript;

        #endregion

        #region Private Variables

        const float FADE_SPEED = 0.2f;
        const float FADE_SPEED_SLOW = 0.008f;

        const float TICK_DURATION = 1.5f;

        const int STARTING_TIME = 7;
        const int STARTING_MONEY = 7000;
        const int INIT_SKIP_TURN_COST = 5000;
        const int INIT_REVERSE_TURN_COST = 5000;
        const int INIT_SKIP_CHAMBER_COST = 5000;
        const int INIT_SPIN_CYLINDER_COST = 10000;
        const int REWARD = 3000;
        const int PRICE_INCREACE = 2000;

        int currentChamber = 0;
        int bulletInChamber = 0;

        int numberOfPlayers;
        int numberOfSurvivors;

        int currentPlayerIndex = 0;

        bool turnOrderRandomized = false;

        List<RR_Player> alivePlayers = new List<RR_Player>();
        List<RR_Player> deadPlayers = new List<RR_Player>();

        bool turnsReversed = false;
        bool firstRound = true;

        TurnAction turnAction = TurnAction.PULL_TRIGGER;
                
        FMOD.Studio.EventInstance ambientInstance;

        #endregion

        #region MonoBehaviour Callbacks

        void Awake() {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            backButtonConfirmatorScript.RegisterCallbackInterface(this);
            InitializeRuleBenderMenu();
        }

        void Start() {
            alivePlayerUi.GetComponent<CanvasGroup>().alpha = 0f;
            currentPlayerUi.GetComponent<CanvasGroup>().alpha = 0f;
            currentPlayerUi.GetComponent<CanvasGroup>().interactable = false;

            ambientInstance = FMODUnity.RuntimeManager.CreateInstance(ambientEvent);
            Options.StartFmodInstance(ambientInstance, Options.AudioType.MUSIC);
            
            blockInputPanel.SetActive(false);

            LoadPlayers();

            handSwipeTransitionScript.EnterScene(this);
        }

        void OnDestroy() {
            ambientInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            ambientInstance.release();

            timerScript.ReleaseResources();
        }

        public void OnSlideDone() {
            handSwipeTransitionScript.DestroyGameObject();
            StartFirstTurn();
        }

        public void OnBackButtonConfirmed() {
            SceneManager.LoadScene(backButtonSceneName);
        }

        #endregion

        #region Private Methods
        
        void Reset() {
            turnsReversed = false;
            currentPlayerIndex = 0;

            turnAction = TurnAction.PULL_TRIGGER;
            
            retryScreenScript.Show(false);

            LoadPlayers();
            
            helpCylinderScript.Reset();
            handScript.Reset();

            alivePlayerUi.GetComponent<CanvasGroup>().alpha = 0f;
            currentPlayerUi.GetComponent<CanvasGroup>().alpha = 0f;
            currentPlayerUi.GetComponent<CanvasGroup>().interactable = false;

            StartFirstTurn();
        }
        
        int GetNextPlayerIndex(bool lastPlayerDied) {
            int nextPlayerIndex = currentPlayerIndex;

            if (!turnsReversed) {
                if (!lastPlayerDied) {
                    ++nextPlayerIndex;
                }
                if (nextPlayerIndex >= alivePlayers.Count) nextPlayerIndex = 0;
            }
            else {
                --nextPlayerIndex;

                if (nextPlayerIndex < 0) nextPlayerIndex = alivePlayers.Count - 1;
            }
            return nextPlayerIndex;
        }

        void StartFirstTurn() {
            changeYourFateButton.interactable = true;

            retryScreenScript.Show(false);
            
            PrepareRuleBenderMenu();
            ShowWhatsItGonnaBe();
            UpdatePlayerMoney();
            playerInfoBoxesScript.SetMoney(currentPlayerIndex, alivePlayers[currentPlayerIndex].money.ToString(), false);

            SpinCylinder();

            ruleBenderMenu.Close();

            if (turnOrderRandomized) {
                turnRandomizationScreenScript.Shuffle(ref alivePlayers);
            }
            ShowTurnRandomizationScreen();
        }

        void ShowTurnRandomizationScreen() {
            UtilityMethods.FadeCanvasGroupVisible(this, false, alivePlayerUi, FADE_SPEED);

            turnRandomizationScreenScript.Show(alivePlayers.ToArray(), firstRound);

            // Update playerInfoBoxes
            for (int playerIndex = 0; playerIndex < alivePlayers.Count; ++playerIndex) {
                playerInfoBoxesScript.SetInfo(playerIndex, alivePlayers[playerIndex].name, alivePlayers[playerIndex].money.ToString());
            }
        }

        public void LetsGo() {
            turnRandomizationScreenScript.Hide(this);
        }
        // Callback->
        public void OnHidden() {
            StartCoroutine(ShowSpinCylinderScreen());
        }

        void StartTurn(bool lastPlayerDied, int lastPlayerMoney,
            int bulletInChamber, int currentChamber, TurnAction ruleBenderUsed) {
            changeYourFateButton.interactable = true;
            
            if (alivePlayers.Count == numberOfSurvivors) {
                Win(alivePlayers, deadPlayers);
                return;
            }

            this.turnAction = TurnAction.PULL_TRIGGER;
            
            PrepareRuleBenderMenu();

            if (lastPlayerDied) StartCoroutine(ShowSpinCylinderScreen());
            else {
                ruleBenderMenu.Close();
                ShowWhatsItGonnaBe();
                UpdatePlayerMoney();
                playerInfoBoxesScript.SetMoney(currentPlayerIndex, alivePlayers[currentPlayerIndex].money.ToString(), false);
                UtilityMethods.FadeCanvasGroupVisible(this, true, alivePlayerUi, FADE_SPEED);
                FadeCurrentPlayerUi(true);
                handScript.Reset();
                StartCountDown();
            }
        }

        void FadeCurrentPlayerUi(bool state, float fadeSpeed = FADE_SPEED) {
            if (!state) {
                currentPlayerUi.GetComponent<CanvasGroup>().interactable = false;
                UtilityMethods.FadeCanvasGroupVisible(this, state, currentPlayerUi, fadeSpeed);
            }
            else UtilityMethods.FadeCanvasGroupVisible(this, state, currentPlayerUi, fadeSpeed, this);
        }
        // Callback->
        // Call only on fade in (don't pass this callback interface to the FadeCanvasGroupVisible when fading out)
        public void OnFadeDone() {
            currentPlayerUi.GetComponent<CanvasGroup>().interactable = true;
        }

        void LoadPlayers() {
            List<string> playerNames = new List<string>();

            if (!PlayerPrefsHelper.LoadGamePrefs(ref playerNames, ref numberOfPlayers, ref numberOfSurvivors, ref turnOrderRandomized))
                Debug.Log("PlayerPrefsHelper.LoadGamePrefs failed");
            
            playerInfoBoxesScript.Initialize(numberOfPlayers);// Not working when turnOrderRandomized == false (This comment might be obsolete...)

            turnRandomizationScreenScript.Initialize(numberOfPlayers);

            alivePlayers.Clear();
            deadPlayers.Clear();

            for (int playerIndex = 0; playerIndex < numberOfPlayers; ++playerIndex) {
                RR_Player player = new RR_Player(playerIndex, STARTING_MONEY, playerNames[playerIndex]);

                player.skipTurnCost = INIT_SKIP_TURN_COST;
                player.reverseTurnCost = INIT_REVERSE_TURN_COST;
                player.skipChamberCost = INIT_SKIP_CHAMBER_COST;
                player.spinCylinderCost = INIT_SPIN_CYLINDER_COST;

                alivePlayers.Add(player);
            }
        }

        void ShowWhatsItGonnaBe() {
            infoBoardScript.SetInfo("What's it gonna be, " + alivePlayers[currentPlayerIndex].name + "?");
        }

        void UpdatePlayerMoney(bool pulsate = false) {
            if (pulsate) infoBoardScript.SetMoneyAndPulsate(alivePlayers[currentPlayerIndex].money);
            else infoBoardScript.SetMoney(alivePlayers[currentPlayerIndex].money);
        }

        void SpinCylinder() {
            bulletInChamber = UnityEngine.Random.Range(0, 6);
            currentChamber = 0;
            helpCylinderScript.SpinCylinder();
        }

        void Win(List<RR_Player> alivePlayers, List<RR_Player> deadPlayers) {
            handScript.FadeOut(FADE_SPEED);

            retryScreenScript.SetWinner(alivePlayers[0].name);

            List<string> loosersNames = new List<string>();

            for (int i = 0; i < deadPlayers.Count; ++i) {
                loosersNames.Add(deadPlayers[i].name);
            }

            retryScreenScript.SetLoosers(loosersNames);

            retryScreenScript.Show(true);
            deathScreenScript.Hide();
        }

        #endregion

        #region Main Screen Buttons

        public void OnPullTheTrigger(bool instant = false) {
            infoBoardScript.EraseInfo();
            blockInputPanel.SetActive(true);
            StopCountdown();
            StartCoroutine(CockAndShoot(instant));
        }

        #endregion

        #region Rule Bender Menu

        // RuleBenderMenu->
        public void InitializeRuleBenderMenu() {
            ruleBenderMenu.Initialize(this);
        }

        public void PrepareRuleBenderMenu() {
            RR_Player player = alivePlayers[currentPlayerIndex];
            ruleBenderMenu.Prepare(player.money, new[]{ player.skipTurnCost, player.reverseTurnCost, player.skipChamberCost, player.spinCylinderCost });// TODO Optimize new int[] -> outer scope
        }

        // RuleBenderMenu.CallbackInterface
        public void OnToggle(bool state) {
        }

        public void OnRuleBenderBought(string ruleBender, int spentMoney, int updatedCost) {
            currentPlayerUi.GetComponent<CanvasGroup>().interactable = false;
            StopCountdown();

            RR_Player currentPlayer = alivePlayers[currentPlayerIndex];

            if (ruleBender.Equals("Skip Turn")) {
                currentPlayer.money -= spentMoney;
                currentPlayer.skipTurnCost = updatedCost;

                turnAction = TurnAction.SKIP_TURN;

                handScript.ShowFistAndShrink();// No trigger pulled, shrink the hand
            }
            else if (ruleBender.Equals("Reverse Turn")) {
                currentPlayer.money -= spentMoney;
                currentPlayer.reverseTurnCost = updatedCost;

                turnsReversed = !turnsReversed;
                
                if (turnsReversed) turnOrderInfoScript.SetDirection(TurnOrderInfo.Direction.DOWN);
                else turnOrderInfoScript.SetDirection(TurnOrderInfo.Direction.UP);

                turnAction = TurnAction.REVERSE_TURN;

                handScript.ShowFistAndShrink();// No trigger pulled, shrink the hand
            }
            else if (ruleBender.Equals("Skip Chamber")) {
                currentPlayer.money -= spentMoney;
                currentPlayer.skipChamberCost = updatedCost;

                ++currentChamber;
                if (currentChamber >= 6) currentChamber = 0;
                helpCylinderScript.SkipChamber();

                turnAction = TurnAction.SKIP_CHAMBER;
            }
            else if (ruleBender.Equals("Spin Cylinder")) {
                currentPlayer.money -= spentMoney;
                currentPlayer.spinCylinderCost = updatedCost;

                SpinCylinder();

                turnAction = TurnAction.SPIN_CYLINDER;
            }
            else Debug.Log("RR_Game.OnRuleBenderBought: Invalid rule bender");

            UpdatePlayerMoney(true);
            playerInfoBoxesScript.SetMoney(currentPlayerIndex, alivePlayers[currentPlayerIndex].money.ToString(), true);
        }

        public void OnClosedAfterPurchase() {
            FadeCurrentPlayerUi(false);
            infoBoardScript.EraseMoney();
            infoBoardScript.EraseInfo();
        }

        public void OnInfoShown() {
            if (turnAction != TurnAction.SKIP_CHAMBER) infoBoardScript.SetInfoAndFlicker(GetPassThePhoneMessage(false));

            if (turnAction == TurnAction.SKIP_TURN) EndTurn();
            else if (turnAction == TurnAction.REVERSE_TURN) EndTurn();
            else if (turnAction == TurnAction.SKIP_CHAMBER) {
                FadeCurrentPlayerUi(false);
                infoBoardScript.EraseMoney();
                OnPullTheTrigger(true);
            }

            else if (turnAction == TurnAction.SPIN_CYLINDER) OnPullTheTrigger(true);
        }

        #endregion

        #region Retry Options

        public void Retry() {
            firstRound = false;
            Reset();
        }

        public void Exit() {
            OnBackButtonConfirmed();
        }

        #endregion

        #region Timer Methods And Callbacks

        public void StartCountDown() {
            blockInputPanel.SetActive(false);
            Options.PlayOneShot(timerStartEvent[UnityEngine.Random.Range(0, timerStartEvent.Length)], Options.AudioType.VOICE_ACTING);
            //ambientInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            timerScript.StartCountDown(STARTING_TIME, TICK_DURATION, this);
            handScript.IncreaseHandSize();
        }

        void StopCountdown() {
            handScript.StopHandSizeGrowing();

            //ambientInstance.start();
            timerScript.StopCountdown();
        }

        public void OnTick(int number) {
        }

        public void OnTimeUp() {
            ruleBenderMenu.ShutUp();
            ruleBenderMenu.Close();

            blockInputPanel.SetActive(true);

            StartCoroutine(TimeUpAndDieAnimation());
        }

        #endregion

        #region Coroutines

        IEnumerator CockAndShoot(bool instant = false) {
            FadeCurrentPlayerUi(false);
            infoBoardScript.EraseMoney();

            if (!instant) {
                Options.PlayOneShot(cockTheRevolverEvent[UnityEngine.Random.Range(0, cockTheRevolverEvent.Length)], Options.AudioType.MUSIC);
                yield return new WaitForSeconds(1.4f);
            }
            
            if (currentChamber != bulletInChamber) {
                handScript.ShowFistAndShrink();
                StartCoroutine(PullTheTriggerAndSurviveAnimation());
            }
            else {
                StartCoroutine(PullTheTriggerAndDieAnimation());
            }
        }

        IEnumerator TimeUpAndDieAnimation() {
            infoBoardScript.EraseMoney();
            UtilityMethods.FadeCanvasGroupVisible(this, false, alivePlayerUi, FADE_SPEED_SLOW);
            FadeCurrentPlayerUi(false, FADE_SPEED_SLOW);

            infoBoardScript.SetInfoAndFlicker("TIME'S UP!");
            handScript.ShowFinger();
            
            yield return new WaitForSeconds(3.5f);

            turnAction = TurnAction.TIME_OUT;

            infoBoardScript.SetInfoAndFlicker(GetPassThePhoneMessage(true));
            KillCurrentPlayer();
            handScript.LungeAndShowDeathScreen(ref deathScreenScript, GetPassThePhoneMessage(true), this);
        }
        // Callback->
        public void OnScreenCracked(string message) {
            handScript.FadeOut(FADE_SPEED);
            EndTurn(true);
        }

        IEnumerator PullTheTriggerAndDieAnimation() {
            infoBoardScript.EraseMoney();
            handScript.FadeOut(FADE_SPEED);
            UtilityMethods.FadeCanvasGroupVisible(this, false, alivePlayerUi, FADE_SPEED);
            
            Options.PlayOneShot(shootEvent, Options.AudioType.SFX);
            flashPanelScript.Flash();            
            helpCylinderScript.Shoot(true, false);

            KillCurrentPlayer();
            deathScreenScript.ShowBulletHole(GetPassThePhoneMessage(true));
            yield return new WaitForSeconds(1f);
            
            Options.PlayOneShot(deathEvent[UnityEngine.Random.Range(0, deathEvent.Length)], Options.AudioType.VOICE_ACTING);

            EndTurn(true);
        }

        IEnumerator PullTheTriggerAndSurviveAnimation() {
            Options.PlayOneShot(shootBlankEvent, Options.AudioType.SFX);
            helpCylinderScript.Shoot(false);
            moneyPopupScript.ShowIncrease(REWARD.ToString());
            yield return new WaitForSeconds(0.5f);
            
            Options.PlayOneShot(survivedEvent[UnityEngine.Random.Range(0, survivedEvent.Length)], Options.AudioType.VOICE_ACTING);
            StartCoroutine(Survive());
        }

        IEnumerator Survive() {
            alivePlayers[currentPlayerIndex].money += REWARD;
            playerInfoBoxesScript.SetMoney(currentPlayerIndex, alivePlayers[currentPlayerIndex].money.ToString(), true);
            infoBoardScript.SetInfoAndFlicker(GetPassThePhoneMessage(false));

            ++currentChamber;

            if (currentChamber >= 6) currentChamber = 0;
            
            yield return new WaitForSeconds(2.2f);
            
            EndTurn();
        }

        void KillCurrentPlayer() {
            deadPlayers.Add(alivePlayers[currentPlayerIndex]);
            alivePlayers.RemoveAt(currentPlayerIndex);

            playerInfoBoxesScript.KillCurrentPlayer(turnsReversed);
        }
        
        // Call KillCurrentPlayer() first if died == true
        void EndTurn(bool died = false) {
            StopCountdown();

            blockInputPanel.SetActive(true);

            if (currentPlayerUi.activeSelf) {
                FadeCurrentPlayerUi(false);
                infoBoardScript.EraseMoney();
            }
            StartCoroutine(ChangeTurn(died));
        }
        
        string GetPassThePhoneMessage(bool lastPlayerDied) {
            int tempCurrentPlayerIndex = currentPlayerIndex;

            if (!turnsReversed) {
                if (!lastPlayerDied) {
                    ++tempCurrentPlayerIndex;
                }
                if (tempCurrentPlayerIndex >= alivePlayers.Count) tempCurrentPlayerIndex = 0;
            }
            else {
                --tempCurrentPlayerIndex;

                if (tempCurrentPlayerIndex < 0) tempCurrentPlayerIndex = alivePlayers.Count - 1;
            }

            if (alivePlayers.Count > numberOfSurvivors) return "Pass the phone to " + alivePlayers[tempCurrentPlayerIndex].name + "!";
            else return "";
        }

        IEnumerator ChangeTurn(bool lastPlayerDied) {
            if (!turnsReversed) {
                if (!lastPlayerDied) {
                    ++currentPlayerIndex;
                }
                if (currentPlayerIndex >= alivePlayers.Count) currentPlayerIndex = 0;
            }
            else {
                --currentPlayerIndex;

                if (currentPlayerIndex < 0) currentPlayerIndex = alivePlayers.Count - 1;
            }

            if (!lastPlayerDied && alivePlayers.Count > numberOfSurvivors)
                playerInfoBoxesScript.ChangePlayer(alivePlayers[currentPlayerIndex].name, alivePlayers[currentPlayerIndex].money.ToString(), turnsReversed);

            yield return new WaitForSeconds(4f);

            StartTurn(lastPlayerDied, alivePlayers[currentPlayerIndex].money, bulletInChamber, currentChamber, turnAction);
        }

        IEnumerator ShowSpinCylinderScreen() {
            deathScreenScript.Hide();
            revolverScreen.Show(alivePlayers[currentPlayerIndex].name);

            blockInputPanel.SetActive(true);
            cylinderSpinner.blockInput = false;// CylinderSpinner sets this true after the spin

            while (true) {
                if (cylinderAnimator.GetBool("spinned")) {
                    cylinderAnimator.SetBool("spinned", false);

                    spinCylinderPromptText.text = "";
                    revolverScreen.Hide();
                    SpinCylinder();

                    spinCylinderPromptText.text = "";

                    ruleBenderMenu.Close();
                    ShowWhatsItGonnaBe();
                    UpdatePlayerMoney();
                    playerInfoBoxesScript.SetMoney(currentPlayerIndex, alivePlayers[currentPlayerIndex].money.ToString(), false);
                    handScript.Reset();
                    UtilityMethods.FadeCanvasGroupVisible(this, true, alivePlayerUi, FADE_SPEED);
                    FadeCurrentPlayerUi(true);
                    handScript.FadeIn(FADE_SPEED);
                    StartCountDown();
                    yield break;
                }
                else yield return null;
            }
        }

        #endregion
    }
}
