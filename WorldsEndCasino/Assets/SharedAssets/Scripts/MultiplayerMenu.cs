﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

static class Constants
{
    public const int MAX_PLAYERS = 6;
    public const int MIN_PLAYERS = 2;
}


public class MultiplayerMenu : MonoBehaviour, AlphaFader.CallbackInterface {

    #region private variables

    int playerCount;
    bool turnOrderRandomized;
    string nextSceneName;
    bool destroyThemePlayer = false;

    #endregion

    #region public variables

    public string backButtonScene;

    [Space]
    public AlphaFader black;
    public HandSwipeTransition handSwipeTransitionScript;

    [Space]
    public Button[] buttons;

    [Space]
    public Image title;
    public Sprite russianRouletteTitleSprite;
    public Sprite blackJackTitleSprite;
        
    [Space]
    public GameObject[] inputFields;
    public Text playerCountTextField;
    public Toggle randomizationToggle;

    #endregion

    #region Monobehavior callbacks

    void Awake()
    {
        black.RegisterCallbackInterface(this);

        // Setup the title based on what game was selected in the MainMenu
        int selectedGame = PlayerPrefs.GetInt(PlayerPrefsHelper.GAME_KEY, -1);

        // Debug settings so you can open the MultiplayerMenu without going through MainMenu
        if (selectedGame == -1) {
            PlayerPrefs.SetInt(PlayerPrefsHelper.GAME_KEY, PlayerPrefsHelper.RUSSIAN_ROULETTE);
            title.GetComponent<Image>().sprite = russianRouletteTitleSprite;
        }
        else if (selectedGame == PlayerPrefsHelper.RUSSIAN_ROULETTE) title.GetComponent<Image>().sprite = russianRouletteTitleSprite;
        else if (selectedGame == PlayerPrefsHelper.BLACK_JACK) title.GetComponent<Image>().sprite = blackJackTitleSprite;

        // At the beginning, the player count is initialized to the minimum possible amount of players.
        playerCount = Constants.MIN_PLAYERS;

        // Set up the access the key elements in the multiplayer menu.
        inputFields = GetInputFieldObjects();
        playerCountTextField = GetPlayerCountTextField();

        InitializeMenuScreen();
    }

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        handSwipeTransitionScript.EnterScene();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(backButtonScene);
        }
    }

    void OnDestroy() {
        if (destroyThemePlayer) Destroy(GameObject.Find("ThemePlayer"));
    }

    #endregion

    #region private methods

    void SetButtonsActive(bool state) {
        for (int i = 0; i < buttons.Length; ++i) {
            buttons[i].interactable = state;
        }
    }

    GameObject[] GetInputFieldObjects()
    {
        GameObject[] unsortedInputFields = GameObject.FindGameObjectsWithTag("PlayerNameInput");

        // Use Linq to sort the list.
        return unsortedInputFields.OrderBy(go => go.name).ToArray();
    }

    Text GetPlayerCountTextField()
    {
        return (GameObject.FindGameObjectWithTag("PlayerCountInput").GetComponent<Text>());
    }

    void InitializeMenuScreen()
    {
        // Initialize the playerCount.
        UpdatePlayerCountUI();
    }

    void UpdatePlayerCountUI()
    {
        playerCountTextField.text = playerCount.ToString();

        // Show the correct amount of input fields.
        for (int i = Constants.MIN_PLAYERS; i < Constants.MAX_PLAYERS; ++i)
        {
            if (i < playerCount)
                inputFields[i].SetActive(true);
            else
                inputFields[i].SetActive(false);
        }
    }

    #endregion

    #region protected methods

    protected List<string> GetPlayerList()
    {
        List<string> playerList = new List<string>();

        for (int i = 0; i < playerCount; ++i)
        {
            // Due to two text components in input fields, we have to choose the right one.
            List<Text> temp = inputFields[i].GetComponentsInChildren<Text>().ToList();
            for (int j = 0; j < temp.Count; ++j)
            {
                if (temp[j].name == "Text")
                {
                    if (temp[j].text.Length == 0)
                        playerList.Add("Player" + (i + 1));
                    else
                        playerList.Add(temp[j].text);
                    Debug.Log(playerList[i]);
                }
            }
        }
        return playerList;
    }

    #endregion

    #region public methods

    public void IncrementPlayerCount()
    {
        // Do not increase the number of players over the set maximum number.
        if (playerCount < Constants.MAX_PLAYERS)
        {
            ++playerCount;
            UpdatePlayerCountUI();
        }
        return;
    }

    public void DecrementPlayerCount()
    {
        // Do not decrease the number of players under the set minimum number.
        if (playerCount > Constants.MIN_PLAYERS)
        { 
            --playerCount;
            UpdatePlayerCountUI();
        }
        return;
    }

    public void RandomizeTurnOrder() {
        turnOrderRandomized = randomizationToggle.isOn;
    }

    // This method is invoked after the game starting button is pressed.
    public void StartGame() {
        destroyThemePlayer = true;

        // Get player list in the desired order. Send it as an array to the game asap.
        List<string> playerList = new List<string>();
        playerList = GetPlayerList();

        PlayerPrefsHelper.SaveGamePrefs(playerList, turnOrderRandomized);

        // Start the game that was selected from the MainMenu
        int selectedGame = PlayerPrefs.GetInt(PlayerPrefsHelper.GAME_KEY, -1);

        if (selectedGame == PlayerPrefsHelper.RUSSIAN_ROULETTE) {
            nextSceneName = "RR_Rules";
            SetButtonsActive(false);
            black.FadeIn();
        }
        else if (selectedGame == PlayerPrefsHelper.BLACK_JACK) {
            nextSceneName = "BJ_Rules";
            SetButtonsActive(false);
            black.FadeIn();
        }
    }

    public void Back() {
        PlayerPrefs.DeleteKey(PlayerPrefsHelper.GAME_KEY);
        SceneManager.LoadScene(backButtonScene);
    }

    #endregion

    #region Callbacks

    public void OnFadeIn() {
        SceneManager.LoadScene(nextSceneName);
    }

    public void OnFadeOut() {
    }

    #endregion
}