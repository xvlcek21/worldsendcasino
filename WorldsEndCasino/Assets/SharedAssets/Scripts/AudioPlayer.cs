﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// IMPLEMENT (if needed): loopingEvents / FMOD parameter supporting events (FMOD.Studio.EventInstance)
public class AudioPlayer : MonoBehaviour {

    [FMODUnity.EventRef]
    public string[] oneShotEvents;// SFX type always, because fuckit only buttons use this shitty class

    public Options.AudioType audioType = Options.AudioType.SFX;
    
	public void PlayOneShot (int index) {
		if (index < oneShotEvents.Length) {
            Options.PlayOneShot(oneShotEvents[index], audioType);
        }
	}
}
