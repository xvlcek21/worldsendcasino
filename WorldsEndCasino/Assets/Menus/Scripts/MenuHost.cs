﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Movement
{
    public float movementSpeed;
    public float initialMovementSpeed;
    public float spread;
    public float interpolationSpeed;
    public float pivotPointY;
}


public class MenuHost : MonoBehaviour {

    #region private variables

    private Transform hand;
    private Transform body;

    #endregion

    #region public variables

    public Movement bodyMovement;
    public Movement handMovement;

    public float handMovementOffset;

    #endregion

    void Start () {
        body = transform;
        hand = transform.GetChild(0);

        InitializeMovementClass(ref body, ref bodyMovement);
        InitializeMovementClass(ref hand, ref handMovement);

        // the hand movement should not be synchronized with the body movement, henceforth we introduce following offset.
        handMovement.pivotPointY += handMovementOffset;
    }
	
	void Update () {
        UpdateMovementSpeed(ref body, ref bodyMovement);
        UpdateMovementSpeed(ref hand, ref handMovement);
        Move();
    }

    #region private methods

    private void InitializeMovementClass(ref Transform t, ref Movement m)
    { // Since these properties are not going to be reused and/or modified anywhere else, we can justify leaving the initialisation here.
        m.movementSpeed = 0.3f;
        m.initialMovementSpeed = m.movementSpeed;
        m.spread = 0.8f;
        m.interpolationSpeed = 1.2f;
        m.pivotPointY = t.localPosition.y;
    }

    void UpdateMovementSpeed(ref Transform t, ref Movement m)
    {
        if (t.localPosition.y > m.pivotPointY + m.spread)
        {
            if (m.movementSpeed > 0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, -m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);

        }
        else if (t.localPosition.y < m.pivotPointY - m.spread)
        {
            if (m.movementSpeed < -0.1f)
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, 0f, m.interpolationSpeed * Time.deltaTime);
            else
                m.movementSpeed = Mathf.Lerp(m.movementSpeed, m.initialMovementSpeed, m.interpolationSpeed * Time.deltaTime);
        }
    }

    void Move()
    {
        body.localPosition = new Vector2(body.localPosition.x, body.localPosition.y + bodyMovement.movementSpeed);
        hand.localPosition = new Vector2(hand.localPosition.x, hand.localPosition.y + handMovement.movementSpeed);
    }

    #endregion



}
