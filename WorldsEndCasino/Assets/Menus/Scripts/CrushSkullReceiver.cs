﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrushSkullReceiver : MonoBehaviour {

    public WorldsEndCasino.TitleScreen titleScreenScript;

	public void CrushSkull() {
        titleScreenScript.CrushSkull();
    }
}
