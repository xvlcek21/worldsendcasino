﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
    PlayerPrefs:
    [X] = id of player starting from 0:

    TYPE:  | KEY:
    string | playerName[X]
    int    | wins[X]
    int    | numberOfPlayers
*/
namespace WorldsEndCasino {

    public class RR_PlayerEntry : MonoBehaviour {

        public GameObject playerAmountPanel;
        public GameObject survivorAmountPanel;
        public Button[] survivorButtons;
        public GameObject playerNamePanel;
        public InputField playerNameInput;
        public Text playerNameText;
        public string nextSceneName;
        public string backButtonSceneName;

        const int MAX_PLAYERS = 6;

        int numberOfPlayers = 0;
        int numberOfSurvivors = 0;
        int currentPlayerIndex = 0;

        string[] playerNames = new string[MAX_PLAYERS];

        void Start() {
            playerAmountPanel.SetActive(true);
            survivorAmountPanel.SetActive(false);
            playerNamePanel.SetActive(false);
        }

        void Update() {
            if (Input.GetKey(KeyCode.Escape)) SceneManager.LoadScene(backButtonSceneName);
        }

        void UpdatePlayerNameText() {
            playerNameText.text = "Name Player " + (currentPlayerIndex + 1).ToString() + ":";
        }

        void SaveAndContinue() {
            PlayerPrefs.SetInt("numberOfPlayers", numberOfPlayers);
            PlayerPrefs.SetInt("numberOfSurvivors", numberOfSurvivors);

            for (int playerIndex = 0; playerIndex < numberOfPlayers; ++playerIndex) {
                PlayerPrefs.SetString("playerName" + playerIndex.ToString(), playerNames[playerIndex]);
                PlayerPrefs.SetInt("wins" + playerIndex.ToString(), 0);
                PlayerPrefs.SetInt("deaths" + playerIndex.ToString(), 0);
            }

            StartCoroutine(WaitAndLoad());
        }

        // Wait for button click sound to end
        IEnumerator WaitAndLoad() {
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(nextSceneName);
        }

        public void SelectNumberOfPlayers(int amount) {
            numberOfPlayers = amount;

            for (int i = numberOfPlayers - 1; i < 5; ++i) {
                survivorButtons[i].interactable = false;
            }

            playerAmountPanel.SetActive(false);
            survivorAmountPanel.SetActive(true);
        }

        public void SelectNumberOfSurvivors(int amount) {
            numberOfSurvivors = amount;

            UpdatePlayerNameText();

            survivorAmountPanel.SetActive(false);
            playerNamePanel.SetActive(true);
        }

        //SelectNumberOfPlayers(int) must be called before this method
        public void SetPlayerName() {
            if (playerNameInput.text.Length > 0) {
                playerNames[currentPlayerIndex++] = playerNameInput.text;

                if (currentPlayerIndex < numberOfPlayers) {
                    playerNameInput.text = "";
                    UpdatePlayerNameText();
                }
                else SaveAndContinue();
            }
            // TODO: Else error handling
        }
    }

}